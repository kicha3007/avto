'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var minify = require('gulp-csso');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var path = 'wp-content/themes/flagstudio/assets/';

var browserSync = require('browser-sync').create();
var localname = 'av.flagstudio.loc';

gulp.task('sass', function () {
    gulp.src(path + 'sass/style.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([
            autoprefixer({
                browsers: ['last 2 versions','IE 11', 'IE 10'],
                remove: false
            })
        ]))
        .pipe(gulp.dest(path + 'css'))
        //.pipe(sourcemaps.write('./'))
        .pipe(minify())
        .pipe(rename('style.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path + 'css'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function () {
    browserSync.init({
        proxy: localname
    });

    gulp.watch(path + 'sass/**/*.scss', ['sass']);
    gulp.watch(path + 'js/main.js').on('change', browserSync.reload);
    gulp.watch(path + '../**/*.php').on('change', browserSync.reload);
});

gulp.task('img', function () {
    return gulp.src(path +'img/**/*.{png,jpg,svg}')
        .pipe(imagemin([
            imagemin.optipng({optimizatonLevel: 3}),
            imagemin.jpegtran({progressive: true}),
            imagemin.svgo()
        ]))
        .pipe(gulp.dest(path +'img'))
});