<?php
/**
 * Template Name: Шаблон страницы подобрать автомобиль (page-cars-for-sale.php)
 */
get_header();

?>

    <main>

        <section class="breadcrumbs">
            <div class="inner">
                <? the_breadcrumb(); ?>
            </div>
        </section>

        <section class="pick-up-block">
            <div class="inner">
                <h2 class="pick-up-block__title">Автомобили с пробегом</h2>
                <section class="form-tabs">
                    <section class="form-tabs__header">
                        <ul>
                            <li class="form-tabs__header-item form-tabs__header-item--active">
                                <a href="#tab1">Подобрать автомобиль</a>
                            </li>
                            <li class="form-tabs__header-item">
                                <a href="#tab2">Продать автомобиль</a>
                            </li>
                        </ul>
                    </section>
                    <section class="form-tabs__body">
                        <section class="form-tabs__body-item form-tabs__body-item--active" id="tab1">
                            <form method="get" enctype="multipart/form-data" class="form-pickup js--nice-select">
                                <div class="form-pickup__row">
                                    <div class="form-pickup__item">
                                        <h1 class="form-pickup__header">Выберите марку</h1>
                                        <select name="brand-name" id="brand">
                                            <option value="LADA">LADA</option>
                                            <option value="BMW">BMW</option>
                                            <option value="AUDI">AUDI</option>
                                        </select>
                                    </div>
                                    <div class="form-pickup__item">
                                        <h1 class="form-pickup__header">Выберите модель</h1>
                                        <select name="model-name" id="model">
                                            <option value="Vesta">Vesta</option>
                                            <option value="Granta">Grants</option>
                                            <option value="Kalina">Kalina</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-pickup__row form-pickup__row--line-b">
                                    <div class="form-pickup__item">
                                        <h1 class="form-pickup__header">Год выпуска</h1>
                                        <div class="range js--range-year"></div>
                                        <input class="js--data-year" type="text" style="display: none;" data-from-year="1990" data-to-year="2018" data-step-year="1" data-set-default-value="1997,2015">
                                        <label><span>от</span><input type="number" class="form-pickup__input js--from-year" name="minYear"></label>
                                        <label><span>до</span><input type="number" class="form-pickup__input js--to-year" name="maxYear"></label>
                                    </div>
                                    <div class="form-pickup__item">
                                        <h1 class="form-pickup__header">Цена, руб.</h1>
                                        <div class="range js--range-cost"></div>
                                        <input class="js--data-cost" type="text" style="display: none;" data-from-cost="100000" data-to-cost="1200000" data-step-cost="1000" data-set-default-cost="228000,999999">
                                        <label><span>от</span><input type="number" class="form-pickup__input js--from-cost" name="minCost"></label>
                                        <label><span>до</span><input type="number" class="form-pickup__input js--to-cost" name="maxcost"></label>
                                    </div>
                                </div>
                                <button class="form-pickup__button" type="submit">Подобрать</button>
                            </form>
                            <section class="searching-result-block">
                                <div class="inner">
                                    <h2 class="searching-result-block__title">Результаты поиска</h2>
                                    <ul class="sorting-block js--sort-block">
                                        <span class="sorting-block__text">Сортировать по: </span>
                                        <li class="sorting-block__item">
                                            <button>цене</button>
                                        </li>
                                        <li class="sorting-block__item sorting-block__item--active sorting-block__item--arrow-up">
                                            <button>году выпуска</button>
                                        </li>
                                    </ul>

                                    <div class="searching-result-block__wrap">
                                        <section class="searching-result-block__item">
                                            <a class="searching-result-block__link" href="#!"></a>
                                            <figure>
                                                <ul class="searching-result-block__images js--fancybox">
                                                    <li class="searching-result-block__image">
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" data-fancybox="gallery1">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" alt="photo1" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" data-fancybox="gallery1">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" alt="photo2" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" data-fancybox="gallery1">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" alt="photo3" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" data-fancybox="gallery1">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" alt="photo4" width="270" height="230" data-fancybox="gallery">
                                                        </a>
                                                    </li>
                                                </ul>
                                                <ul class="searching-result-block__indicators">
                                                    <li class="searching-result-block__indicator searching-result-block__indicator--active"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                </ul>
                                            </figure>
                                            <section class="desc-block">
                                                <h1 class="desc-block__title">
                                                    <span class="desc-block__title-text">LADA Vesta</span>
                                                    <span class="desc-block__title--credit">Доступна в кредит</span>
                                                </h1>
                                                <p class="desc-block__item">
                                                    <b>Двигатель:</b>
                                                    1.6 л 16-кл. (106 л.с.)
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Цвет:</b>
                                                    Зеленый
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Коробка:</b>
                                                    А.К.П.П.
                                                </p>
                                                <span class="btn dark-btn">Подробнее</span>
                                            </section>
                                            <section class="cost-block">
                                                <ul class="cost-block__feature">
                                                    <li class="cost-block__feature-item">
                                                        2015
                                                    </li>
                                                    <li class="cost-block__feature-item">
                                                        60 000 км
                                                    </li>
                                                </ul>
                                                <b class="cost-block__cost">
                                                    599 000 руб.
                                                </b>
                                                <button class="form-pickup__button request-book" data-equipment="LADA Vesta" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                                            </section>
                                        </section>

                                        <section class="searching-result-block__item">
                                            <a class="searching-result-block__link" href="#!"></a>
                                            <figure>
                                                <ul class="searching-result-block__images js--fancybox">
                                                    <li class="searching-result-block__image">
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" data-fancybox="gallery2">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" alt="photo1" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" data-fancybox="gallery2">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" alt="photo2" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" data-fancybox="gallery2">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" alt="photo3" width="270" height="230">
                                                        </a>
                                                    </li>
                                                </ul>
                                                <ul class="searching-result-block__indicators">
                                                    <li class="searching-result-block__indicator searching-result-block__indicator--active"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                </ul>
                                            </figure>
                                            <section class="desc-block">
                                                <h1 class="desc-block__title">
                                                    <span class="desc-block__title-text">Mitsubishi Outlander I</span>
                                                    <span class="desc-block__title--credit">Доступна в кредит</span>
                                                </h1>
                                                <p class="desc-block__item">
                                                    <b>Двигатель:</b>
                                                    1.6 л 16-кл. (106 л.с.)
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Цвет:</b>
                                                    Зеленый
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Коробка:</b>
                                                    А.К.П.П.
                                                </p>
                                                <span class="btn dark-btn">Подробнее</span>
                                            </section>
                                            <section class="cost-block">
                                                <ul class="cost-block__feature">
                                                    <li class="cost-block__feature-item">
                                                        2015
                                                    </li>
                                                    <li class="cost-block__feature-item">
                                                        60 000 км
                                                    </li>
                                                </ul>
                                                <b class="cost-block__cost">
                                                    599 000 руб.
                                                </b>
                                                <button class="form-pickup__button request-book" data-equipment="Mitsubishi Outlander I" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                                            </section>
                                        </section>

                                        <section class="searching-result-block__item">
                                            <a class="searching-result-block__link" href="#!"></a>
                                            <figure>
                                                <ul class="searching-result-block__images js--fancybox">
                                                    <li class="searching-result-block__image">
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" data-fancybox="gallery3">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" alt="photo1" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" data-fancybox="gallery3">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" alt="photo2" width="270" height="230">
                                                        </a>
                                                    </li>
                                                </ul>
                                                <ul class="searching-result-block__indicators">
                                                    <li class="searching-result-block__indicator searching-result-block__indicator--active"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                </ul>
                                            </figure>
                                            <section class="desc-block">
                                                <h1 class="desc-block__title">
                                                    <span class="desc-block__title-text">Volkswagen Touareg I</span>
                                                </h1>
                                                <p class="desc-block__item">
                                                    <b>Двигатель:</b>
                                                    1.6 л 16-кл. (106 л.с.)
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Цвет:</b>
                                                    Зеленый
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Коробка:</b>
                                                    А.К.П.П.
                                                </p>
                                                <span class="btn dark-btn">Подробнее</span>
                                            </section>
                                            <section class="cost-block">
                                                <ul class="cost-block__feature">
                                                    <li class="cost-block__feature-item">
                                                        2015
                                                    </li>
                                                    <li class="cost-block__feature-item">
                                                        60 000 км
                                                    </li>
                                                </ul>
                                                <b class="cost-block__cost">
                                                    599 000 руб.
                                                </b>
                                                <button class="form-pickup__button request-book" data-equipment="Volkswagen Touareg I" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                                            </section>
                                        </section>

                                        <section class="searching-result-block__item">
                                            <a class="searching-result-block__link" href="#!"></a>
                                            <figure>
                                                <ul class="searching-result-block__images js--fancybox">
                                                    <li class="searching-result-block__image">
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" data-fancybox="gallery4">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" alt="photo1" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" data-fancybox="gallery4">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" alt="photo2" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" data-fancybox="gallery4">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" alt="photo3" width="270" height="230">
                                                        </a>
                                                    </li>
                                                    <li class="searching-result-block__image visually-hidden" >
                                                        <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" data-fancybox="gallery4">
                                                            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" alt="photo4" width="270" height="230" data-fancybox="gallery">
                                                        </a>
                                                    </li>
                                                </ul>
                                                <ul class="searching-result-block__indicators">
                                                    <li class="searching-result-block__indicator searching-result-block__indicator--active"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                    <li class="searching-result-block__indicator"></li>
                                                </ul>
                                            </figure>
                                            <section class="desc-block">
                                                <h1 class="desc-block__title">
                                                    <span class="desc-block__title-text">Ford Mondeo IV</span>
                                                    <span class="desc-block__title--credit">Доступна в кредит</span>
                                                </h1>
                                                <p class="desc-block__item">
                                                    <b>Двигатель:</b>
                                                    1.6 л 16-кл. (106 л.с.)
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Цвет:</b>
                                                    Зеленый
                                                </p>
                                                <p class="desc-block__item">
                                                    <b>Коробка:</b>
                                                    А.К.П.П.
                                                </p>
                                                <span class="btn dark-btn">Подробнее</span>
                                            </section>
                                            <section class="cost-block">
                                                <ul class="cost-block__feature">
                                                    <li class="cost-block__feature-item">
                                                        2015
                                                    </li>
                                                    <li class="cost-block__feature-item">
                                                        60 000 км
                                                    </li>
                                                </ul>
                                                <b class="cost-block__cost">
                                                    599 000 руб.
                                                </b>
                                                <button class="form-pickup__button request-book" data-equipment="Ford Mondeo IV" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                                            </section>
                                        </section>
                                    </div>
                                    <section class="pagination">
                                        <a class="prev page-numbers" href="#!">Назад</a>
                                        <a class="page-numbers" href="#!">1</a>
                                        <span aria-current="page" class="page-numbers current">2</span>
                                        <a class="page-numbers" href="#!">3</a>
                                        <a class="page-numbers" href="#!">4</a>
                                        <span class="page-numbers dots">…</span>
                                        <a class="page-numbers" href="#!">21</a>
                                        <a class="page-numbers" href="#!">22</a>
                                        <a class="next page-numbers" href="#!">Вперед</a>
                                    </section>
                                </div>
                            </section>
                            <section class="content">
                                <div class="inner">
                                    <h2 class="content-title">
                                        <svg class="pickup-svg">
                                            <path fill-rule="evenodd"  stroke="rgb(229, 229, 229)" stroke-width="2px" stroke-dasharray="8, 4" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                                                  d="M742.000,1469.000 C592.000,1520.000 453.000,1377.000 381.000,1355.000 C309.000,1333.000 243.000,1324.000 167.000,1410.000 C91.000,1496.000 -12.000,1428.000 3.000,1340.000 C18.000,1252.000 141.000,1226.000 143.000,1144.000 C145.000,1061.999 127.000,1034.000 116.000,967.000 C105.000,900.000 228.201,867.760 249.000,832.000 C272.028,792.405 225.000,692.999 216.000,631.000 C207.000,569.000 177.000,392.000 271.000,307.000 C365.000,222.000 543.000,283.000 564.000,281.000 C585.000,279.000 642.655,261.443 637.000,200.000 C631.178,136.735 602.241,115.647 623.000,82.000 C644.853,46.578 724.437,69.301 744.000,38.000 C756.500,18.000 754.000,2.000 754.000,2.000 "/>
                                        </svg>
                                        Автомобили с пробегом
                                    </h2>
                                    <p class="content__text">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas!
                                    </p>
                                </div>
                            </section>
                        </section>
                        <section class="form-tabs__body-item" id="tab2">
                            <form method="post" enctype="multipart/form-data" class="form-pickup form-sell">
                                <div class="form-pickup__row">
                                    <label class="field-block">
                                        <span class="field-block__title">Марка*</span>
                                        <input class="js--validate" type="text" name="brand-name" placeholder="Марка автомобиля" required>
                                    </label>
                                    <label class="field-block">
                                        <span class="field-block__title">Модель*</span>
                                        <input class="js--validate"  type="text" name="model-name" placeholder="Модель автомобиля" required>
                                    </label>
                                    <label class="field-block field-block--half">
                                        <span class="field-block__title">Пробег*</span>
                                        <input class="js--validate"  type="number" name="mileage" min="0" value="100000" required>
                                    </label>
                                    <label class="field-block field-block--half">
                                        <span class="field-block__title">Год выпуска*</span>
                                        <input class="js--validate"  type="number" name="release-year" min="1980" value="2010" required>
                                    </label>
                                </div>
                                <div class="form-pickup__row">
                                    <label class="field-block">
                                        <span class="field-block__title">Ф.И.О.*</span>
                                        <input class="js--validate"  type="text" name="name-user"  placeholder="Фамилия Имя Отчество" required>
                                    </label>
                                    <label class="field-block">
                                        <span class="field-block__title">Телефон*</span>
                                        <input class="js--validate"  type="tel" name="tel-user" placeholder="+7 (999) 999-99-99" required>
                                    </label>
                                    <label class="field-block">
                                        <span class="field-block__title">E-mail*</span>
                                        <input class="js--validate"  type="email" name="email-user"  placeholder="info@sitename.com" required>
                                    </label>
                                </div>
                                <div class="form-pickup__row form-pickup__row--line-b form-pickup__row--half">
                                    <input class="privacy-policy__checkbox" id="agree-privacy-policy" type="checkbox">
                                    <label class="custom-checkbox field-text" for="agree-privacy-policy">Отправляя заявку через форму обратной связи Вы соглашаетесь с нашей <a href="#!">Политикой конфиденциальности</a></label>
                                </div>
                                <button class="form-pickup__button form-pickup__button--theme-blue" type="submit" id="submit-sell" disabled>Отправить заявку</button>
                            </form>
                            <section class="content">
                                <div class="inner">
                                    <h2 class="content-title">
                                        <svg class="pickup-svg">
                                            <path fill-rule="evenodd"  stroke="rgb(229, 229, 229)" stroke-width="2px" stroke-dasharray="8, 4" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                                                  d="M742.000,1469.000 C592.000,1520.000 453.000,1377.000 381.000,1355.000 C309.000,1333.000 243.000,1324.000 167.000,1410.000 C91.000,1496.000 -12.000,1428.000 3.000,1340.000 C18.000,1252.000 141.000,1226.000 143.000,1144.000 C145.000,1061.999 127.000,1034.000 116.000,967.000 C105.000,900.000 228.201,867.760 249.000,832.000 C272.028,792.405 225.000,692.999 216.000,631.000 C207.000,569.000 177.000,392.000 271.000,307.000 C365.000,222.000 543.000,283.000 564.000,281.000 C585.000,279.000 642.655,261.443 637.000,200.000 C631.178,136.735 602.241,115.647 623.000,82.000 C644.853,46.578 724.437,69.301 744.000,38.000 C756.500,18.000 754.000,2.000 754.000,2.000 "/>
                                        </svg>
                                        Автомобили с пробегом
                                    </h2>
                                    <p class="content__text">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas!
                                    </p>
                                    <p class="content__text">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem blanditiis dicta dolores, et eum facere incidunt iure laudantium minus modi necessitatibus nihil nobis quia repellat suscipit temporibus, veritatis voluptas!
                                    </p>
                                    <p class="content__text">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    </p>
                                </div>
                            </section>
                            <section class="car-types-block">
                                <?get_template_part('inc/car-types', 'index'); ?>
                            </section>
                        </section>
                    </section>
                </section>
            </div>
        </section>
    </main>
<?php get_footer(); ?>