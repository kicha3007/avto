<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
//Обработчик ФОС Запись на ТО
if (isset($_POST['subRepair']) && wp_verify_nonce($_POST['noncerepair'], 'repair')) {
    global $wp;
    include('lada-mailer.php');
    $err = false;
    $name = $_POST['username'];
    if (empty($name) || $name == ' ') $err = true;
    $phone = $_POST['userphone'];
    if (empty($phone) || $phone == ' ') $err = true;
    $mail = $_POST['useremail'];
    if (empty($mail) || $mail == ' ') $err = true;
    $car = $_POST['car'];
    if (empty($car) || $car == ' ') $err = true;
    $date = $_POST['userdate'];
    if (empty($date) || $date == ' ') $err = true;
    $time = $_POST['usertime'];
    if (empty($time) || $time == ' ') $err = true;
    $jobs = $_POST['jobs'];
    if (!$err) {
//Отправка письма пользователю
        $message = 'Добрый день!<br><br>
                Вы оставили заявку на сайте ' . get_site_url() . '.<br> Спасибо за проявленный интерес, наш менеджер свяжется с Вами в ближайшее время.<br>
                Содержание заявки:<br>
                Заявка на ТО<br>
                ФИО: ' . $name . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>
                Дата визита: ' . $date . '<br>
                Время визита: ' . $time . '<br>
                Автомобиль: ' . $car . '<br>
                Работы: ' . $jobs . '<br>';
        $email = ladaMail();
        $email->Subject = 'Заявка на сайте Лада Автовек';
        $email->MsgHTML($message);
        $email->AddAddress($mail);
        $send = $email->Send();
//Письмо администраторам
        $message = 'Поступила заявка «Записаться на ТО» со страницы «' . home_url($wp->request) . '»<br><br>
                Содержание заявки:<br>
                ФИО: ' . $name . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>
                Дата визита: ' . $date . '<br>
                Время визита: ' . $time . '<br>
                Автомобиль: ' . $car . '<br>
                Работы: ' . $jobs . '<br>';
        $email2 = ladaMail();
        $email2->Subject = 'Новая заявка на ТО с сайта Лада Автовек';
        $email2->MsgHTML($message);
        $mails = get_field('fos_to', 'options');
        foreach ($mails as $m) {
            $email2->AddAddress($m['mail']);
        }
        $send = $email2->Send();
        header('Location: /thank-you');
    }
}
//Обработчик ФОС ТЕСТ ДРАЙВ
if (isset($_POST['testDrive']) && wp_verify_nonce($_POST['noncetestForm'], 'testForm')) {
    global $wp;
    include('lada-mailer.php');
    $err = false;
    $text = $_POST['text'];
    $name = $_POST['username'];
    if (empty($name) || $name == ' ') $err = true;
    $phone = $_POST['userphone'];
    if (empty($phone) || $phone == ' ') $err = true;
    $mail = $_POST['useremail'];
    if (empty($mail) || $mail == ' ') $err = true;
    $car = $_POST['car'];
    if (empty($car) || $car == ' ') $err = true;
    if (!$err) {
//Отправка письма пользователю
        $message = "Добрый день!<br><br>
                Вы оставили заявку на сайте " . get_site_url() . ".<br> Спасибо за проявленный интерес, наш менеджер свяжется с Вами в ближайшее время.<br>
                Содержание заявки:<br>
                Заявка на Тест-драйв<br>
                ФИО: $name<br>
                E-mail: $mail<br>
                Телефон: $phone<br>
                Автомобиль: $car<br>
                Текст: $text<br>
                <br><br>
                Удачного дня!";
        $email = ladaMail();
        $email->Subject = 'Заявка на сайте Лада Автовек';
        $email->MsgHTML($message);
        $email->AddAddress($mail);
        $send = $email->Send();
//Письмо администраторам
        $message = 'Поступила заявка «Записаться на тест-драйв» со страницы «' . home_url($wp->request) . '»<br><br>
                Содержание заявки:<br>
                ФИО: ' . $name . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>
                Автомобиль: ' . $car . '<br>
                Текст: ' . $text . '<br>';
        $email2 = ladaMail();
        $email2->Subject = 'Новая заявка на тест-драйв с сайта Лада Автовек';
        $email2->MsgHTML($message);
        $mails = get_field('fos_test', 'options');
        foreach ($mails as $m) {
            $email2->AddAddress($m['mail']);
        }
        $send = $email2->Send();
        header('Location: /thank-you');
    }
}
//Обработчик ФОС Оставить заявку
if (isset($_POST['subComplect']) && wp_verify_nonce($_POST['noncecomplect'], 'complect')) {
    global $wp;
    include('lada-mailer.php');
    $err = false;
    $text = $_POST['text'];
    $name = $_POST['username'];
    if (empty($name) || $name == ' ') $err = true;
    $phone = $_POST['userphone'];
    if (empty($phone) || $phone == ' ') $err = true;
    $mail = $_POST['useremail'];
    if (empty($mail) || $mail == ' ') $err = true;
    $model = $_POST['usermodel'];
    if (empty($model) || $model == ' ') $err = true;
    if (!$err) {
//Отправка письма пользователю
        $message = 'Добрый день!<br><br>
                Вы оставили заявку на сайте ' . get_site_url() . '.<br> Спасибо за проявленный интерес, наш менеджер свяжется с Вами в ближайшее время.<br>
                Содержание заявки:<br>
                Модель/Комплектация: ' . $model . '<br>
                ФИО: ' . $name . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>
                Доп. информация: ' . $text . '<br>
                <br><br>
                Удачного дня!';
        $email = ladaMail();
        $email->Subject = 'Заявка на сайте Лада Автовек';
        $email->MsgHTML($message);
        $email->AddAddress($mail);
        $send = $email->Send();
//Письмо администраторам
        $message = 'Поступила заявка со страницы «' . home_url($wp->request) . '»<br><br>
                Содержание заявки:<br>
                Модель/Комплектация: ' . $model . '<br>
                ФИО: ' . $name . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>
                Доп. информация: ' . $text . '<br>';
        $email2 = ladaMail();
        $email2->Subject = 'Новая заявка на автомобиль с сайта Лада Автовек';
        $email2->MsgHTML($message);
        $mails = get_field('fos_complect', 'options');
        foreach ($mails as $m) {
            $email2->AddAddress($m['mail']);
        }
        $send = $email2->Send();
        header('Location: /thank-you');
    }
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="cmsmagazine" content="3a145314dbb5ea88527bc9277a5f8274" />
    <meta name="developer" content="flagstudio.ru" />
    <link rel="icon" href="<?=get_stylesheet_directory_uri()?>/assets/img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css">

	 <!--[if lt IE 9]>
	 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	 <![endif]-->

    <?php
    if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'pick-up'){
        echo "<title>Автомобили с пробегом</title>";
    }
    else{?>
        <title><?php wp_title(''); ?></title>
    <?php } ?>

	<?php wp_head(); ?>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PM2ZWKW');</script>
    <!-- End Google Tag Manager -->

    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PM2ZWKW" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<header>
    <section class="top-menu">
        <div class="inner">
            <div class="top-menu__logotypes-wrap">
                <a class="top-menu__lada-wrap" href="/" title="На главную"><img class="logo-lada" src="<?=get_stylesheet_directory_uri()?>/assets/img/lada-logo.svg" width="126" height="35" alt="logo lada"></a>
                <div class="top-menu__avtovek-wrap">
                    <a href="/" title="На главную"><img class="logo-avtovek" src="<?=get_stylesheet_directory_uri()?>/assets/img/logo-avtovek--gray.svg" width="101" height="26" alt="logo avtovek"></a>
                    <b class="top-menu__avtovek-text">Самый крупный<br>на Урале*</b>
                </div>
            </div>

            <a class="top-menu__phone" href="tel: +7 343 253 00 53">+7 343 253 00 53</a>
            <a class="show-call-popup">
                Обратный звонок
            </a>
            <div class="top-menu-contacts-wrap">
                <div class="top-menu__link-wrap">
                    <a class="top-menu__whatsapp" href="tel: +7 904 165 1 888">+7 904 165 1 888</a>
                    <span>- Отдел продаж</span>
                </div>
                <div class="top-menu__link-wrap">
                    <a class="top-menu__whatsapp" href="tel: +7 965 516 00 99">+7 965 516 00 99</a>
                    <span>- Сервис</span>
                </div>

                <address>
                    <a href="/#question">Екатеринбург, ул. Металлургов, 69</a>
                </address>
            </div>
        </div>
    </section>

    <ul id="js-fixed-icons" class="fixed-icons">
        <li><a class="show-call-popup"><span>Бесплатный звонок</span><b class="fixed-icons__icon phone-big"></b></a></li>
        <li><a class="show-test-drive-popup"><span>Пройти Тест-Драйв</span><b class="fixed-icons__icon test"></b></a></li>
        <li><a class="show-repair-popup"><span>Записаться на ремонт/ТО</span><b class="fixed-icons__icon repairs"></b></a></li>
        <li><a href="/avtokredit"><span>Заявка на кредит</span><b class="fixed-icons__icon credit"></b></a></li>
        <li><a href="/#question"><span>Задать вопрос</span><b class="fixed-icons__icon question"></b></a></li>
    </ul>

    <section class="main-menu">
        <div class="inner">
            <button class="cmn-toggle-switch">
                <span>toggle menu</span>
            </button>
            <?
           $args = array(
              'theme_location'  => 'main-header-menu',
              'menu'            => '',              // (string) Название выводимого меню (указывается в админке при создании меню, приоритетнее чем указанное местоположение theme_location - если указано, то параметр theme_location игнорируется)
              'container'       => '',           // (string) Контейнер меню. Обворачиватель ul. Указывается тег контейнера (по умолчанию в тег div)
              'container_class' => '',              // (string) class контейнера (div тега)
              'menu_class'      => 'nav-menu',          // (string) class самого меню (ul тега)
              'echo'            => true,            // (boolean) Выводить на экран или возвращать для обработки
              'fallback_cb'     => 'wp_page_menu',  // (string) Используемая (резервная) функция, если меню не существует (не удалось получить)
           );
           wp_nav_menu($args);
           ?>
        </div>
        <script>
            var $menu = jQuery('.main-menu .nav-menu > li.menu-item-has-children > .sub-menu');

            $menu.each(function () {

                var $sub_menu = jQuery(this).find('.sub-menu');

                if (jQuery(this).length && $sub_menu.length) {
                    $sub_menu.append('<div class="sub-menu__img"></div>');

                    var sub_menu_height = $sub_menu.outerHeight();
                    var menu_height = jQuery(this).outerHeight();
                    var $elemOfMenu = jQuery(this).find('> li');
                    var countOfElemMenu = $elemOfMenu.length;

                    if (menu_height < sub_menu_height) {
                        var difference = sub_menu_height - menu_height + 4;
                        var value = difference/countOfElemMenu/2;

                        $elemOfMenu.each(function () {
                            jQuery(this).css({
                                'paddingTop': value + 'px',
                                'paddingBottom': value + 'px'
                            });
                        });
                    } else {
                        var difference = menu_height - sub_menu_height - 4;
                        $sub_menu.css({
                            'paddingBottom': parseInt($sub_menu.css('paddingBottom')) +  difference + 'px'
                        });
                    }
                }
            });
        </script>
    </section>
    <section id="search-block" class="search-block">
        <div class="inner">
            <input class="search-field" type="search" placeholder="Поиск по сайту">
        </div>
    </section>
</header>