<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
//Получение списка машин для тест-драйва
$cars = get_field('test_catalog', 'options');
?>
<footer>
    <div class="second-bg">
        <div class="inner">
            <div class="first-row">
                <div class="footer-menu">
                    <?
                    $args = array(
                        'theme_location' => 'footer-menu',
                        'menu' => '',              // (string) Название выводимого меню (указывается в админке при создании меню, приоритетнее чем указанное местоположение theme_location - если указано, то параметр theme_location игнорируется)
                        'container' => '',           // (string) Контейнер меню. Обворачиватель ul. Указывается тег контейнера (по умолчанию в тег div)
                        'container_class' => '',              // (string) class контейнера (div тега)
                        'menu_class' => '',          // (string) class самого меню (ul тега)
                        'echo' => true,            // (boolean) Выводить на экран или возвращать для обработки
                        'fallback_cb' => 'wp_page_menu',  // (string) Используемая (резервная) функция, если меню не существует (не удалось получить)
                    );
                    wp_nav_menu($args);
                    ?>
                </div>
                <div class="contacts">
                    <h3>ООО «АВТОВЕК»</h3>
                    <address>Екатеринбург, ул. Металлургов, 69</address>
                    <div class="phones">
                        <span>8(343) 253-00-53</span> Отдел продаж <br>
                        <span>8(343) 253-00-13</span> Техцентр
                    </div>
                </div>
            </div>
            <div class="second-row">
                <div class="social-icons">
                    <a class="vk" href="https://vk.com/avtovekekbru" title="ссылка на страницу VK" target="_blank" rel="noopener">ссылка
                        VK</a>
                    <a class="fb" href="https://www.facebook.com/AVTOVEKLADA/ " title="ссылка на страницу Facebook"
                       target="_blank" rel="noopener">ссылка Facebook</a>
                    <a class="you" href="https://www.youtube.com/user/ttrulia" title="ссылка на страницу Youtube"
                       target="_blank" rel="noopener">ссылка Youtube</a>
                    <a class="inst" href="https://www.instagram.com/avtovek_ekb_lada/"
                       title="ссылка на профиль Instagram" target="_blank" rel="noopener">ссылка Instagram</a>
                    <a class="ok" href="https://ok.ru/avtovekekb" title="ссылка на страницу Одноклассники"
                       target="_blank" rel="noopener">ссылка Одноклассники</a>
                    <a class="ps" href="https://www.periscope.tv/AVTOVEK" title="ссылка на профиль Periscope"
                       target="_blank" rel="noopener">ссылка Periscope</a>
                </div>
                <div class="developer">
                    <a href="https://flagstudio.ru/service/%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0-%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2"
                       title="Дизайн и разработка - Студия Флаг"
                    target="_blank" rel="noopener"><span>Разработка и дизайн сайта</span></a> - Студия Флаг
                </div>
            </div>
            <div class="additional-info">
                <p>
                    * - Рекомендованные розничные цены указаны с учетом максимальной выгоды по действующим акциям.
                    Информация о технических характеристиках, составе комплектаций, цветовой гамме и рекомендованных
                    розничных ценах, опубликованных на
                    сайте официального дилера LADA, носит справочный характер и ни при каких обстоятельствах не является
                    публичной офертой,
                    определяемой положениями Статьи 437 ч.2 Гражданского кодекса Российской Федерации.
                    Для получения подробной информации обращайтесь к консультантам нашего автосалона
                </p>
                <p>
                    Отправляя данные через формы обратной связи на сайте Вы соглашаетесь с нашей <a
                            href="<?= get_stylesheet_directory_uri() ?>/assets/files/politica.pdf" target="_blank"
                            title="Ознакомиться с Политикой конфиденциальности">Политикой конфиденциальности</a>
                </p>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<div class="flex_warning">
    <p>Вы пользуетесь устаревшей версией браузера. Данная версия браузера не поддерживает многие современные технологии,
        из-за чего многие страницы отображаются некорректно, а главное — на сайтах могут работать не все функции.</p>
</div>
<style>
    .flex_warning {
        display: none;
        width: 100%;
        position: fixed;
        bottom: 0;
        left: 0;
        color: white;
        text-align: center;
        background: #e05252;
        background-size: cover;
        font-size: 10px;
        padding: 14px 50px;
        line-height: 17px;
        text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.3);
        z-index: 999999;
    }
</style>

<div class="cookie-compliance">
    <div class="cookie-compliance__text">Мы используем файлы cookie, данные об IP-адресе и местоположении, разработанные
        третьими лицами для анализа событий на нашем сайте. Продолжая просмотр страниц сайта, вы принимаете условия его
        использования. Более подробные сведения можно посмотреть в <a
                href="<?= get_stylesheet_directory_uri() ?>/assets/files/politica.pdf" target="_blank"
                class="cookie-compliance__link">Политике конфиденциальности</a>.
    </div>
    <button type="button" class="cookie-compliance__btn">Подтвердить</button>
</div><!-- /.cookie-compliance -->

<section class="popup-block-test-drive hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup">
        <h2>Записаться на тест-драйв</h2>
        <p>Для записи на тест-драйв автомобиля позвоните в автоцентр по телефону <strong>(343)253-00-53</strong> или
            заполните ниже приведенную форму.</p>
        <p>Необходимые документы для тест-драйва: водительские права, паспорт.</p>
        <form class="main-form" name="testForm" method="post" onSubmit="yaCounter42970034.reachGoal('test_drive');">
            <label for="name-1">
                <input name="username" id="name-1" type="text" required autofocus>
                <span>ФИО</span>
            </label>
            <label class="half" for="email-1">
                <input name="useremail" id="email-1" type="text" required>
                <span>Email</span>
            </label>

            <label class="half" for="phone-1">
                <input name="userphone" id="phone-1" type="text" required>
                <span>Телефон</span>
            </label>
            <label>
                <select name="car" class="choose-car" required>
                    <?
                    foreach ($cars as $car) :
                        ?>
                        <option><?= $car['car'] ?></option>
                    <? endforeach; ?>
                </select>
                <span>Выберите автомобиль</span>
            </label>
            <label for="text-1">
                <span>Дополнительная информация</span>
                <textarea class="js-area" name="text" id="text-1" rows="1"> </textarea>
            </label>
            <input class="personal-data" id="personal-data" type="checkbox">
            <label class="custom-checkbox" for="personal-data">Согласен на обработку персональных данных</label>
            <input type="hidden" id="url" name="url" value="">
            <input name="testDrive" id="submit-test-drive" class="popup-submit" type="submit" value="Отправить"
                   disabled>
            <?php wp_nonce_field('testForm', 'noncetestForm'); ?>
        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-equipment hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup">
        <h2>Оставить заявку</h2>
        <p>Будем рады помочь Вам с выбором автомобиля. Оставьте заявку и мы перезвоним в течение дня.</p>
        <form class="main-form" method="post" name="complect">
            <label>
                <input name="usermodel" id="model" type="text" readonly>
                <span class="top">Вы выбрали</span>
            </label>
            <label for="name-2">
                <input name="username" id="name-2" type="text" required>
                <span>ФИО</span>
            </label>
            <label class="half" for="email-2">
                <input name="useremail" id="email-2" type="text" required>
                <span>Email</span>
            </label>

            <label class="half" for="phone-2">
                <input name="userphone" id="phone-2" type="text" required>
                <span>Телефон</span>
            </label>
            <label for="text-2">
                <span>Дополнительная информация</span>
                <textarea class="js-area" name="text" id="text-2" rows="1"> </textarea>
            </label>
            <input class="personal-data" id="personal-data-2" type="checkbox">
            <label class="custom-checkbox" for="personal-data-2">Согласен на обработку персональных данных</label>
            <input id="submit-equipment" class="popup-submit" type="submit" value="Отправить" name="subComplect"
                   disabled>
            <?php wp_nonce_field('complect', 'noncecomplect'); ?>
        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-book hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup">
        <h2>Забронировать</h2>
        <p>Будем рады помочь Вам с выбором автомобиля. Оставьте заявку и мы перезвоним в течение дня.</p>
        <form class="main-form" method="post" name="book" onSubmit="yaCounter42970034.reachGoal('trade_in');">
            <label>
                <input name="book-usermodel" id="book-model" type="text" readonly>
                <span class="top">Вы выбрали</span>
            </label>
            <label class="half">
                <input name="book-year" id="book-year" type="text" readonly>
                <span class="top">Год выпуска</span>
            </label>
            <label class="half">
                <input name="book-mileage" id="book-mileage" type="text" readonly>
                <span class="top">Пробег</span>
            </label>
            <label>
                <input name="book-cost" id="book-cost" type="text" readonly>
                <span class="top">Цена</span>
            </label>
            <label for="book-name">
                <input name="book-username" id="book-name" type="text" required>
                <span>ФИО</span>
            </label>
            <label class="half" for="book-email">
                <input name="book-useremail" id="book-email" type="text" required>
                <span>Email</span>
            </label>

            <label class="half" for="book-phone">
                <input name="book-userphone" id="book-phone" type="text" required>
                <span>Телефон</span>
            </label>
            <label for="book-text">
                <span>Дополнительная информация</span>
                <textarea class="js-area" name="book-text" id="book-text" rows="1"> </textarea>
            </label>
            <input class="personal-data" id="book-personal-data" type="checkbox">
            <label class="custom-checkbox" for="book-personal-data">Согласен на обработку персональных данных</label>
            <input id="submit-book" class="popup-submit" type="submit" value="Отправить" name="subBook"
                   disabled>
        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-repair hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup repair-popup">
        <h2>Записаться на ремонт, ТО</h2>
        <p>Для записи на прохождение планового технического обслуживания автомобиля или на ремонт позвоните в автоцентр
            по телефону<br> <strong>(343)253-00-13</strong> или заполните ниже приведенную форму.</p>
        <p>Необходимые документы для прохождения ТО ЛАДА: сервисная книжка, гарантийный талон.</p>
        <form class="main-form repair-form" name="repair" method="post" onSubmit="yaCounter42970034.reachGoal('to');">
            <label for="name-3">
                <input name="username" id="name-3" type="text" required>
                <span>ФИО</span>
            </label>
            <label class="half" for="email-3">
                <input name="useremail" id="email-3" type="text" required>
                <span>Email</span>
            </label>

            <label class="half" for="phone-3">
                <input name="userphone" id="phone-3" type="text" required>
                <span>Телефон</span>
            </label>

            <label class="half" for="date">
                <input name="userdate" id="date" type="text" required>
                <span>Дата визита</span>
            </label>

            <label class="half" for="time">
                <input name="usertime" id="time" type="text" required>
                <span>Время визита</span>
            </label>

            <label for="type">
                <input name="car" id="type" type="text" required>
                <span>Марка и модель автомобиля</span>
            </label>

            <label for="text-3">
                <span>Необходимые работы и дополнительная информация</span>
                <textarea class="js-area" name="jobs" id="text-3" rows="1"> </textarea>
            </label>
            <input class="personal-data" id="personal-data-3" type="checkbox">
            <label class="custom-checkbox" for="personal-data-3">Согласен на обработку персональных данных</label>
            <input id="submit-repair" class="popup-submit" type="submit" value="Отправить" name="subRepair" disabled>
            <?php wp_nonce_field('repair', 'noncerepair'); ?>
        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-call hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup call-popup">
        <h2>Заказать звонок</h2>
        <form class="main-form repair-form" enctype="multipart/form-data" method="post" action="/wp-content/themes/flagstudio/formsender.php">
            <label for="name-5">
                <input name="username" id="name-5" type="text" required>
                <span>ФИО</span>
            </label>

            <label for="phone-5">
                <input name="userphone" id="phone-5" type="text" required>
                <span>Телефон</span>
            </label>
            <input class="personal-data" id="personal-data-5" type="checkbox">
            <label class="custom-checkbox" for="personal-data-5">Согласен на обработку персональных данных</label>
            <input id="submit-call" class="popup-submit" type="submit" value="Отправить" name="callMe" disabled>
	        <?php wp_nonce_field('callMe', 'callMeEnc'); ?>

        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-offer hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup offer-popup">
        <h2>Специальное предложение</h2>
        <p><b>Просто заполни форму</b></p>
        <form autocomplete="off" class="main-form offer-form" enctype="multipart/form-data" method="post" action="/wp-content/themes/flagstudio/formsender.php" onSubmit="yaCounter42970034.reachGoal('Forma_specuha');">
            <label for="name-7">
                <input name="username" id="name-7" type="text" required>
                <span>ФИО</span>
            </label>

            <label for="phone-7">
                <input name="userphone" id="phone-7" type="text" required>
                <span>Телефон</span>
            </label>
            <input class="personal-data" id="personal-data-7" type="checkbox">
            <label class="custom-checkbox" for="personal-data-7">Согласен на обработку персональных данных</label>
            <input id="submit-offer" class="popup-submit submit-thin" type="submit" value="Жду звонка" name="specOffer" disabled>
	        <?php wp_nonce_field('specOffer', 'specOfferEnc'); ?>
        </form>
        <div id="popup-timer" class="timer" data-offer-end="<?=get_field('offer_popup_date', 'options');?>" data-start="<?=get_field('offer_popup_start', 'options');?>">
            <div class="timer__item">
                <span id="js-timer-days" class="timer__number">00</span>
                <b class="timer__text">дней</b>
            </div>
            <div class="timer__item">
                <span id="js-timer-hours" class="timer__number">00</span>
                <b class="timer__text">часов</b>
            </div>
            <div class="timer__item">
                <span id="js-timer-minutes" class="timer__number">00</span>
                <b class="timer__text">минут</b>
            </div>
            <div class="timer__item">
                <span id="js-timer-seconds" class="timer__number">00</span>
                <b class="timer__text">секунд</b>
            </div>
        </div>
        <div class="offer-form__image" style="background-image: url(<?=get_field('offer_popup_image', 'options');?>)"></div>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-alert hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup alert-popup">
        <h2>Подождите уходить</h2>
        <p><b><?=get_field('popup_wait_text', 'options');?></b></p>
        <form autocomplete="off" class="main-form alert-form" enctype="multipart/form-data" method="post" action="/wp-content/themes/flagstudio/formsender.php" onSubmit="yaCounter42970034.reachGoal('wait');">
            <label for="name-6">
                <input name="username" id="name-6" type="text" required>
                <span>ФИО</span>
            </label>

            <label for="phone-6">
                <input name="userphone" id="phone-6" type="text" required>
                <span>Телефон</span>
            </label>
            <label for="auto">
                <span>Модель автомобиля</span>
                <input class="js-area" name="auto" id="auto" type="text">
            </label>
            <input class="personal-data" id="personal-data-6" type="checkbox">
            <label class="custom-checkbox" for="personal-data-6">Согласен на обработку персональных данных</label>
            <input id="submit-alert" class="popup-submit submit-thin" type="submit" name="dontLeave" value="Жду звонка" disabled>
	        <?php wp_nonce_field('dontLeave', 'dontLeaveEnc'); ?>
        </form>
        <div class="alert-form__image-wrap">
            <img src="<?=get_stylesheet_directory_uri()?>/assets/img/alert-image.jpg" width="241" height="420">
        </div>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-service-fast hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup service-fast-popup">
        <h2>Заявка на техническое обслуживание</h2>
        <form class="main-form" method="post" name="service-request">
            <label>
                <input name="model" id="service-fast-model" type="text" required>
                <span class="top">Модель автомобиля</span>
            </label>
            <label for="service-name">
                <input name="service-username" id="service-fast-name" type="text" required>
                <span>ФИО</span>
            </label>
            <label class="half" for="service-fast-email">
                <input name="service-useremail" id="service-fast-email" type="text" required>
                <span>Email</span>
            </label>
            <label class="half" for="service-phone">
                <input name="service-userphone" id="service-fast-phone" type="text" required>
                <span>Телефон</span>
            </label>

            <input class="personal-data" id="service-fast-personal-data" type="checkbox">
            <label class="custom-checkbox" for="service-fast-personal-data">Согласен на обработку персональных данных</label>
            <input id="submit-service-fast" class="popup-submit" type="submit" value="Отправить" name="subFastService"
                   disabled>
        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-service-request hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup service-request-popup">
        <h2>Заявка на техническое обслуживание</h2>
        <form class="main-form" method="post" name="service-request">
            <label>
                <input name="model" id="service-model" type="text" readonly>
                <span class="top">Модель</span>
            </label>
            <label>
                <input name="equipment" id="service-equipment" type="text" readonly>
                <span class="top">Комплектация</span>
            </label>
            <label>
                <input name="type" id="service-type" type="text" readonly>
                <span class="top">Тип технического обслуживания</span>
            </label>
            <label for="service-name">
                <input name="service-username" id="service-name" type="text" required>
                <span>ФИО</span>
            </label>
            <label class="half" for="service-email">
                <input name="service-useremail" id="service-email" type="text" required>
                <span>Email</span>
            </label>
            <label class="half" for="service-phone">
                <input name="service-userphone" id="service-phone" type="text" required>
                <span>Телефон</span>
            </label>

            <input class="personal-data" id="service-personal-data" type="checkbox">
            <label class="custom-checkbox" for="service-personal-data">Согласен на обработку персональных данных</label>
            <input id="submit-service" class="popup-submit" type="submit" value="Отправить" name="subService"
                   disabled>
        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-materials-1 hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup materials-popup">
        <div class="materials-popup__item">
            <h2>Перечень материалов, подлежащих замене на ТО-1</h2>
            <p>
                Стоимость указана с учетом применения при проведении ТО моторного масла LADA ULTRA 5W-40.
            </p>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
                <li>
                    <p>моторное масло</p>
                </li>
                <li>
                    масляный фильтр
                </li>
            </ul>
            <p class="materials-popup__item-note">
                запасные части, рабочие жидкости, хладагент и работы по замене (доливке/дозаправке) оплачиваются потребителем дополнительно
            </p>
        </div>
        <div class="materials-popup__item">
            <h2>Регламентные работы</h2>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
            </ul>
        </div>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-materials-2 hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup materials-popup">
        <div class="materials-popup__item">
            <h2>Перечень материалов, подлежащих замене на ТО-2</h2>
            <p>
                Стоимость указана с учетом применения при проведении ТО моторного масла LADA ULTRA 5W-40.
            </p>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
                <li>
                    <p>моторное масло</p>
                </li>
                <li>
                    масляный фильтр
                </li>
            </ul>
            <p class="materials-popup__item-note">
                запасные части, рабочие жидкости, хладагент и работы по замене (доливке/дозаправке) оплачиваются потребителем дополнительно
            </p>
        </div>
        <div class="materials-popup__item">
            <h2>Регламентные работы</h2>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
            </ul>
        </div>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-materials-3 hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup materials-popup">
        <div class="materials-popup__item">
            <h2>Перечень материалов, подлежащих замене на ТО-3</h2>
            <p>
                Стоимость указана с учетом применения при проведении ТО моторного масла LADA ULTRA 5W-40.
            </p>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
                <li>
                    <p>моторное масло</p>
                </li>
                <li>
                    масляный фильтр
                </li>
            </ul>
            <p class="materials-popup__item-note">
                запасные части, рабочие жидкости, хладагент и работы по замене (доливке/дозаправке) оплачиваются потребителем дополнительно
            </p>
        </div>
        <div class="materials-popup__item">
            <h2>Регламентные работы</h2>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
            </ul>
        </div>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-materials-4 hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup materials-popup">
        <div class="materials-popup__item">
            <h2>Перечень материалов, подлежащих замене на ТО-4</h2>
            <p>
                Стоимость указана с учетом применения при проведении ТО моторного масла LADA ULTRA 5W-40.
            </p>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
                <li>
                    <p>моторное масло</p>
                </li>
                <li>
                    масляный фильтр
                </li>
            </ul>
            <p class="materials-popup__item-note">
                запасные части, рабочие жидкости, хладагент и работы по замене (доливке/дозаправке) оплачиваются потребителем дополнительно
            </p>
        </div>
        <div class="materials-popup__item">
            <h2>Регламентные работы</h2>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
            </ul>
        </div>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-materials-5 hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup materials-popup">
        <div class="materials-popup__item">
            <h2>Перечень материалов, подлежащих замене на ТО-5</h2>
            <p>
                Стоимость указана с учетом применения при проведении ТО моторного масла LADA ULTRA 5W-40.
            </p>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
                <li>
                    <p>моторное масло</p>
                </li>
                <li>
                    масляный фильтр
                </li>
            </ul>
            <p class="materials-popup__item-note">
                запасные части, рабочие жидкости, хладагент и работы по замене (доливке/дозаправке) оплачиваются потребителем дополнительно
            </p>
        </div>
        <div class="materials-popup__item">
            <h2>Регламентные работы</h2>
            <ul>
                <li>
                    <p>прокладка клапанной крышки, регулировочные шайбы клапанов (для 8-ми клапанного ДВС)</p>
                </li>
            </ul>
        </div>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<section class="popup-block-credit-fast hidden js-popup-block">
    <div class="overlay">

    </div>
    <div class="popup credit-fast-popup">
        <h2>Заявка на кредит</h2>
        <form class="main-form" method="post" name="credit-request" onSubmit="yaCounter42970034.reachGoal('kredit_bystro');">
            <label>
                <input name="model" id="credit-fast-model" type="text" required>
                <span class="top">Модель автомобиля</span>
            </label>
            <label for="service-name">
                <input name="credit-username" id="credit-fast-name" type="text" required>
                <span>ФИО</span>
            </label>
            <label class="half" for="credit-fast-email">
                <input name="credit-useremail" id="credit-fast-email" type="text" required>
                <span>Email</span>
            </label>
            <label class="half" for="credit-phone">
                <input name="credit-userphone" id="credit-fast-phone" type="text" required>
                <span>Телефон</span>
            </label>

            <input class="personal-data" id="credit-fast-personal-data" type="checkbox">
            <label class="custom-checkbox" for="credit-fast-personal-data">Согласен на обработку персональных данных</label>
            <input id="submit-credit-fast" class="popup-submit" type="submit" value="Отправить" name="subCreditService"
                   disabled>
        </form>
        <button class="close-btn" type="button">Закрыть</button>
    </div>
</section>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter42970034 = new Ya.Metrika(
                    {id: 42970034, clickmap: true, trackLinks: true, accurateTrackBounce: true}
                );
            } catch (e) {
            }
        });
        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            }
        ;
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";
        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        }
        else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=“https://www.googletagmanager.com/gtag/js?id=UA-92271588-1“></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-92271588-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->


<!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 861060547;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/861060547/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Код тега ремаркетинга Google -->

<!-- Имя шаблона в панельке WordPress -->
<?php if (current_user_can('administrator')) : ?>
    <div class="for-develop" style="position: fixed; min-width: 15%; height: 32px; top: 0; left: 750px; z-index: 100000; text-align: center; line-height: 1.8; background-color: #42b142; color: #fff;">
        <?php global $template; echo basename($template); ?>
    </div>
<?php endif; ?>

</body>
</html>
