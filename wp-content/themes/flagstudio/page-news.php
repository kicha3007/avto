<?php
/**
 * Template Name: Шаблон списка новостей (page-news.php)
 */
get_header();
$post_fields = get_fields();
$options = get_fields(options);

$args = array(
    'post_type' => 'news',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'ASC',
    'posts_per_page' => '-1'
);
$posts = new WP_Query( $args );
?>

    <main>

        <section class="sales-list-block">
            <section class="breadcrumbs">
                <div class="inner">
                    <? the_breadcrumb(); ?>
                </div>
            </section>
            <div class="inner">
                <h1 class="title"><? the_title() ?></h1>
                <?php
                while ( $posts->have_posts() ) :
                    $posts->the_post();
                    ?>
                    <a class="sales-item" href="<? the_permalink() ?>" title="<? the_title() ?>">
                      <span class="img-wrapper">
                         <?= the_post_thumbnail('full') ?>
                      </span>
                        <h2 class="link-title"><? the_title() ?></h2>
                    </a>
                <? endwhile; ?>
            </div>
        </section>
        <section class="car-types-block no-border">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>