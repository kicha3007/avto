<?php
/**
 * Template Name: Шаблон страницы б/у авто (page-used-car.php)
 */
get_header();

?>

    <main>

        <section class="breadcrumbs">
            <div class="inner">
                <? the_breadcrumb(); ?>
            </div>
        </section>
        <section class="product">
            <div class="inner">

                <div class="product__gallery" data-responsive="false">
                    <div class="gallery">
                        <div class="gallery__inner js--gallery-generate"></div>
                        <div class="swiper-container gallery__thumbs js--product-slider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide gallery__thumb" style="background-image: url('<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb1.jpg')" data-url="<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb1.jpg">
                                </div>
                                <div class="swiper-slide gallery__thumb" style="background-image: url('<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb2.jpg')" data-url="<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb2.jpg">
                                </div>
                                <div class="swiper-slide gallery__thumb" style="background-image: url('<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb3.jpg')" data-url="<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb3.jpg">
                                </div>
                                <div class="swiper-slide gallery__thumb" style="background-image: url('<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb4.jpg')" data-url="<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb4.jpg">
                                </div>
                                <div class="swiper-slide gallery__thumb" style="background-image: url('<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb5.jpg')" data-url="<?=get_stylesheet_directory_uri()?>/assets/img/gallery-thumb5.jpg">
                                </div>
                            </div>
                            <div class="swiper-button-prev gallery__prev js--product-slider-prev">
                                <svg width="14" height="14">
                                    <use xlink:href="#icon-arrow-down"/>
                                </svg>
                            </div>
                            <div class="swiper-button-next gallery__next js--product-slider-next">
                                <svg width="14" height="14" >
                                    <use xlink:href="#icon-arrow-down"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="product__description">
                    <h2 class="product-description__header">
                        Lada (ВАЗ) Kalina II Sport
                    </h2>
                    <div class="product-description__inner">
                        <span class="product-description__cost">
                        500 000 руб.
                        </span>
                        <button class="form-pickup__button request-book" data-equipment="LADA Vesta" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                    </div>
                    <span class="product-description--credit" data-credit>Этот автомобиль можно купить в кредит</span>
                    <table class="product-characteristics">
                        <tr>
                            <td class="product-characteristics__title">
                                Год выпуска
                            </td>
                            <td class="product-characteristics__item">
                                2015
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Пробег
                            </td>
                            <td class="product-characteristics__item">
                                25 000 км
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Двигатель
                            </td>
                            <td class="product-characteristics__item">
                                1.6 л / 118 л.с. / Бензин
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Цвет
                            </td>
                            <td class="product-characteristics__item">
                                Черный
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Коробка
                            </td>
                            <td class="product-characteristics__item">
                                Механическая
                            </td>
                        </tr>
                    </table>
                    <p class="product-description__text">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatu.
                    </p>
                    <p class="product-description__text">
                        Excepteur sint occaecat cupidatat non proident,explicabo.
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    </p>
                </section>

            </div>
        </section>
        <section class="sales-request">
            <div class="inner">
                <h2>Оставить заявку</h2>
                <svg class="sales-line" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     width="591px" height="276px">
                    <path fill-rule="evenodd" stroke="rgb(228, 228, 228)" stroke-width="2px" stroke-dasharray="8, 4"
                          stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                          d="M440.000,2.000 C439.000,93.000 492.000,99.000 557.000,144.000 C622.000,189.000 585.000,313.000 455.000,261.000 C325.000,209.000 256.270,87.983 116.000,126.000 C9.000,155.000 8.000,185.000 3.000,193.000 "/>
                </svg>
            </div>
            <div class="sale-form-bg sale-form--top">
                <div class="inner">
                    <form class="sale-form" name="form" method="post">
                        <input name="username" type="text" placeholder="ФИО" required>
                        <input name="userphone" type="text" placeholder="Номер телефона" required>
                        <input name="useremail" type="text" placeholder="Email" required>
                        <input type="submit" value="Отправить заявку" name="sale" id="send-request" disabled>
                        <?php wp_nonce_field('form', 'nonceform'); ?>

                        <div class="sale-form__row">
                            <input class="personal-data" type="checkbox" id="accept-credit" checked>
                            <label class="custom-checkbox" for="accept-credit">
                                Расчитать в кредит
                            </label>
                        </div>
                        <div class="sale-form__row">
                            <input class="personal-data" type="checkbox" id="personal-data-8">
                            <label class="custom-checkbox" for="personal-data-8">Согласен на обработку персональных данных</label>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <section class="searching-result-block">
            <div class="inner">
                <h2 class="searching-result-block__title">Похожие предложения</h2>
                <div class="searching-result-block__wrap">
                    <section class="searching-result-block__item">
                        <a class="searching-result-block__link" href="#!"></a>
                        <figure>
                            <ul class="searching-result-block__images js--fancybox">
                                <li class="searching-result-block__image">
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" data-fancybox="gallery1">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" alt="photo1" width="270" height="230">
                                    </a>
                                </li>
                                <li class="searching-result-block__image visually-hidden" >
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" data-fancybox="gallery1">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" alt="photo2" width="270" height="230">
                                    </a>
                                </li>
                                <li class="searching-result-block__image visually-hidden" >
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" data-fancybox="gallery1">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" alt="photo3" width="270" height="230">
                                    </a>
                                </li>
                                <li class="searching-result-block__image visually-hidden" >
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" data-fancybox="gallery1">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" alt="photo4" width="270" height="230" data-fancybox="gallery">
                                    </a>
                                </li>
                            </ul>
                            <ul class="searching-result-block__indicators">
                                <li class="searching-result-block__indicator searching-result-block__indicator--active"></li>
                                <li class="searching-result-block__indicator"></li>
                                <li class="searching-result-block__indicator"></li>
                            </ul>
                        </figure>
                        <section class="desc-block">
                            <h1 class="desc-block__title">
                                <span class="desc-block__title-text">LADA Vesta</span>
                                <span class="desc-block__title--credit">Доступна в кредит</span>
                            </h1>
                            <p class="desc-block__item">
                                <b>Двигатель:</b>
                                1.6 л 16-кл. (106 л.с.)
                            </p>
                            <p class="desc-block__item">
                                <b>Цвет:</b>
                                Зеленый
                            </p>
                            <p class="desc-block__item">
                                <b>Коробка:</b>
                                А.К.П.П.
                            </p>
                            <span class="btn dark-btn">Подробнее</span>
                        </section>
                        <section class="cost-block">
                            <ul class="cost-block__feature">
                                <li class="cost-block__feature-item">
                                    2015
                                </li>
                                <li class="cost-block__feature-item">
                                    60 000 км
                                </li>
                            </ul>
                            <b class="cost-block__cost">
                                599 000 руб.
                            </b>
                            <button class="form-pickup__button request-book" data-equipment="LADA Vesta" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                        </section>
                    </section>

                    <section class="searching-result-block__item">
                        <a class="searching-result-block__link" href="#!"></a>
                        <figure>
                            <ul class="searching-result-block__images js--fancybox">
                                <li class="searching-result-block__image">
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" data-fancybox="gallery3">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" alt="photo1" width="270" height="230">
                                    </a>
                                </li>
                                <li class="searching-result-block__image visually-hidden" >
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" data-fancybox="gallery3">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" alt="photo2" width="270" height="230">
                                    </a>
                                </li>
                            </ul>
                            <ul class="searching-result-block__indicators">
                                <li class="searching-result-block__indicator searching-result-block__indicator--active"></li>
                                <li class="searching-result-block__indicator"></li>
                                <li class="searching-result-block__indicator"></li>
                            </ul>
                        </figure>
                        <section class="desc-block">
                            <h1 class="desc-block__title">
                                <span class="desc-block__title-text">Volkswagen Touareg I</span>
                            </h1>
                            <p class="desc-block__item">
                                <b>Двигатель:</b>
                                1.6 л 16-кл. (106 л.с.)
                            </p>
                            <p class="desc-block__item">
                                <b>Цвет:</b>
                                Зеленый
                            </p>
                            <p class="desc-block__item">
                                <b>Коробка:</b>
                                А.К.П.П.
                            </p>
                            <span class="btn dark-btn">Подробнее</span>
                        </section>
                        <section class="cost-block">
                            <ul class="cost-block__feature">
                                <li class="cost-block__feature-item">
                                    2015
                                </li>
                                <li class="cost-block__feature-item">
                                    60 000 км
                                </li>
                            </ul>
                            <b class="cost-block__cost">
                                599 000 руб.
                            </b>
                            <button class="form-pickup__button request-book" data-equipment="Volkswagen Touareg I" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                        </section>
                    </section>

                    <section class="searching-result-block__item">
                        <a class="searching-result-block__link" href="#!"></a>
                        <figure>
                            <ul class="searching-result-block__images js--fancybox">
                                <li class="searching-result-block__image">
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" data-fancybox="gallery4">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img1.jpg" alt="photo1" width="270" height="230">
                                    </a>
                                </li>
                                <li class="searching-result-block__image visually-hidden" >
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" data-fancybox="gallery4">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img2.jpg" alt="photo2" width="270" height="230">
                                    </a>
                                </li>
                                <li class="searching-result-block__image visually-hidden" >
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" data-fancybox="gallery4">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img3.jpg" alt="photo3" width="270" height="230">
                                    </a>
                                </li>
                                <li class="searching-result-block__image visually-hidden" >
                                    <a href="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" data-fancybox="gallery4">
                                        <img src="<?=get_stylesheet_directory_uri()?>/assets/img/pickup-img4.jpg" alt="photo4" width="270" height="230" data-fancybox="gallery">
                                    </a>
                                </li>
                            </ul>
                            <ul class="searching-result-block__indicators">
                                <li class="searching-result-block__indicator searching-result-block__indicator--active"></li>
                                <li class="searching-result-block__indicator"></li>
                                <li class="searching-result-block__indicator"></li>
                            </ul>
                        </figure>
                        <section class="desc-block">
                            <h1 class="desc-block__title">
                                <span class="desc-block__title-text">Ford Mondeo IV</span>
                                <span class="desc-block__title--credit">Доступна в кредит</span>
                            </h1>
                            <p class="desc-block__item">
                                <b>Двигатель:</b>
                                1.6 л 16-кл. (106 л.с.)
                            </p>
                            <p class="desc-block__item">
                                <b>Цвет:</b>
                                Зеленый
                            </p>
                            <p class="desc-block__item">
                                <b>Коробка:</b>
                                А.К.П.П.
                            </p>
                            <span class="btn dark-btn">Подробнее</span>
                        </section>
                        <section class="cost-block">
                            <ul class="cost-block__feature">
                                <li class="cost-block__feature-item">
                                    2015
                                </li>
                                <li class="cost-block__feature-item">
                                    60 000 км
                                </li>
                            </ul>
                            <b class="cost-block__cost">
                                599 000 руб.
                            </b>
                            <button class="form-pickup__button request-book" data-equipment="Ford Mondeo IV" data-year="2015" data-mileage="60000" data-cost="599000" type="button">Забронировать</button>
                        </section>
                    </section>
                </div>
                <section class="searching-result-navigation">
                        <a href="#!" class="btn dark-btn btn--reverse">
                            Перейти в список автомобилей
                        </a>
                </section>
            </div>
        </section>
        <section class="car-types-block">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
        <svg style="display: none">
            <? get_template_part('/assets/img/vector-sprite'); ?>
        </svg>
    </main>
<?php get_footer(); ?>