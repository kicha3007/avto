jQuery(document).ready(function($) {
    //==================================================
    // отображение поля поиска
    // ==================================================

    $("#search-button").on('click', function () {
        $("#search-block").stop().slideToggle();
    });


    //==================================================
    // алерт для старых браузеров
    //==================================================

    if (Modernizr.flexbox && Modernizr.flexwrap) {
        $('.flex_warning').hide();
    } else {
        $('.flex_warning').show();
    }

    //==================================================
    // клик по кнопке бургер
    //==================================================

    $(window).on("resize", function () {
        if (window.innerWidth <= 480 && !$('.cmn-toggle-switch').hasClass('active')) {
            $(".nav-menu").hide();
        } else {
            //$(".nav-menu").show();
            $(".nav-menu").attr("style", "");
            $(".cmn-toggle-switch.active").removeClass("active");
        }
    }).resize();

    $(".cmn-toggle-switch").on("click", function() {
        $(this).toggleClass("active");

        $(this).siblings(".nav-menu").slideToggle(0);
        //toggleClass("show-menu");
    });

    //==================================================
    // слайдер на главной
    //==================================================

    $('#main-slider').owlCarousel({
        items: 1,
        //stagePadding: 220,
        smartSpeed: 1000,
        autoplay: true,
        autoplayTimeout: 5000,
        loop:true,
        dots: true,
        nav: true,

        // responsive:{
        //     0:{
        //         stagePadding: 60
        //     },
        //     1600:{
        //         stagePadding: 120
        //     },
        //     1700:{
        //         stagePadding: 200
        //     },
        //     1800:{
        //         stagePadding: 250
        //     },
        //     1970:{
        //         stagePadding: 0,
        //         nav: false
        //     }
        // }
    });

    //==================================================
    // карусель на главной
    //==================================================

    $('#main-carousel').owlCarousel({
        loop:true,
        autoplay: true,
        smartSpeed: 1000,
        autoplayTimeout: 3000,
        responsive:{
            0:{
                items: 2
            },
            600: {
                items: 3
            },
            900: {
                items: 3
            },
            1350:{
                items: 4
            },
            1600:{
                items: 5
            }
        }
    });

    //==================================================
    // минигалери в товарах
    //==================================================

    if($('.mini-gallery').length) {
        $('.mini-gallery .carousel').each(function () {
            $(this).owlCarousel({
                nav: true,
                responsive: {
                    0: {
                        items: 1,
                        center: true,
                    },

                    480: {
                        items: 4,
                        margin: 15,
                        center: false
                    },
                }
            });
        });

        $('.mini-gallery').each(function () {
            var container = document.createElement('div');
            $(container).addClass('mini-gallery--mobile')
                        .addClass('mini-gallery')
                        .addClass('owl-carousel');

           $(this).find('.thumbnail').each(function () {
               var src = $(this).attr('data-src');
               var elem = document.createElement('div');

               $(elem).addClass('thumbnail');
               $(elem).css('background-image', 'url("'+src+'")');
               $(container).append(elem);
           });

            $(this).parent().prepend(container);
        });


        $('.mini-gallery--mobile').each(function () {
            $(this).owlCarousel({
                items: 1,
                nav: true,
            });
        });


        $('.thumbnail').each(function () {
            $(this).on('click', function () {
                var src = $(this).attr('data-src');

                $(this).parents('.carousel').prev('.main-image').css('background-image', 'url("'+src+'")');

            });
        });
    }

    //==================================================
    // заглушка на карте на главной
    //==================================================

    $('#substrate').on('click', function () {
        $(this).hide();
        $('#map-block svg').hide();
    })


    //==================================================
    // на странице каталога переклчючение категорий
    //==================================================

    if($('.car-catalog-items').length) {
        $('.car-catalog-items .fake-input').each(function () {
            $(this).on('change', function () {
                if ($(window).width() > 480) {
                    $(this).parent().find(".wrapper-for-js").slideToggle();
                } else {
                    $(this).parent().find(".wrapper-for-js").slideToggle(0);
                }
            });
        });
    }

    //==================================================
    // задание span формы отрицательный отступ сверху,
    // в зависимости от высоты
    //==================================================

    $('.main-form span').each(function () {
       var height = $(this).height();

       $(this).css('margin-top', '-' + height * 0.8 + 'px');
    });

    //==================================================
    // на странице авто одного типа
    //==================================================

    $('.page-car').find('.wrapper-for-js').css('display', 'block');

    //==================================================
    // добавление класса active к пунктам табов на странице автомобиля
    //==================================================

    var labels = $('#tabs-menu').find('label');
    if (labels.length > 0) {
        labels.each(function () {
            $(this).on('click', function() {

                labels.each(function () {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    }
                });

                $(this).addClass('active');
            });
        });
    }
    //==================================================
    // галереи в табе на странице автомобиля
    //==================================================

    if($('.grid').length) {

        $('.grid').justifiedGallery({
            selector: '.grid-item',
            margins: 4,
            rowHeight: 200,
            maxRowHeight: 250,
            lastRow: "justify"
        });
        $('input[id="exterior"]').on('change', function () {
            $('input[id="exterior"]:checked ~ #exterior-tab .preloader').show();
        });
        $('input[id="interior"]').on('change', function () {
            $('input[id="interior"]:checked ~ #interior-tab .preloader').show();
        });

        $('.grid').justifiedGallery().on('jg.resize', function (e) {
            $(this).find('.preloader').hide();
        });
        $("[data-fancybox]").fancybox({
            buttons: [
                'close'
            ],
        });
    }


    //==================================================
    // закрепление меню на странице автомобиля при скролле и прокрутка до пункта
    //==================================================

    var menu=$("#tabs-menu");
    var tabsStart=$('#tabs-start');
    var stepsBlock=$('.steps-block');
    if(menu.length) {
        window.onscroll = function () {
            if (menu.offset().top - $(window).scrollTop() < 30 && !menu.hasClass('fixed-tabs')) {
                menu.addClass('fixed-tabs');
            }
            if (tabsStart.offset().top - menu.offset().top > 30 && menu.hasClass('fixed-tabs')) {
                menu.removeClass('fixed-tabs');
            }
            if (stepsBlock.offset().top - $(window).scrollTop() < 30 && menu.hasClass('fixed-tabs')) {
                menu.removeClass('fixed-tabs');
            }
        };

        menu.trigger('scroll');
        $('input[name="catalog-item-tab-fake-input"]').on('change', function () {
            var destination = tabsStart.offset().top - 110;
            if(menu.hasClass('fixed-tabs')) {

                if ($.browser.safari) {
                    $('body').animate({scrollTop: destination}, 1100); //1100 - скорость
                } else {
                    $('html').animate({scrollTop: destination}, 1100);
                }
            }
        });
    }

    //==================================================
    // убираем попапы на мобильниках
    //==================================================

    $(window).on('resize', function () {
        if (window.innerWidth <= 480) {
            $('.js-popup-block').each(function () {
                if ($(this).hasClass('popup-block-test-drive')
                    || $(this).hasClass('popup-block-repair')
                    || $(this).hasClass('popup-block-equipment')
                    || $(this).hasClass('popup-block-call')
                    || $(this).hasClass('popup-block-book')
                    || $(this).hasClass('popup-block-materials-1')
                    || $(this).hasClass('popup-block-materials-2')
                    || $(this).hasClass('popup-block-materials-3')
                    || $(this).hasClass('popup-block-materials-4')
                    || $(this).hasClass('popup-block-materials-5')
                    || $(this).hasClass('popup-block-service-request')
                    || $(this).hasClass('popup-block-service-fast')
                    || $(this).hasClass('popup-block-credit-fast')) {
                    return;
                }

                $(this).addClass('hidden');
                $('body').css('margin-right', 0);
                $('body').removeClass('no-scroll');
                $('#js-fixed-icons').css('margin-right', 0);
            });
        }
    }).resize();

    //==================================================
    // попапы
    //==================================================

    $('.show-test-drive-popup').on('click', function () {
        $('.popup-block-test-drive').removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.request-btn').on('click', function () {
        var model=$(this).data('equipment');
        $('#model').val(model);
        $('.popup-block-equipment').removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.request-book').on('click', function () {
        var model= $(this).data('equipment');
        var year = $(this).data('year');
        var mileage = $(this).data('mileage');
        var cost = $(this).data('cost');

        $('#book-model').val(model);
        $('#book-year').val(year + ' год');
        $('#book-mileage').val(mileage  + ' км.');
        $('#book-cost').val(cost  + ' руб.');

        $('.popup-block-book').removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.show-repair-popup').on('click', function () {
        $('.popup-block-repair').removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.show-call-popup').on('click', function () {
        $('.popup-block-call').removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.overlay').on('click', function () {
        $(this).parent().addClass('hidden');
        $("body").removeClass('no-scroll');
        $("body").css('margin-right', '0');
        $('#js-fixed-icons').css('margin-right', 0);
    });

    $('.close-btn').on('click', function () {
        $(this).parent().parent().addClass('hidden');
        $("body").removeClass('no-scroll');
        $("body").css('margin-right', '0');
        $('#js-fixed-icons').css('margin-right', 0);
    });

    $('.js--open-materials').on('click', function (e) {
        e.preventDefault();
        var index = $(this).attr('data-material-id');

        $('.popup-block-materials-' + index).removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.js--service-request').on('click', function (e) {
        e.preventDefault();

        var $popup =  $('.popup-block-service-request');

        var carcase = $('.js--data-select').find('[name=carcase]').val();
        var equipment = $('.js--data-select').find('[name=equipment]').val();
        var type = $('.service-calculator__data-cost .type').text();

        $popup.find('[name=model]').val(carcase);
        $popup.find('[name=equipment]').val(equipment);
        $popup.find('[name=type]').val(type);

        $popup.removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.js--service-fast').on('click', function (e) {
        e.preventDefault();

        $('.popup-block-service-fast').removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $('.js--credit-fast').on('click', function (e) {
        e.preventDefault();

        $('.popup-block-credit-fast').removeClass('hidden');
        $("body").css('margin-right', scrollbarWidth());
        $('#js-fixed-icons').css('margin-right', scrollbarWidth());
        $("body").addClass('no-scroll');
    });

    $(window).on('keydown', function (event) {
        if (event.keyCode === 27 && !$('.popup-block-equipment').hasClass('hidden')) {
            $('.popup-block-equipment').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-equipment').hasClass('hidden')) {
            $('.popup-block-equipment').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-repair').hasClass('hidden')) {
            $('.popup-block-repair').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-call').hasClass('hidden')) {
            $('.popup-block-call').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-offer').hasClass('hidden')) {
            $('.popup-block-offer').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-alert').hasClass('hidden')) {
            $('.popup-block-alert').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-book').hasClass('hidden')) {
            $('.popup-block-book').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-materials-1').hasClass('hidden')) {
            $('.popup-block-materials-1').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }

        if (event.keyCode === 27 && !$('.popup-block-materials-2').hasClass('hidden')) {
            $('.popup-block-materials-2').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }

        if (event.keyCode === 27 && !$('.popup-block-materials-3').hasClass('hidden')) {
            $('.popup-block-materials-3').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }

        if (event.keyCode === 27 && !$('.popup-block-materials-4').hasClass('hidden')) {
            $('.popup-block-materials-4').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }

        if (event.keyCode === 27 && !$('.popup-block-materials-5').hasClass('hidden')) {
            $('.popup-block-materials-5').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }

        if (event.keyCode === 27 && !$('.popup-block-service-request').hasClass('hidden')) {
            $('.popup-block-service-request').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-service-fast').hasClass('hidden')) {
            $('.popup-block-service-fast').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
        if (event.keyCode === 27 && !$('.popup-block-credit-fast').hasClass('hidden')) {
            $('.popup-block-credit-fast').addClass('hidden');
            $("body").removeClass('no-scroll');
            $("body").css('margin-right', '0');
            $('#js-fixed-icons').css('margin-right', 0);
        }
    });

    $('#personal-data').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-test-drive').prop('disabled', false);
        } else {
            $('#submit-test-drive').prop('disabled', true);
        }
    });
    $('#personal-data-2').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-equipment').prop('disabled', false);
        } else {
            $('#submit-equipment').prop('disabled', true);
        }
    });
    $('#personal-data-3').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-repair').prop('disabled', false);
        } else {
            $('#submit-repair').prop('disabled', true);
        }
    });
    $('#personal-data-4').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-credit').prop('disabled', false);
        } else {
            $('#submit-credit').prop('disabled', true);
        }
    });
    $('#personal-data-5').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-call').prop('disabled', false);
        } else {
            $('#submit-call').prop('disabled', true);
        }
    });
    $('#personal-data-6').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-alert').prop('disabled', false);
        } else {
            $('#submit-alert').prop('disabled', true);
        }
    });
    $('#personal-data-7').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-offer').prop('disabled', false);
        } else {
            $('#submit-offer').prop('disabled', true);
        }
    });

    $('#book-personal-data').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-book').prop('disabled', false);
        } else {
            $('#submit-book').prop('disabled', true);
        }
    });

    $('#service-personal-data').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-service').prop('disabled', false);
        } else {
            $('#submit-service').prop('disabled', true);
        }
    });

    $('#service-fast-personal-data').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-service-fast').prop('disabled', false);
        } else {
            $('#submit-service-fast').prop('disabled', true);
        }
    });

    $('#credit-fast-personal-data').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-credit-fast').prop('disabled', false);
        } else {
            $('#submit-credit-fast').prop('disabled', true);
        }
    });

    $('#credit-calculator-personal-data').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-creditCalc').prop('disabled', false);
        } else {
            $('#submit-creditCalc').prop('disabled', true);
        }
    });

    $('#submit-sell').prop('disabled', true);
    $('#agree-privacy-policy').on('change', function() {
        if($(this).prop("checked")) {
            $('#submit-sell').prop('disabled', false);
        } else {
            $('#submit-sell').prop('disabled', true);
        }
    });

    $('#personal-data-8').on('change', function() {
        if($(this).prop("checked")) {
            $('#send-request').prop('disabled', false);
        } else {
            $('#send-request').prop('disabled', true);
        }
    });

    $('.js-area').on('focus', function() {
        $(this).prev().addClass('fix-span');
    });
    $('.js-area').on('blur', function() {
        if($(this).val().length == 0) {
            $(this).prev().removeClass('fix-span');
        }
    });


    //==================================================
    // плавный скролл якорей
    //==================================================

    $(document).on('click', 'a[href^="#"]', function (e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top
        }, 777);
        e.preventDefault();
        return false;
    });

    $(document).on('click', 'a[href^="/#"]', function (e) {
        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href').slice(1)).offset().top
        }, 777);
        e.preventDefault();
        return false;
    });

    //==================================================
    // Cookie compliance
    //==================================================

    $('.cookie-compliance .cookie-compliance__btn').on('click', function() {
        $('.cookie-compliance').fadeOut();
        localStorage.setItem('politic_cookie', 'true');
    });

    if(localStorage.getItem('politic_cookie') !== "true"){
        $('.cookie-compliance').fadeIn();
    }


//==================================================
// Google ReCaptcha
//==================================================

    // $(".g-recaptcha").each(function() {
    //     var object = $(this);
    //         grecaptcha.render(object.attr("id"), {
    //             "sitekey": object.data("sitekey"),
    //             "callback": function (token) {
    //                 object.parents('form').find(".g-recaptcha-response").val(token);
    //                 object.parents('form').find('input[type=submit]').click();
    //             }
    //         });
    // });


    $(document).on('click', '.g-recaptcha', function(){
        $(this).text('Отправляю');
        $(this).addClass('button-dis');
        var t = $(this);
        setTimeout(function () {
            t.text('Отправить');
            t.removeClass('button-dis');
        },10000)
    });

    //==================================================
    // выводим баннер при попытке свалить
    //==================================================
    if (window.sessionStorage && window.innerWidth > 480) {
        $(document).on('mouseleave', function() {
            var flag = true;
            $('.js-popup-block').each(function () {
                if(!$(this).hasClass('hidden')) {
                    flag = false;
                }
            });
            if($('.popup-block-alert').hasClass('hidden') && sessionStorage.getItem('exitAlert') !== 'true' && flag && window.innerWidth > 480) {
                sessionStorage.setItem('exitAlert', 'true');
                $('.popup-block-alert').removeClass('hidden');
                $("body").css('margin-right', scrollbarWidth());
                $('#js-fixed-icons').css('margin-right', scrollbarWidth());
                $("body").addClass('no-scroll');
            }
        });
    }


    //==================================================
    // счетчик специального предложения
    //==================================================
    if (window.innerWidth > 480) {
        var startTime = $('#popup-timer').data('start'),
            dateString = $('#popup-timer').data('offer-end');

        if(!sessionStorage.getItem('show-offer') && startTime!=='' && dateString !== '') {
            var startOffer = parseInt(startTime)*1000;

            sessionStorage.setItem('startOffer', startOffer);
            setTimeout(showOffer, startOffer);
        }
    }

    //==================================================
    // включение табов на странице pickup
    //==================================================

    var $tabs = $('.form-tabs__header-item a');

    $tabs.on('click' ,function (e) {
        e.preventDefault();
        e.stopPropagation();
        var activeBody = $(this).attr('href');

        $tabs.parent().removeClass('form-tabs__header-item--active');
        $(this).parent().addClass('form-tabs__header-item--active');

        $('.form-tabs__body-item').removeClass('form-tabs__body-item--active');
        $(activeBody).addClass('form-tabs__body-item--active');
    });
    //==================================================
    // nice select для селектов
    //==================================================

    $('.js--nice-select select').niceSelect();

    //==================================================
    // jquery ui для ползунков type=range год
    //==================================================

    var $range_of_year = $('.js--range-year'),
        $data1 = $('.js--data-year');

    if ($data1.length) {
        var min_of_year = Number($data1.attr('data-from-year')) || 2005,
            max_of_year = Number($data1.attr('data-to-year')) || 2018,
            step_of_year = Number($data1.attr('data-step-year')) || 1,
            value_of_default_year = $data1.attr('data-set-default-value').split(',') || [2000, 2015];


        $range_of_year.slider({
            range: true,
            min: min_of_year,
            max: max_of_year,
            step: step_of_year,
            values: value_of_default_year,
        });
        var $handle1 = $range_of_year.find('.ui-slider-handle')[0];
        var $handle2 = $range_of_year.find('.ui-slider-handle')[1];

        var span = document.createElement('span');
        $($handle1).prepend(span);
        span = document.createElement('span');
        $($handle2).prepend(span);

        $($handle1).children('span').text($range_of_year.slider('values')[0]);
        $($handle2).children('span').text($range_of_year.slider('values')[1]);

        $range_of_year.on('slide', function(e, ui) {
            $($handle1).children('span').text(ui.values[0]);
            $($handle2).children('span').text(ui.values[1]);
        });

        var $from_year = $('.js--from-year');
        var $to_year = $('.js--to-year');

        $('input[type="number"]').inputNumber();

        $from_year[0].min = min_of_year;
        $from_year[0].max = max_of_year;
        $from_year[0].step = step_of_year;

        $to_year[0].min = min_of_year;
        $to_year[0].max = max_of_year;
        $to_year[0].step = step_of_year;

        $from_year.val($range_of_year.slider('values')[0]);
        $to_year.val($range_of_year.slider('values')[1]);

        $range_of_year.on('slidechange', function(e, ui) {
            $from_year.val(ui.values[0]);
            $to_year.val(ui.values[1]);

            $($handle1).children('span').text(ui.values[0]);
            $($handle2).children('span').text(ui.values[1]);
        });

        $from_year.on('focus', function () {
            $($handle1).addClass('ui-state-active');
        });

        $from_year.on('blur', function () {
            $($handle1).removeClass('ui-state-active');
        });

        $to_year.on('focus', function () {
            $($handle2).addClass('ui-state-active');
        });

        $to_year.on('blur', function () {
            $($handle2).removeClass('ui-state-active');
        });

        $($handle1).on('focus', function() {
            $(this).addClass('ui-state-active');
        });

        $($handle2).on('focus', function() {
            $(this).addClass('ui-state-active');
        });

        $from_year.on('change', function () {
            var value = Number($(this).val());
            var value_of_toYear = Number($to_year.val());

            if (value > value_of_toYear) {
                value = value_of_toYear;
                $(this).val(value);
            }

            var result = [value, value_of_toYear];

            $range_of_year.slider('values', result);
        });

        $from_year.on('keydown', function (e) {
            var value = Number($(this).val());
            var value_of_toYear = Number($to_year.val());

            if (value > value_of_toYear) {
                value = value_of_toYear;
            }

            var result = [value, value_of_toYear];

            if (e.which === 13) {
                $(this).val(value);
                $range_of_year.slider('values', result);
                $(this).trigger('blur');
            }
        });

        $to_year.on('change', function () {
            var value = Number($(this).val());
            var value_of_fromYear = Number($from_year.val());

            if (value < value_of_fromYear) {
                value = value_of_fromYear;
                $(this).val(value);
            }

            var result = [value_of_fromYear, value];
            $range_of_year.slider('values', result);
        });

        $to_year.on('keydown', function (e) {
            var value = Number($(this).val());
            var value_of_fromYear = Number($from_year.val());

            if (value < value_of_fromYear) {
                value = value_of_fromYear;
            }

            var result = [value_of_fromYear, value];

            if (e.which === 13) {
                $(this).val(value);
                $range_of_year.slider('values', result);
                $(this).trigger('blur');
            }
        });
    }


    //==================================================
    // jquery ui для ползунков type=range стоимость
    //==================================================

    var $range_of_cost = $('.js--range-cost'),
        $data2 = $('.js--data-cost');

    if ($data2.length) {
        var min_of_cost = Number($data2.attr('data-from-cost')) || 50000,
            max_of_cost = Number($data2.attr('data-to-cost')) || 1200000,
            step_of_cost = Number($data2.attr('data-step-cost')) || 1000,
            values_of_default_cost = $data2.attr('data-set-default-cost').split(',') || [100000, 522000];

        $range_of_cost.slider({
            range: true,
            min: min_of_cost,
            max: max_of_cost,
            step: step_of_cost,
            values: values_of_default_cost,
        });
        var $handle3 = $range_of_cost.find('.ui-slider-handle')[0];
        var $handle4 = $range_of_cost.find('.ui-slider-handle')[1];

        var span = document.createElement('span');
        $($handle3).prepend(span);
        span = document.createElement('span');
        $($handle4).prepend(span);

        $($handle3).children('span').text($range_of_cost.slider('values')[0]);
        $($handle4).children('span').text($range_of_cost.slider('values')[1]);

        $range_of_cost.on('slide', function(e, ui) {
            $($handle3).children('span').text(ui.values[0]);
            $($handle4).children('span').text(ui.values[1]);
        });

        var $from_cost = $('.js--from-cost');
        var $to_cost = $('.js--to-cost');

        $from_cost[0].min = min_of_cost;
        $from_cost[0].max = max_of_cost;
        $from_cost[0].step = step_of_cost;

        $to_cost[0].min = min_of_cost;
        $to_cost[0].max = max_of_cost;
        $to_cost[0].step = step_of_cost;

        $from_cost.val($range_of_cost.slider('values')[0]);
        $to_cost.val($range_of_cost.slider('values')[1]);

        $range_of_cost.on('slidechange', function(e, ui) {
            $from_cost.val(ui.values[0]);
            $to_cost.val(ui.values[1]);

            $($handle3).children('span').text(ui.values[0]);
            $($handle4).children('span').text(ui.values[1]);
        });

        $from_cost.on('focus', function () {
            $($handle3).addClass('ui-state-active');
        });

        $from_cost.on('blur', function () {
            $($handle3).removeClass('ui-state-active');
        });

        $to_cost.on('focus', function () {
            $($handle4).addClass('ui-state-active');
        });

        $to_cost.on('blur', function () {
            $($handle4).removeClass('ui-state-active');
        });

        $($handle3).on('focus', function() {
            $(this).addClass('ui-state-active');
        });

        $($handle4).on('focus', function() {
            $(this).addClass('ui-state-active');
        });

        $from_cost.on('change', function () {
            var value = Number($(this).val());
            var value_of_toCost = Number($to_cost.val());

            if (value > value_of_toCost) {
                value = value_of_toCost;
                $(this).val(value);
            }

            var result = [value, value_of_toCost];

            $range_of_cost.slider('values', result);
        });

        $from_cost.on('keydown', function (e) {
            var value = Number($(this).val());
            var value_of_toCost = Number($to_cost.val());

            if (value > value_of_toCost) {
                value = value_of_toCost;
            }

            var result = [value, value_of_toCost];

            if (e.which === 13) {
                $(this).val(value);
                $range_of_cost.slider('values', result);
                $(this).trigger('blur');
            }
        });

        $to_cost.on('change', function () {
            var value = Number($(this).val());
            var value_of_fromCost = Number($from_cost.val());

            if (value < value_of_fromCost) {
                value = value_of_fromCost;
                $(this).val(value);
            }

            var result = [value_of_fromCost, value];
            $range_of_cost.slider('values', result);
        });

        $to_cost.on('keydown', function (e) {
            var value = Number($(this).val());
            var value_of_fromCost = Number($from_cost.val());

            if (value < value_of_fromCost) {
                value = value_of_fromCost;
            }

            var result = [value_of_fromCost, value];

            if (e.which === 13) {
                $(this).val(value);
                $range_of_cost.slider('values', result);
                $(this).trigger('blur');
            }
        });
    }

    function updateYear(obj) {
        $range_of_year.slider({
            range: true,
            min: obj['min_year'],
            max: obj['max_year'],
            values: $data1.attr('data-set-default-value').split(',')
        });
    }

    function updateCost(obj) {
        $range_of_cost.slider({
            range: true,
            min: obj['min_price'],
            max: obj['max_price'],
            values: $data2.attr('data-set-default-cost').split(',')
        });
    }

    //==================================================
    // ajax запросы для страницы pickup
    //==================================================

    var $pickupSubmit = $('.js--pickup-submit');

    if ($('.form-pickup').length) {
        var $pickup_brand = $('.js--pickup-brand'),
            $pickup_model = $('.js--pickup-model');

        var stateOfRequestBrand = false;
        var stateOfURL = location.href.indexOf('?') !== -1;

        if (stateOfURL && $('.js--scrollTo').length) {
            $('html, body').animate({
                scrollTop: $('.js--scrollTo').offset().top-100
            }, 1000)
        }
        $pickup_brand.on('change', function () {
            if (stateOfRequestBrand) {
                return;
            }

            stateOfRequestBrand = true;
            $pickup_brand.next('.nice-select').addClass('disabled');
            $pickup_model.next('.nice-select').addClass('disabled');
            $pickupSubmit.attr('disabled', true);

            disableRangeFields();
            var value = $(this).val();
            if (value) {
                $.ajax({
                    dataType: "text",
                    url: "/ajax/",
                    type: "POST",
                    data: {
                        action: 'mark_change',
                        mark_id: value
                    },
                    timeout: 5000,
                    success: function (result) {
                        createModelSelect(JSON.parse(result));

                        stateOfRequestBrand = false;
                        $pickup_brand.next('.nice-select').removeClass('disabled');
                        $pickup_model.next('.nice-select').removeClass('disabled');

                        if (stateOfURL && MODEL) {
                            $pickup_model.val(MODEL[1]).trigger('change').niceSelect('update');
                        }

                    },
                    error: function (jqXHR, textStatus) {
                        if (textStatus === 'timeout') {
                            console.log(jqXHR);
                        }

                        stateOfRequestBrand = false;
                        $pickup_brand.next('.nice-select').removeClass('disabled');
                        $pickup_model.next('.nice-select').removeClass('disabled');

                        enableRangeFields();
                    }
                });
            }
        });

        var stateOfRequestModel = false;

        $pickup_model.on('change', function () {
            if (stateOfRequestModel) {
                return;
            }

            stateOfRequestModel = true;
            $pickup_brand.next('.nice-select').addClass('disabled');
            $pickup_model.next('.nice-select').addClass('disabled');
            disableRangeFields();
            $pickupSubmit.attr('disabled', true);
            var value = $(this).val();
            if (value) {
                $.ajax({
                    dataType: "text",
                    url: "/ajax/",
                    type: "POST",
                    data: {
                        action: 'model_change',
                        model_id: value
                    },
                    timeout: 5000,
                    success: function (result) {
                        setToSettingsRange(JSON.parse(result));

                        stateOfRequestModel = false;
                        $pickup_brand.next('.nice-select').removeClass('disabled');
                        $pickup_model.next('.nice-select').removeClass('disabled');

                        enableRangeFields();
                        $pickupSubmit.removeAttr('disabled');

                        if (stateOfURL) {
                            updateRangeFromURL({
                                'min_year' : minYear[1],
                                'max_year': maxYear[1],
                                'min_price': minCost[1],
                                'max_price': maxCost[1]
                            });
                        }
                        stateOfURL = false;

                    },
                    error: function (jqXHR, textStatus) {
                        if (textStatus === 'timeout') {
                            console.log(jqXHR);
                        }

                        stateOfRequestModel = false;
                        $pickup_brand.next('.nice-select').removeClass('disabled');
                        $pickup_model.next('.nice-select').removeClass('disabled');

                        enableRangeFields();
                        $pickupSubmit.removeAttr('disabled');
                    }
                });
            }
        });

        function createModelSelect(obj) {
            $pickup_model.html('<option value="all">Все</option>');
            if (obj) {
                for (var i = 0; i < obj.length; i++) {
                    $pickup_model.append('<option value="' + obj[i].id + '">' + obj[i].name + '</option>');
                }
            }

            $pickup_model.removeClass('disabled').next('.nice-select').remove();
            $pickup_model.niceSelect();
        }

        function setToSettingsRange(obj) {

            $('.js--data-year').attr('data-from-year', obj['min_year'])
                .attr('data-to-year', obj['max_year'])
                .attr('data-set-default-value', (obj['min_year'] - 1) + ',' + (obj['max_year'] + 1));
            $('.js--data-cost').attr('data-from-cost', obj['min_price'])
                .attr('data-to-cost', obj['max_price'])
                .attr('data-set-default-cost', (obj['min_price']) + ',' + (obj['max_price']));

            $('.js--from-year').attr('min', obj['min_year'])
                .attr('max', obj['max_year']);
            $('.js--to-year').attr('min', obj['min_year'])
                .attr('max', obj['max_year']);

            $('.js--from-cost').attr('min', obj['min_price'])
                .attr('max', obj['max_price']);
            $('.js--to-cost').attr('min', obj['min_price'])
                .attr('max', obj['max_price']);

            updateYear(obj);
            updateCost(obj);
        }

        function disableRangeFields() {
            var $elem = $('.range').parent('.form-pickup__item');

            $elem.addClass('disabled');
            $elem.find('input').attr('disabled', 'true');
        }

        function enableRangeFields() {
            var $elem = $('.range').parent('.form-pickup__item');

            $elem.removeClass('disabled');
            $elem.find('input').removeAttr('disabled');
        }

        function updateRangeFromURL(obj) {
            $('.js--data-year').attr('data-set-default-value', (obj['min_year']) + ',' + (obj['max_year']));
            $('.js--data-cost').attr('data-set-default-cost', (obj['min_price']) + ',' + (obj['max_price']));

            $range_of_year.slider({
                range: true,
                values: $data1.attr('data-set-default-value').split(',')
            });

            $range_of_cost.slider({
                range: true,
                values: $data2.attr('data-set-default-cost').split(',')
            });
        }
        //задаем значения по умолчанию для get запроса
        if (stateOfURL) {
            var BRAND = location.href.match(/brand-name=(.+?)&/);
            var MODEL = location.href.match(/model-name=(.+?)&/);
            var minYear = location.href.match(/minYear=(.+?)&/);
            var maxYear = location.href.match(/maxYear=(.+?)&/);
            var minCost = location.href.match(/minCost=(.+?)&/);
            var maxCost = location.href.match(/maxcost=(.+?)&/);

            if (BRAND) {
                $pickup_brand.val(BRAND[1]).trigger('change').niceSelect('update');


            } else {
                $pickup_brand.val('all').trigger('change');
                $pickup_model.val('all').trigger('change');
            }

        } else {
            $pickup_brand.val('all').trigger('change');
            $pickup_model.val('all').trigger('change');
        }

        //==================================================
        // задаем вид сортировки по умолчанию
        //==================================================

        if (location.href.indexOf('?') === -1) {
            history.pushState(null, null, '?sortByPrice=asc');
        }

        //==================================================
        // вешаем прелоадер при нажатии на кнопку подобрать
        //==================================================

        var $preloader = $('.preloader');

        $pickupSubmit.on('click', function() {
           $preloader.addClass('active');
           $('body').addClass('no-scroll');
        });
        $('.js--sort-block').on('click', 'button', function() {
            $preloader.addClass('active');
            $('body').addClass('no-scroll');
        });
    }

    //==================================================
    // jquery ui для ползунков type=range калькулятор
    //==================================================

    function checkClassOfNumbers(value) {
        $serviceNumbers.each(function() {
            if (value >= $(this).text() * 1000) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    }

    var $range_of_calculatorService = $('.js--service-mileage'),
        $data_of_mileage = $('.js--data-service-mileage');

    if ($range_of_calculatorService.length) {
        $('input[type="number"]').inputNumber();

        var min_of_mileage = Number($data_of_mileage.attr('data-from-mileage')) || 1000,
            max_of_mileage = Number($data_of_mileage.attr('data-to-mileage')) || 75000,
            step_of_mileage = Number($data_of_mileage.attr('data-step-mileage')) || 1000,
            value_of_default_mileage = Number($data_of_mileage.attr('data-set-default-mileage')) || 14830;

        $range_of_calculatorService.slider({
            min: min_of_mileage,
            max: max_of_mileage,
            step: step_of_mileage,
            value: value_of_default_mileage
        });

        //генерация чисел под ползунком

        var difference = max_of_mileage - min_of_mileage;
        var countOfNumbers = 4;
        var number = difference / (countOfNumbers + 1);
        var insertNumber = number;
        var result = [];

        result.push(Math.floor(min_of_mileage / 1000));

        for (var i = 1; i <= countOfNumbers; i++) {
            result.push(Math.floor(insertNumber / 1000) + 0.5);
            insertNumber += number;
        }

        result.push(Math.floor(max_of_mileage / 1000));

        var $outputNumbers = $('.js--generate-mileage');

        result.forEach(function(item) {
            $outputNumbers.append($('<span class="service-mileage__numbers-number"></span>').text(item));

        });
        var $handle_of_service = $range_of_calculatorService.find('.ui-slider-handle');
        var $serviceNumbers = $('.service-mileage__numbers-number');
        var $input_service_mileage = $('.js--input-service-mileage');

        $input_service_mileage[0].min = min_of_mileage;
        $input_service_mileage[0].max = max_of_mileage;
        $input_service_mileage[0].step = step_of_mileage;

        $input_service_mileage.val($range_of_calculatorService.slider('value'));

        var $line_of_range = $('.js--line-of-range');

        //присваиваем значения по умолчанию
        $line_of_range.attr('style', 'width:'+$handle_of_service.attr('style').split(' ')[1]);
        checkClassOfNumbers(value_of_default_mileage);

        $range_of_calculatorService.on('slide change', function(e, ui) {
            checkClassOfNumbers(ui.value);

            $input_service_mileage.val(ui.value);
            $line_of_range.attr('style', 'width:'+$handle_of_service.attr('style').split(' ')[1]);
        });

        $range_of_calculatorService.on('slidestop', function (e, ui) {
            $line_of_range.attr('style', 'width:'+$handle_of_service.attr('style').split(' ')[1]);

            console.log('change');
        });

        $input_service_mileage.on('change', function () {
            var value = Number($(this).val());

            if (value > max_of_mileage) {
                value = max_of_mileage;
                $(this).val(value);
            }

            if (value < min_of_mileage) {
                value = min_of_mileage;
                $(this).val(value);
            }

            $range_of_calculatorService.slider('value', value);
            $line_of_range.attr('style', 'width:'+$handle_of_service.attr('style').split(' ')[1]);

            checkClassOfNumbers(value);

            console.log('change');
        });

        $input_service_mileage.on('keydown', function (e) {
            var value = Number($(this).val());

            if (e.which === 13) {
                if (value > max_of_mileage) {
                    value = max_of_mileage;
                }

                if (value < min_of_mileage) {
                    value = min_of_mileage;
                }

                $(this).val(value);
                $range_of_calculatorService.slider('value', value);
                $(this).trigger('blur');
                $line_of_range.attr('style', 'width:'+$handle_of_service.attr('style').split(' ')[1]);
            }

        });
    }


    //==================================================
    // настройка кредитного калькулятора
    //==================================================

    if ($('.credit-calculator').length) {

        function updateCostValues() {
            var $cost_in = $('.js--cost-in'),
                $per_day_in = $('.js--per-day-in'),
                $per_month_in = $('.js--per-month-in');

            var $cost_out = $('.js--cost-out'),
                $per_day_out = $('.js--per-day-out'),
                $per_month_out = $('.js--per-month-out');

            $cost_out.text($cost_in.text());
            $per_day_out.text($per_day_in.text());
            $per_month_out.text($per_month_in.text());
        }

        function createEquipSelect(str) {
            var $first = $($pickup_equipCalc[0]);

            $first.html('<option value="selected">Выберите комплектацию</option>');
            $first.append(str);

            $pickup_equipCalc.niceSelect('update');
        }

        function lockFieldsCalc() {
            $('.credit-first-payment').addClass('disabled');
            $('.credit-date').addClass('disabled');
        }

        function unlockFieldsCalc() {
            $('.credit-first-payment').removeClass('disabled');
            $('.credit-date').removeClass('disabled');
        }

        function percentToCost(percent, fullCost) {
            return fullCost / 100 * percent;
        }

        function costToPercent(cost, fullCost) {
            return (cost / fullCost) * 100;
        }

        $('input[type="number"]').inputNumber();

        var $pickup_modelCalc = $('.js--credit-model'),
            $pickup_equipCalc = $('.js--credit-equip'),
            $cost = $('.js--auto-cost'),
            $image = $('.js--credit-image');

        var stateOfRequestModelCalc = false;

        var COST_AUTO = Number($('.js--auto-cost').text().trim().replace(' ', ''));

        addRangeOfPayment();
        addRangeOfCreditDate();

        $pickup_modelCalc.on('change', function () {
            if (stateOfRequestModelCalc) {
                return;
            }

            stateOfRequestModelCalc = true;

            var value = $(this).val();
            if (value) {
                $.ajax({
                    dataType: "text",
                    url: "/ajax/",
                    type: "POST",
                    data: {
                        action: 'get_complect_for_credit_calc',
                        model_id: value
                    },
                    timeout: 5000,
                    success: function (result) {
                        createEquipSelect(result);
                        updateCostValues();
                        stateOfRequestModelCalc = false;
                    },
                    error: function (jqXHR, textStatus) {
                        if (textStatus === 'timeout') {
                            console.log(jqXHR);
                        }
                    }
                });
            }
        });

        $pickup_equipCalc.on('change', function() {
            var text = $(this).val(),
                elem = $($(this)[0]).find('option:contains("' + text + '")')[0],
                price = elem.dataset.price,
                img = elem.dataset.photo;

                $('.js--load-image').addClass('active');
                var photo = new Image();
                photo.src = img;

                photo.onload = function() {
                    setTimeout(() => {
                        $('.js--load-image').removeClass('active');

                        $image.fadeOut(0)
                            .attr('src', img)
                            .fadeIn(600);
                        }, 1000);
                }

                $cost.text(price);

                updateCostValues();
                unlockFieldsCalc();
                updateSlider();

                function updateSlider() {
                    var val = Number(price.replace(/\s/g, ''));
                    COST_AUTO = val;

                    $('.js--credit-payment').slider({
                        min: 0,
                        max: 100,
                        value: 0
                    }).trigger('slidestop');


                    $('.js--input-credit-payment').val(percentToCost(0, val));
                    $('.js--input-credit-payment').prop('max', val);

                    $('.js--input-credit-payment').trigger('change');


                }
            });
    }

    //==================================================
    // переключаем сортировку по клику
    //==================================================

    $('.js--sort-block').on('click', function (e) {
       var target = $(e.target).parent('.sorting-block__item');

        var currentURL = location.href;
        var str;

       if ($(target).hasClass('sorting-block__item--arrow-up')) {
           if ($(target).hasClass('price')) {
               str = 'sortByPrice=asc';
           } else {
               str = 'sortByYear=asc';
           }
       } else if ($(target).hasClass('sorting-block__item--arrow-down')) {
           if ($(target).hasClass('price')) {
               str = 'sortByPrice=desc'
           } else {
               str = 'sortByYear=desc';
           }
       } else {
           str = 'sortByPrice=asc';
       }

        if (currentURL.indexOf('&sortBy') !== -1) {
            currentURL = currentURL.slice(0, currentURL.indexOf('&sortBy'));
            currentURL += '&' + str;
        } else if (currentURL.indexOf('?sortBy') !== -1){
            currentURL = currentURL.slice(0, currentURL.indexOf('?sortBy'));
            currentURL += '?' + str;
        }

        //history.pushState(null, null, currentURL);
        document.location.href = currentURL;

       if ($(target).hasClass('sorting-block__item--active')) {
           return;
       } else {
           $(target).siblings('.sorting-block__item--active').removeClass('sorting-block__item--active');
           $(target).addClass('sorting-block__item--active');
       }
    });

    //==================================================
    // jquery ui для ползунков type=range первоначальный взнос
    //==================================================

    function addRangeOfPayment() {
        function checkClassOfPaymentNumbers(value, arr) {
            arr.each(function() {
                if ( value>= $(this).text() ) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        }

        function percentToCost(percent, fullCost) {
            return fullCost / 100 * percent;
        }

        function costToPercent(cost, fullCost) {
            return (cost / fullCost) * 100;
        }

        function updateCostValues() {
            var $cost_in = $('.js--cost-in'),
                $per_day_in = $('.js--per-day-in'),
                $per_month_in = $('.js--per-month-in');

            var $cost_out = $('.js--cost-out'),
                $per_day_out = $('.js--per-day-out'),
                $per_month_out = $('.js--per-month-out');

            $cost_out.text($cost_in.text());
            $per_day_out.text($per_day_in.text());
            $per_month_out.text($per_month_in.text());
        }

        function updateCrediting() {
            var CREDIT_TERM = Number($('.js--input-credit-date').val()),
                LOAN_RATE = 9.9,
                RATE_PER_MONTH = (+(LOAN_RATE / 12).toFixed(10))/100,
                ANNUITY = (RATE_PER_MONTH * Math.pow(1 + RATE_PER_MONTH, CREDIT_TERM)) / (Math.pow(1 + RATE_PER_MONTH, CREDIT_TERM) - 1),
                VALUE = Number($('.js--auto-cost').text().replace(/\s/g, '')),
                DEPOSIT = $('.js--input-credit-payment').val(),
                PAYMENT_PER_MONTH = Math.floor(ANNUITY * (VALUE - DEPOSIT)),
                PAYMENT_PER_DAY = Math.floor(PAYMENT_PER_MONTH / 30);

            $per_day.text(PAYMENT_PER_DAY);
            $per_month.text(PAYMENT_PER_MONTH);

            COST_AUTO = VALUE;

            updateCostValues();
            $('.js--input-credit-payment')[0].max = VALUE;

            //update data width form

            $('.js--cost-form').attr('value', VALUE);
            $('.js--per-day-form').attr('value', PAYMENT_PER_DAY);
            $('.js--per-month-form').attr('value', PAYMENT_PER_MONTH);
        }

        var $range_of_calculatorPayment = $('.js--credit-payment'),
            $data_of_payment = $('.js--data-credit-payment');

        if ($range_of_calculatorPayment.length) {
            lockFieldsCalc();

            var min_of_payment = Number($data_of_payment.attr('data-from-payment')) || 0,
                max_of_payment = Number($data_of_payment.attr('data-to-payment')) || 100,
                step_of_payment = Number($data_of_payment.attr('data-step-payment')) || 1,
                value_of_default_payment = Number($data_of_payment.attr('data-set-default-payment'));

            //$('.js--cost-form').attr('value', COST_AUTO);

            $range_of_calculatorPayment.slider({
                min: min_of_payment,
                max: max_of_payment,
                step: step_of_payment,
                value: value_of_default_payment
            });

            //настройка формул расчета ежедневного/ежемесячного платежей

            var $per_day = $('.js--per-day-in'),
                $per_month = $('.js--per-month-in');

            updateCrediting();

            //генерация чисел под ползунком

            var difference = max_of_payment - min_of_payment;
            var countOfNumbers = 4;
            var number = difference / (countOfNumbers + 1);
            var insertNumber = number;
            var result = [];

            result.push(min_of_payment);

            for (var i = 1; i <= countOfNumbers; i++) {
                result.push(insertNumber);
                insertNumber += number;
            }

            result.push(max_of_payment);

            var $outputNumbers = $('.js--generate-payment');

            result.forEach(function(item) {
                $outputNumbers.append($('<span class="credit-payment__numbers-number"></span>').text(item));

            });
            var $handle_of_payment = $range_of_calculatorPayment.find('.ui-slider-handle');
            var $paymentNumbers = $('.credit-payment__numbers-number');
            var $input_credit_payment = $('.js--input-credit-payment');

            $input_credit_payment[0].min = percentToCost(min_of_payment, COST_AUTO);
            $input_credit_payment[0].max = percentToCost(max_of_payment, COST_AUTO);
            $input_credit_payment[0].step = step_of_payment;

            $input_credit_payment.val(percentToCost($range_of_calculatorPayment.slider('value'), COST_AUTO));

            var $line_of_range = $outputNumbers.next('.js--line-of-range');

            //присваиваем значения по умолчанию
            $line_of_range.attr('style', 'width:'+$handle_of_payment.attr('style').split(' ')[1]);
            checkClassOfPaymentNumbers(value_of_default_payment, $paymentNumbers);

            $range_of_calculatorPayment.on('slide change', function(e, ui) {
                checkClassOfPaymentNumbers(ui.value, $paymentNumbers);

                $input_credit_payment.val(percentToCost(ui.value, COST_AUTO));
                $line_of_range.attr('style', 'width:'+$handle_of_payment.attr('style').split(' ')[1]);
            });

            $range_of_calculatorPayment.on('slidestop', function (e, ui) {
                $line_of_range.attr('style', 'width:'+$handle_of_payment.attr('style').split(' ')[1]);

                updateCrediting();

                $input_credit_payment.attr('value', $input_credit_payment.val());
            });

            $input_credit_payment.on('change', function () {

                var value = Number($(this).val());

                if (value > percentToCost(max_of_payment, COST_AUTO)) {
                    value = percentToCost(max_of_payment, COST_AUTO);
                    $(this).val(value);
                }

                if (value < percentToCost(min_of_payment, COST_AUTO)) {
                    value = percentToCost(min_of_payment, COST_AUTO);
                    $(this).val(value);
                }

                $range_of_calculatorPayment.slider('value', costToPercent(value, COST_AUTO));
                $line_of_range.attr('style', 'width:'+$handle_of_payment.attr('style').split(' ')[1]);

                checkClassOfPaymentNumbers(costToPercent(value, COST_AUTO), $paymentNumbers);
                updateCrediting();

                $input_credit_payment.attr('value', $input_credit_payment.val());
            });

            $input_credit_payment.on('keydown', function (e) {
                var value = Number($(this).val());

                if (e.which === 13) {
                    if (value > percentToCost(max_of_payment, COST_AUTO)) {
                        value = percentToCost(max_of_payment, COST_AUTO);
                    }

                    if (value < percentToCost(min_of_payment, COST_AUTO)) {
                        value = percentToCost(min_of_payment, COST_AUTO);
                    }

                    $(this).val(value);
                    $range_of_calculatorPayment.slider('value', costToPercent(value, COST_AUTO));
                    $(this).trigger('blur');
                    $line_of_range.attr('style', 'width:'+$handle_of_payment.attr('style').split(' ')[1]);
                    checkClassOfPaymentNumbers(costToPercent(value, COST_AUTO), $paymentNumbers);
                    updateCrediting();

                    $input_credit_payment.attr('value', $input_credit_payment.val());
                }

            });
        }
    }

    //==================================================
    // jquery ui для ползунков type=range срок кредитования
    //==================================================

    function addRangeOfCreditDate() {
        function updateCostValues() {
            var $cost_in = $('.js--cost-in'),
                $per_day_in = $('.js--per-day-in'),
                $per_month_in = $('.js--per-month-in');

            var $cost_out = $('.js--cost-out'),
                $per_day_out = $('.js--per-day-out'),
                $per_month_out = $('.js--per-month-out');

            $cost_out.text($cost_in.text());
            $per_day_out.text($per_day_in.text());
            $per_month_out.text($per_month_in.text());
        }

        function updateCrediting() {
            var CREDIT_TERM = Number($('.js--input-credit-date').val()),
                LOAN_RATE = 9.9,
                RATE_PER_MONTH = (+(LOAN_RATE / 12).toFixed(10))/100,
                ANNUITY = (RATE_PER_MONTH * Math.pow(1 + RATE_PER_MONTH, CREDIT_TERM)) / (Math.pow(1 + RATE_PER_MONTH, CREDIT_TERM) - 1),
                VALUE = Number($('.js--auto-cost').text().replace(/\s/g, '')),
                DEPOSIT = $('.js--input-credit-payment').val(),
                PAYMENT_PER_MONTH = Math.floor(ANNUITY * (VALUE - DEPOSIT)),
                PAYMENT_PER_DAY = Math.floor(PAYMENT_PER_MONTH / 30);

            $per_day.text(PAYMENT_PER_DAY);
            $per_month.text(PAYMENT_PER_MONTH);

            COST_AUTO = VALUE;

            updateCostValues();

            $('.js--input-credit-payment')[0].max = VALUE;

            //update data width form

            $('.js--cost-form').attr('value', VALUE);
            $('.js--per-day-form').attr('value', PAYMENT_PER_DAY);
            $('.js--per-month-form').attr('value', PAYMENT_PER_MONTH);
        }

        function checkClassOfDateNumbers(value, arr) {
            arr.each(function() {
                if ( value >= $(this).text() ) {
                    $(this).addClass('active');
                } else {
                    $(this).removeClass('active');
                }
            });
        }

        var $per_day = $('.js--per-day-in'),
            $per_month = $('.js--per-month-in');

        var $range_of_calculatorDate = $('.js--credit-date'),
            $data_of_date = $('.js--data-credit-date');

        if ($range_of_calculatorDate.length) {

            var min = Number($data_of_date.attr('data-from-date')) || 6,
                max = Number($data_of_date.attr('data-to-date')) || 60,
                step = Number($data_of_date.attr('data-step-date')) || 1,
                value_of_default = Number($data_of_date.attr('data-set-default-date')) || 24;

            $range_of_calculatorDate.slider({
                min: min,
                max: max,
                step: step,
                value: value_of_default
            });

            //генерация чисел под ползунком

            var difference = max - min;
            var countOfNumbers = 3;
            var number = difference / (countOfNumbers + 1);
            var insertNumber = number + min;
            var result = [];

            result.push(min);

            for (var i = 1; i <= countOfNumbers; i++) {
                result.push(Math.floor(insertNumber));
                insertNumber += number;
            }

            result.push(max);

            var $outputNumbers = $('.js--generate-date');

            result.forEach(function(item) {
                $outputNumbers.append($('<span class="credit-date__numbers-number"></span>').text(item));

            });
            var $handle = $range_of_calculatorDate.find('.ui-slider-handle');
            var $dateNumbers = $('.credit-date__numbers-number');
            var $input_credit_date = $('.js--input-credit-date');

            $input_credit_date[0].min = min;
            $input_credit_date[0].max = max;
            $input_credit_date[0].step = step;

            $input_credit_date.val($range_of_calculatorDate.slider('value'));

            var $line_of_range = $outputNumbers.next('.js--line-of-range');
            $input_credit_date.attr('value', $input_credit_date.val());

            //присваиваем значения по умолчанию
            $line_of_range.attr('style', 'width:'+$handle.attr('style').split(' ')[1]);
            checkClassOfDateNumbers(value_of_default, $dateNumbers);

            $range_of_calculatorDate.on('slide change', function(e, ui) {
                checkClassOfDateNumbers(ui.value, $dateNumbers);

                $input_credit_date.val(ui.value);
                $line_of_range.attr('style', 'width:'+$handle.attr('style').split(' ')[1]);

                $input_credit_date.attr('value', $input_credit_date.val());
            });

            $range_of_calculatorDate.on('slidestop', function (e, ui) {
                $line_of_range.attr('style', 'width:'+$handle.attr('style').split(' ')[1]);

                updateCrediting();

                $input_credit_date.attr('value', $input_credit_date.val());
            });

            $input_credit_date.on('change', function () {
                var value = Number($(this).val());

                if (value > max) {
                    value = max;
                    $(this).val(value);
                }

                if (value < min) {
                    value = min;
                    $(this).val(value);
                }

                $range_of_calculatorDate.slider('value', value);
                $line_of_range.attr('style', 'width:'+$handle.attr('style').split(' ')[1]);

                checkClassOfDateNumbers(value, $dateNumbers);

                updateCrediting();

                $input_credit_date.attr('value', $input_credit_date.val());
            });

            $input_credit_date.on('keydown', function (e) {
                var value = Number($(this).val());

                if (e.which === 13) {
                    if (value > max) {
                        value = max;
                    }

                    if (value < min) {
                        value = min;
                    }

                    $(this).val(value);
                    $range_of_calculatorDate.slider('value', value);
                    $(this).trigger('blur');
                    $line_of_range.attr('style', 'width:'+$handle.attr('style').split(' ')[1]);

                    checkClassOfDateNumbers(value, $dateNumbers);
                    updateCrediting();

                    $input_credit_date.attr('value', $input_credit_date.val());
                }

            });
        }
    }

    //==================================================
    // включение fancybox для галереи на странице pickup
    //==================================================

    $('.js--fancybox a').fancybox({
        arrows: true,
        infobar: false,
        buttons: [
            "zoom",
            "close",
        ],
        animationEffect: "fade",

        afterClose : function (instance, current) {
            // console.log(instance, current);
            // console.log($('[href=' + current.src + ']'));
            // $($(this).parent('figure').find('.searching-result-block__indicator').removeClass('searching-result-block__indicator--active')[instance.currIndex]).addClass('searching-result-block__indicator--active');
        }
    });

    //==================================================
    // настройка галлереи на pickup странице
    //==================================================

    $('.searching-result-block__item').each(function () {
        var $indicators = $(this).find('.searching-result-block__indicators').html('');

        $(this).find('.searching-result-block__image').each(function (index) {
            if (index === 0) {
                $($indicators).append('<li class="searching-result-block__indicator searching-result-block__indicator--active"></li>');
            } else {
                $($indicators).append('<li class="searching-result-block__indicator"></li>');
            }
        });
    });

    //==================================================
    // добавление валидации к форме pickup
    //==================================================

    $('.js--validate').prop('required', true);

    //==================================================
    // Инициализация галлереи слайдера
    //==================================================

    function Gallery_Slider(swiperSlide) {
        var slidesOfSwiper = $(swiperSlide + ' .gallery__thumb');
        var gallery = $('.js--gallery-generate');

        function generate(slides) {
            var index = 0;

            $(gallery).html('');

            $(slides).each(function () {
                var slide = document.createElement('a');

                $(slide).addClass('gallery__picture')
                    .attr('href', $(this).data('url'))
                    .css('backgroundImage', $(this).css('backgroundImage'))
                    .attr('data-fancybox', 'gallery')
                    .attr('data-index', index++);

                if (!$(this).hasClass('active')) {
                    $(slide).addClass('visually-hidden');
                } else {
                    $(slide).addClass('js-main-photo');
                }

                $(gallery).append(slide);
            });
        }

        this.init = function () {
            //classOfSlider
            generate(slidesOfSwiper);
            if ($(swiperSlide).length) {
                var productSlider = new Swiper(swiperSlide, {
                    direction: 'horizontal',
                    slidesPerView: 4,
                    spaceBetween: 4,
                    loop: false,
                    speed: 500,
                    //effect: 'fade',
                    navigation: {
                        nextEl: '.js--product-slider-next',
                        prevEl: '.js--product-slider-prev',
                        hiddenClass: '.js--hide'
                    },
                    slideActiveClass: 'active',
                    centeredSlides: true,
                    //normalizeSlideIndex : false,
                    slideToClickedSlide: true,
                    // autoplay: {
                    //     delay: 2000,
                    // },j
                });

                productSlider.slideTo(Math.floor(slidesOfSwiper.length / 2));
                $.when(generate(slidesOfSwiper)).done(function () {
                    setTimeout(function () {
                        $('.js--gallery-generate').find('[data-fancybox]').fancybox({
                            buttons: ['zoom', 'close'],
                            beforeShow: function (instance) {
                                productSlider.slideTo(instance.currIndex);
                            }
                        });
                    }, 1);
                });

                productSlider.on('slideChange', function () {
                    var slideIndex = productSlider.activeIndex;
                    var gallery = $('.js--gallery-generate .gallery__picture');

                    if (gallery.length > 1) {
                        $(gallery).removeClass('js-main-photo').addClass('visually-hidden');
                        $(gallery[slideIndex]).addClass('js-main-photo').removeClass('visually-hidden');
                    } else {
                        $.when(generate(slidesOfSwiper)).done(function () {
                            setTimeout(function () {
                                $('.js--gallery-generate').find('[data-fancybox]').fancybox({
                                    buttons: ['zoom', 'close'],
                                    beforeShow: function (instance) {
                                        productSlider.slideTo(instance.currIndex);
                                    }
                                });
                            }, 1);
                        });
                    }
                });
            }
        },
            this.hideArrows = function () {
                gallery.parent().find('.gallery__prev--main').css('display', 'none');
                gallery.parent().find('.gallery__next--main').css('display', 'none');
                gallery.find('.gallery__picture:not(.js-main-photo)').remove();
            }

        function showArrows() {
            gallery.parent().find('.gallery__prev--main').css('display', 'flex');
            gallery.parent().find('.gallery__next--main').css('display', 'flex');
        }
    }

    const slider = new Gallery_Slider('.js--product-slider');
    slider.init();



//==================================================
// настройка карусели акция для кредитного калькулятора
//==================================================

    $('#credit-carousel').owlCarousel({
        loop:true,
        autoplay: true,
        smartSpeed: 1000,
        autoplayTimeout: 5000,
        items: 1
    });

//==================================================
// настройка карусели с видео
//==================================================

    var video_carousel = $('.js--swiper');
    if (video_carousel.length) {
        var swiper_videos = new Swiper('.js--swiper', {
            slidesPerView: 3,
            spaceBetween: 30,
            breakpoints: {
                480: {
                    spaceBetween: 10
                }
            }
        });
    }

//==================================================
// принудительно отправляем форму
//==================================================
    var sendForm = function (formElement) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", window.location.href);
        xhr.send(new FormData(formElement));
        xhr.onload = function () {
            window.location.replace(window.location.origin+'/');
        }
    };

    // $('.credit-calculator form').on('submit', function(e) {
    //     e.preventDefault();
    //
    //     sendForm($('.credit-calculator form')[0]);
    // });

    /**
     * Формирование ссылки для car-types
     */
    $('.car-types-link').on('mousedown', function(e) {
        var url = 'http://' + document.domain + '/catalog/' + $(this).html().toLowerCase();
        $(this).attr('href', url);
    });



    if(window.innerWidth <= 1199) {
        $('.main-menu ul>li.menu-item-has-children').append('<button type="button" class="main-menu__submenu-open-btn js--open-submenu">Открыть</button>');

        $('.js--open-submenu').on('click', function (event) {
            event.stopPropagation();
            $(this).toggleClass('active');
            $(this).siblings('.sub-menu').slideToggle();
        });
    }
});


//==================================================
// счетчик специального предложения
//==================================================

var showOffer = function() {
    var flag = true;
    jQuery('.js-popup-block').each(function () {
        if(!jQuery(this).hasClass('hidden')) {
            var offerTimeOffset = parseInt(sessionStorage.getItem('startOffer'));
            setTimeout(showOffer, offerTimeOffset);
            flag = false;
        }
    });
    if (flag) {
        sessionStorage.setItem('show-offer', 'true');
        var dateEnd,
            dateCurrent,
            timerValue,
            days,
            hours,
            minutes,
            seconds,
            daysElement = jQuery('#js-timer-days'),
            hoursElement = jQuery('#js-timer-hours'),
            minutesElement = jQuery('#js-timer-minutes'),
            secondsElement = jQuery('#js-timer-seconds'),
            dateEndString = jQuery('#popup-timer').data('offer-end');

        jQuery('.popup-block-offer').removeClass('hidden');
        jQuery('body').css('margin-right', scrollbarWidth());
        jQuery('#js-fixed-icons').css('margin-right', scrollbarWidth());
        jQuery('body').addClass('no-scroll');

        dateEnd = new Date(dateEndString);

        dateCurrent = Date.now();
        timerValue = dateEnd - dateCurrent;
        var seconds_timer_id = setInterval(function() {
            if (timerValue > 0) {
                days = parseInt(timerValue/(60*60*1000*24));
                if(days < 10) {
                    days = '0' + days;
                }

                hours = parseInt(timerValue/(60*60*1000))%24;
                if(hours < 10) {
                    hours = '0' + hours;
                }

                minutes = parseInt(timerValue/(60*1000))%60;
                if(minutes < 10) {
                    minutes = '0' + minutes;
                }

                seconds = parseInt(timerValue/1000)%60;
                if(seconds < 10) {
                    seconds = '0' + seconds;
                }
                timerValue-=1000;
                daysElement.text(days);
                hoursElement.text(hours);
                minutesElement.text(minutes);
                secondsElement.text(seconds);
            } else {
                daysElement.text('00');
                hoursElement.text('00');
                minutesElement.text('00');
                secondsElement.text('00');
                clearInterval(seconds_timer_id);
            }
        }, 1000);
    }

}

function scrollbarWidth() {
    var documentWidth = parseInt(document.documentElement.clientWidth);
    var windowsWidth = parseInt(window.innerWidth);

    var scrollbarWidth = windowsWidth - documentWidth;
    return scrollbarWidth;
}