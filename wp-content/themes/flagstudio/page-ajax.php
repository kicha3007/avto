<?php

if ( $_REQUEST['action'] == 'mark_change' ) {
	$args           = array(
		'taxonomy'   => "product_cat",
		'hide_empty' => 0,
	);
	$all_categories = get_terms( $args );
	if ( $_REQUEST['mark_id'] == 'all' ) {
		foreach ( $all_categories as $category ) {
			if ( $category->parent !== 0 ) {
				$models[] = [ 'name' => $category->name, 'id' => $category->term_id ];
			}
		}
	} else {
		foreach ( $all_categories as $category ) {
			if ( $category->parent == (int) $_REQUEST['mark_id'] ) {
				$models[] = [ 'name' => $category->name, 'id' => $category->term_id ];
			}
		}
	}

	echo json_encode( $models );
}

if ( $_REQUEST['action'] == 'model_change' ) {
	if ( $_REQUEST['model_id'] == 'all' ) {
		$args           = array(
			'taxonomy'   => "product_cat",
			'hide_empty' => 0,
		);
		$all_categories = get_terms( $args );
		if ( $_REQUEST['mark_id'] == 'all' ) {
			foreach ( $all_categories as $category ) {
				if ( $category->parent !== 0 ) {
					$models[] = [ 'name' => $category->name, 'id' => $category->term_id ];
				}
			}
		} else {
			foreach ( $all_categories as $category ) {
				if ( $category->parent == (int) $_REQUEST['mark_id'] ) {
					$models[] = [ 'name' => $category->name, 'id' => $category->term_id ];
				}
			}
		}
		foreach ( $models as $model ) {
			$args     = array(
				'post_type'      => 'product',
				'tax_query'      => array(
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'id',
						'terms'    => $model['id']
					),
				),
				'posts_per_page' => - 1
			);
			$products = new WP_Query( $args );
			$products = $products->posts;

			foreach ( $products as $product ) {
				$product  = wc_get_product( $product->ID );
				$years[]  = (int) $product->get_attribute( 'yaer' );
				$prices[] = (int) $product->get_price();
			}
		}
	} else {
		$args     = array(
			'post_type'      => 'product',
			'tax_query'      => array(
				array(
					'taxonomy' => 'product_cat',
					'field'    => 'id',
					'terms'    => $_REQUEST['model_id']
				),
			),
			'posts_per_page' => - 1
		);
		$products = new WP_Query( $args );
		$products = $products->posts;

		foreach ( $products as $product ) {
			$product  = wc_get_product( $product->ID );
			$years[]  = (int) $product->get_attribute( 'yaer' );
			$prices[] = (int) $product->get_price();
		}
	}

	echo json_encode( [
		'min_year'  => min( $years ),
		'max_year'  => max( $years ),
		'min_price' => min( $prices ),
		'max_price' => max( $prices )
	] );
}

if ( $_REQUEST['action'] == 'get_complect_for_credit_calc' ) {
	$complects = get_field('equipment_block', $_REQUEST['model_id']);
	$colors = get_field('car_colors', $_REQUEST['model_id']);
	$response = '';
	foreach ($complects as $complect){
		$response .= '<option data-photo="'.$colors[0]["catalog_color_image"].'" data-price="'.$complect["equipment_item"][0]["equipment-price"].'">'.$complect["equipment-header"].'</option>';
	}
	echo $response;
}

if ( $_REQUEST['action'] == 'get_body_by_mark_for_service_calc' ) {
	$cars = get_posts( array( 'numberposts' => -1, 'post_status' => 'publish', 'post_type' => 'catalog', 'post_parent' => $_REQUEST['mark_id'], 'suppress_filters' => false ));
	$response = '';
	foreach ($cars as $car){
		$response .= '<option value="'.$car->ID.'" >'.$car->post_title.'</option>';
	}
	echo $response;
}

if ( $_REQUEST['action'] == 'get_complect_by_model_for_service_calc' ) {
	$complects = get_field('equipment_block', $_REQUEST['model_id']);
	$tos = get_field('to-costs', $_REQUEST['model_id']);
	foreach ($complects as $complect){
		foreach ($tos as $to){
			if ($complect["equipment-header"] == $to['compl_name']){
				$response[] = [
					'name' => $complect["equipment-header"],
					'to_0' => $to["to_0"],
					'to_1' => $to["to_1"],
					'to_2' => $to["to_2"],
					'to_3' => $to["to_3"],
					'to_4' => $to["to_4"],
					'to_5' => $to["to_5"],
					'to_6' => $to["to_6"],
					'popup' => $to["popup"],
				];
			}
		}
	}
	echo serialize($response);
}