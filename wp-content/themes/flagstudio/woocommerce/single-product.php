<?php
if (isset($_REQUEST['useremail']) && wp_verify_nonce($_POST['nonceform'], 'form')){
	include_once(ABSPATH.'wp-content/themes/flagstudio/lada-mailer.php');
    $message = 'Поступила новая заявка на продажу автомобиля '.$_POST['model'].' '.$_POST['price'].' руб. '.get_permalink().'<br>
        Содержание заявки:<br>
        ФИО: '.$_POST['username'].'<br>
        Телефон: '.$_POST['userphone'].'<br>
        Email: '.$_POST['useremail'].'<br>
        ';
    if (isset($_POST['accept-credit'])) $message .= 'Рассчитать кредит: выбрано.<br>';
	$email = ladaMail();
	$email->Subject = 'Заявка на покупку автомобиля';
	$email->MsgHTML($message);
	$mails = get_field('fos_usedcars', 'options');
	foreach ($mails as $m) {
		$email->AddAddress($m['mail']);
	}
	$email->send();
	header('Location: /thank-you');
}

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;
if ( ! is_object( $product ) ) {
	$product = wc_get_product( get_the_ID() );
}
get_header();
the_post();
?>

    <main>

        <section class="breadcrumbs">
            <div class="inner">
                <ul>
                    <li><a href="/">Главная страница</a></li>
                    <li><a href="/pick-up">Подобрать автомобиль</a></li>
                    <li><? the_title() ?></li>
                </ul>
            </div>
        </section>
        <section class="product">
            <div class="inner">

                <div class="product__gallery" data-responsive="false">
                    <div class="gallery">
                        <div class="gallery__inner js--gallery-generate"></div>
                        <div class="swiper-container gallery__thumbs js--product-slider">
                            <div class="swiper-wrapper">
								<?
								$attachment_ids = $product->get_gallery_image_ids();
								if ( $attachment_ids ) {
									foreach ( $attachment_ids as $attachment_id ) {
										$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
										?>
                                        <div class="swiper-slide gallery__thumb"
                                             style="background-image: url(<?= esc_url( $full_size_image[0] ) ?>)"
                                             data-url="<?= esc_url( $full_size_image[0] ) ?>">
                                        </div>
										<?
									}
								}
								?>
                            </div>
                            <div class="swiper-button-prev gallery__prev js--product-slider-prev">
                                <svg width="14" height="14">
                                    <use xlink:href="#icon-arrow-down"/>
                                </svg>
                            </div>
                            <div class="swiper-button-next gallery__next js--product-slider-next">
                                <svg width="14" height="14">
                                    <use xlink:href="#icon-arrow-down"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="product__description">
                    <h2 class="product-description__header">
						<? the_title() ?>
                    </h2>
                    <div class="product-description__inner">
                        <span class="product-description__cost">
                            <?= number_format($product->get_price(), 0, '.', ' '); ?>
                            руб.
                        </span>
                        <button class="form-pickup__button request-book" data-equipment="<? the_title() ?>"
                                data-year="<?= $product->get_attribute( 'yaer' ) ?>"
                                data-mileage="<?= $product->get_attribute( 'km' ) ?>"
                                data-cost="<?= $product->get_price(); ?>" type="button">Забронировать
                        </button>
                    </div>
                    <? if (get_field('av_credit', $product->get_id())) {?>
                    <span class="product-description--credit" data-credit>Этот автомобиль можно купить в кредит</span>
                    <? } ?>
                    <table class="product-characteristics">
                        <tr>
                            <td class="product-characteristics__title">
                                Год выпуска
                            </td>
                            <td class="product-characteristics__item">
								<?= $product->get_attribute( 'yaer' ) ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Пробег
                            </td>
                            <td class="product-characteristics__item">
                                <?= number_format($product->get_attribute( 'km' ), 0, '.', ' '); ?>
								 км
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Двигатель
                            </td>
                            <td class="product-characteristics__item">
								<?= $product->get_attribute( 'eng_litr' ) ?> л
                                / <?= $product->get_attribute( 'eng_ls' ) ?>
                                л.с. / <?= $product->get_attribute( 'fuel' ) ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Цвет
                            </td>
                            <td class="product-characteristics__item">
								<?= $product->get_attribute( 'color' ) ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="product-characteristics__title">
                                Коробка
                            </td>
                            <td class="product-characteristics__item">
								<?= $product->get_attribute( 'transmission' ) ?>
                            </td>
                        </tr>
                    </table>
					<? the_content() ?>
                </section>

            </div>
        </section>
        <section class="sales-request">
            <div class="inner">
                <h2>Оставить заявку</h2>
                <svg class="sales-line" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     width="591px" height="276px">
                    <path fill-rule="evenodd" stroke="rgb(228, 228, 228)" stroke-width="2px" stroke-dasharray="8, 4"
                          stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                          d="M440.000,2.000 C439.000,93.000 492.000,99.000 557.000,144.000 C622.000,189.000 585.000,313.000 455.000,261.000 C325.000,209.000 256.270,87.983 116.000,126.000 C9.000,155.000 8.000,185.000 3.000,193.000 "/>
                </svg>
            </div>
            <div class="sale-form-bg sale-form--top">
                <div class="inner">
                    <form class="sale-form" name="form" method="post">
                        <input name="username" type="text" placeholder="ФИО" required>
                        <input name="userphone" type="text" placeholder="Номер телефона" required>
                        <input name="useremail" type="text" placeholder="Email" required>
                        <input type="submit" value="Отправить заявку" name="sale" id="send-request" disabled>
						<?php wp_nonce_field( 'form', 'nonceform' ); ?>
                        <input type="hidden" name="price" value="<?= $product->get_price(); ?>">
                        <input type="hidden" name="model" value="<?= $product->get_name(); ?>">
                        <div class="sale-form__row">
                            <input class="personal-data" type="checkbox" name="accept-credit" id="accept-credit" checked>
                            <label class="custom-checkbox" for="accept-credit">
                                Расчитать в кредит
                            </label>
                        </div>
                        <div class="sale-form__row">
                            <input class="personal-data" type="checkbox" id="personal-data-8">
                            <label class="custom-checkbox" for="personal-data-8">Согласен на обработку персональных
                                данных</label>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <section class="searching-result-block">
            <div class="inner">
                <?
                $cat = $product->get_category_ids()[0];
                $args     = array(
	                'post_type'      => 'product',
	                'tax_query'      => array(
		                array(
			                'taxonomy' => 'product_cat',
			                'field'    => 'id',
			                'terms'    => $cat
		                ),
	                ),
	                'posts_per_page' => 3
                );
                $products = new WP_Query( $args );
                $products = $products->posts;
                foreach ($products as $key => $product){
                    if ($product->ID == $post->ID) unset($products[$key]);
                }
                if ($products) {
                ?>
                <h2 class="searching-result-block__title">Похожие предложения</h2>
                <div class="searching-result-block__wrap">
                    <? foreach ($products as $product) {
                        $product = wc_get_product($product->ID);
	                    $attachment_ids = $product->get_gallery_attachment_ids();
                    }?>
                        <section class="searching-result-block__item">
                            <a class="searching-result-block__link"
                               href="<?= get_permalink($product->get_id()) ?>"></a>
                            <figure>
                                <ul class="searching-result-block__images js--fancybox">
					                <?
					                if ( $attachment_ids ) {
						                foreach ( $attachment_ids as $key => $attachment_id ) {
							                $full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
							                ?>
                                            <li class="searching-result-block__image <? if ( $key !== 0 ) {
								                echo 'visually-hidden';
							                } ?>">
                                                <a href="<?= esc_url( $full_size_image[0] ) ?>"
                                                   data-fancybox="gallery1">
                                                    <img src="<?= esc_url( $full_size_image[0] ) ?>"
                                                         alt="photo1" width="270" height="230">
                                                </a>
                                            </li>
							                <?
						                }
					                }
					                ?>
                                </ul>
                                <ul class="searching-result-block__indicators">
					                <? foreach ( $attachment_ids as $key => $attachment_id ) { ?>
                                        <li class="searching-result-block__indicator <? if ( $key !== 0 ) {
							                echo 'searching-result-block__indicator--active';
						                } ?>"></li>
					                <? } ?>
                                </ul>
                            </figure>
                            <section class="desc-block">
                                <h1 class="desc-block__title">
                                    <span class="desc-block__title-text"><?= $product->get_name() ?></span>
	                                <? if (get_field('av_credit', $product->get_id())) {?>
                                    <span class="desc-block__title--credit">Доступна в кредит</span>
                                    <? } ?>
                                </h1>
                                <p class="desc-block__item">
                                    <b>Двигатель:</b>
					                <?= $product->get_attribute( 'eng_litr' ) ?> л
                                    / <?= $product->get_attribute( 'eng_ls' ) ?>
                                    л.с. / <?= $product->get_attribute( 'fuel' ) ?>
                                </p>
                                <p class="desc-block__item">
                                    <b>Цвет:</b>
					                <?= $product->get_attribute( 'color' ) ?>
                                </p>
                                <p class="desc-block__item">
                                    <b>Коробка:</b>
					                <?= $product->get_attribute( 'transmission' ) ?>
                                </p>
                                <span class="btn dark-btn">Подробнее</span>
                            </section>
                            <section class="cost-block">
                                <ul class="cost-block__feature">
                                    <li class="cost-block__feature-item">
						                <?= $product->get_attribute( 'yaer' ) ?>
                                    </li>
                                    <li class="cost-block__feature-item">
						                <?= $product->get_attribute( 'km' ) ?> км
                                    </li>
                                </ul>
                                <b class="cost-block__cost">
					                <?= $product->get_price(); ?> руб.
                                </b>
                                <button class="form-pickup__button request-book"
                                        data-equipment="<? the_title() ?>"
                                        data-year="<?= $product->get_attribute( 'yaer' ) ?>"
                                        data-mileage="<?= $product->get_attribute( 'km' ) ?>"
                                        data-cost="<?= $product->get_price(); ?>" type="button">
                                    Забронировать
                                </button>
                            </section>
                        </section>
                </div>
                <? } ?>
                <section class="searching-result-navigation">
                    <a href="#!" class="btn dark-btn btn--reverse">
                        Перейти в список автомобилей
                    </a>
                </section>
            </div>
        </section>
        <section class="car-types-block">
			<? get_template_part( 'inc/car-types', 'index' ); ?>
        </section>
        <svg style="display: none">
			<? get_template_part( '/assets/img/vector-sprite' ); ?>
        </svg>
    </main>
<?php get_footer(); ?>