<div class="inner">
      <div class="car-types-items">
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/granta.png') no-repeat left bottom;"></div>
            <h3>Granta</h3>
            <ul>
                <?php
                $posts = query_posts('category=granta&post_type=catalog');
                foreach ($posts as $post){
                if ( ! get_field( 'is_cat', $post->ID ) ) {?>
                    <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
                <?}
                }?>
            </ul>
         </div>
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/kalina.png') no-repeat left bottom;"></div>
            <h3>Kalina</h3>
            <ul>
                <?php
                $posts = query_posts('category=kalina&post_type=catalog');
                foreach ($posts as $post) {
	                if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
	                <?
	                }
                }?>
            </ul>
         </div>
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/priora.png') no-repeat left bottom;"></div>
            <h3>Priora</h3>
            <ul>
                <?php
                $posts = query_posts('category=priora&post_type=catalog');
                foreach ($posts as $post) {
	                if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
	                <?
	                }
                }?>
            </ul>
         </div>
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/vesta.png') no-repeat left bottom;"></div>
            <h3>Vesta</h3>
            <ul>
                <?php
                $posts = query_posts('category=vesta&post_type=catalog');
                foreach ($posts as $post) {
	                if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
	                <?
	                }
                }?>
            </ul>
         </div>
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/xray.png') no-repeat left bottom;"></div>
            <h3>XRAY</h3>
            <ul>
                <?php
                $posts = query_posts('category=xray&post_type=catalog');
                foreach ($posts as $post) {
	                if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
	                <?
	                }
                }?>
            </ul>
         </div>
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/largus.png') no-repeat left bottom;"></div>
            <h3>Largus</h3>
            <ul>
                <?php
                $posts = query_posts('category=largus&post_type=catalog');
                foreach ($posts as $post) {
	                if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
	                <?
	                }
                }?>
            </ul>
         </div>
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/4x4.png') no-repeat left bottom;"></div>
            <h3>4x4</h3>
            <ul>
                <?php
                $posts = query_posts('category=4x4&post_type=catalog');
                foreach ($posts as $post) {
	                if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
	                <?
	                }
                }?>
            </ul>
         </div>
         <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri()?>/assets/img/commerce.png') no-repeat left bottom;"></div>
            <h3>Коммерция</h3>
            <ul>
                <?php
                $list = get_field('footer_list', 'options');
                foreach ($list as $li) {
	                if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<?= $li['link'] ?>" title="<?= $li['text'] ?>"><?= $li['text'] ?></a></li>
	                <?php }
                }?>
            </ul>
         </div>

      </div>
   </div>