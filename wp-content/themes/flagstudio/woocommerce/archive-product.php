<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( isset( $_REQUEST['brand-name'] ) && wp_verify_nonce( $_POST['noncesellcar'], 'sellcar' ) ) {
	include_once( ABSPATH . 'wp-content/themes/flagstudio/lada-mailer.php' );
	$message        = 'Поступила новая заявка на продажу автомобиля со страницы ' . get_site_url() . $_SERVER['REQUEST_URI'] . '<br>
        Содержание заявки:<br>
        Марка автомобиля: ' . $_POST['brand-name'] . '<br>
        Модель автомобиля: ' . $_POST['model-name'] . '<br>
        Пробег: ' . $_POST['mileage'] . '<br>
        Год выпуска: ' . $_POST['release-year'] . '<br>
        ФИО: ' . $_POST['name-user'] . '<br>
        Телефон: ' . $_POST['tel-user'] . '<br>
        Email: ' . $_POST['email-user'] . '<br>
        ';
	$email          = ladaMail();
	$email->Subject = 'Заявка на продажу автомобиля';
	$email->MsgHTML( $message );
	$mails = get_field( 'fos_usedcars', 'options' );
	foreach ( $mails as $m ) {
		$email->AddAddress( $m['mail'] );
	}
	$email->send();
	header( 'Location: /thank-you' );
}


//global $wpdb;
//
//$prices = $wpdb->get_col( "
//SELECT
//  wppm.meta_value AS VALUE
//FROM wp_posts AS wpp
//  LEFT JOIN wp_postmeta AS wppm
//    ON wpp.ID = wppm.post_id
//WHERE wpp.post_type = 'product'
//      AND (wppm.meta_key = 'pa_yaer'
//     )
//    " );

//$attrs = $wpdb->get_col( "
//SELECT
//  wppm.meta_value AS VALUE
//FROM wp_posts AS wpp
//  LEFT JOIN wp_postmeta AS wppm
//    ON wpp.ID = wppm.post_id
//WHERE wpp.post_type = 'product'
//      AND (wppm.meta_key = '_product_attributes'
//     )
//    " );
//
//foreach ($attrs as $attr){
//    $attributes[] = unserialize($attr);
//}

$products = wc_get_products( array( 'post_type' => 'product', 'posts_per_page' => - 1 ) );

foreach ( $products as $product ) {
	$years[]  = (int) $product->get_attribute( 'yaer' );
	$prices[] = (int) $product->get_price();
}

$args           = array(
	'taxonomy'   => "product_cat",
	'hide_empty' => 0,
);
$all_categories = get_terms( $args );

foreach ( $all_categories as $category ) {
	if ( $category->parent == 0 && $category->name !== 'Uncategorized' ) {
		$marks[] = [ 'name' => $category->name, 'id' => $category->term_id ];
	}
}
foreach ( $all_categories as $category ) {
	if ( $category->parent == $marks[0]['id'] ) {
		$models[] = [ 'name' => $category->name, 'id' => $category->term_id ];
	}
}

get_header();
?>

<main>
    <div class="preloader">
        <div class="circle circle-1"></div>
        <div class="circle circle-2"></div>
        <div class="circle circle-3"></div>
    </div>
    <section class="breadcrumbs">
        <div class="inner">
            <ul>
                <li><a href="/">Главная страница</a></li>
                <li><a href="/pick-up">Подобрать автомобиль</a></li>
            </ul>
        </div>
    </section>

    <section class="pick-up-block">
        <div class="inner">
            <h2 class="pick-up-block__title">Автомобили с пробегом</h2>
            <section class="form-tabs">
                <section class="form-tabs__header">
                    <ul>
                        <li class="form-tabs__header-item form-tabs__header-item--active">
                            <a href="#tab1">Подобрать автомобиль</a>
                        </li>
                        <li class="form-tabs__header-item">
                            <a href="#tab2">Продать автомобиль</a>
                        </li>
                    </ul>
                </section>
                <section class="form-tabs__body">
                    <section class="form-tabs__body-item form-tabs__body-item--active" id="tab1">
                        <form method="get" enctype="multipart/form-data" class="form-pickup js--nice-select">
                            <div class="form-pickup__row">
                                <div class="form-pickup__item">
                                    <h1 class="form-pickup__header">Выберите марку</h1>
                                    <select name="brand-name" id="brand" class="js--pickup-brand">
                                        <option value="all">Все</option>
										<? foreach ( $marks as $mark ) { ?>
                                            <option value="<?= $mark['id'] ?>"><?= $mark['name'] ?></option>
										<? } ?>
                                    </select>
                                </div>
                                <div class="form-pickup__item">
                                    <h1 class="form-pickup__header">Выберите модель</h1>
                                    <select name="model-name" id="model" class="js--pickup-model">
                                        <option value="all">Все</option>
										<? foreach ( $models as $model ) { ?>
                                            <option value="<?= $model['id'] ?>"><?= $model['name'] ?></option>
										<? } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-pickup__row form-pickup__row--line-b">
                                <div class="form-pickup__item">
                                    <h1 class="form-pickup__header">Год выпуска</h1>
                                    <div class="range js--range-year"></div>
                                    <input class="js--data-year" type="text" style="display: none;"
                                           data-from-year="<?= min( $years ) ?>"
                                           data-to-year="<?= max( $years ) ?>" data-step-year="1"
                                           data-set-default-value="<?= min( $years ) ?>,<?= max( $years ) ?>">
                                    <label><span>от</span><input type="number"
                                                                 class="form-pickup__input js--from-year"
                                                                 name="minYear"></label>
                                    <label><span>до</span><input type="number"
                                                                 class="form-pickup__input js--to-year"
                                                                 name="maxYear"></label>
                                </div>
                                <div class="form-pickup__item">
                                    <h1 class="form-pickup__header">Цена, руб.</h1>
                                    <div class="range js--range-cost"></div>
                                    <input class="js--data-cost" type="text" style="display: none;"
                                           data-from-cost="<?= min( $prices ) ?>"
                                           data-to-cost="<?= max( $prices ) ?>" data-step-cost="1000"
                                           data-set-default-cost="<?= min( $prices ) ?>,<?= max( $prices ) ?>">
                                    <label><span>от</span><input type="number"
                                                                 class="form-pickup__input js--from-cost"
                                                                 name="minCost"></label>
                                    <label><span>до</span><input type="number"
                                                                 class="form-pickup__input js--to-cost"
                                                                 name="maxcost"></label>
                                </div>
                            </div>
                            <button class="form-pickup__button js--pickup-submit" type="submit" >Подобрать
                            </button>
                            <? if ($_GET['sortByPrice']) echo '<input type="hidden" name="sortByPrice" value="'.$_GET['sortByPrice'].'">'?>
	                        <? if ($_GET['sortByYear']) echo '<input type="hidden" name="sortByYear" value="'.$_GET['sortByYear'].'">'?>
                        </form>
                        <section class="searching-result-block">
                            <div class="inner">
                                <h2 class="searching-result-block__title">Результаты поиска</h2>
                                <ul class="sorting-block js--sort-block">
                                    <span class="sorting-block__text">Сортировать по: </span>
                                    <li class="sorting-block__item <?
                                    if ($_GET['sortByPrice']) echo 'sorting-block__item--active';?>  sorting-block__item--arrow-<?
                                if ($_GET['sortByPrice'] == 'asc') echo 'down'; else echo 'up'   ?> price">
                                        <button>цене</button>
                                    </li>
                                    <li class="sorting-block__item <?
                                    if ($_GET['sortByYear']) echo 'sorting-block__item--active';?> sorting-block__item--arrow-<?
                                    if ($_GET['sortByYear'] == 'asc') echo 'down'; else echo 'up'   ?>">
                                        <button>году выпуска</button>
                                    </li>
                                </ul>

                                <div class="searching-result-block__wrap js--scrollTo">
									<?php
									global $wp_query;

									if ( $wp_query->have_posts() ) {
										while ( have_posts() ) : the_post();
											global $product, $post;
											$check = true;
											$cat = $product->get_category_ids()[0];
											$cat = get_term_by('id', $cat, 'product_cat', 'ARRAY_A');
											$parent = $cat['parent'];
											$cat_id = $cat['term_id'];
											if ( isset( $_GET['minYear'] ) && $product->get_attribute( 'yaer' ) < $_GET['minYear'] ) $check = false;
											if ( isset( $_GET['maxYear'] ) && $product->get_attribute( 'yaer' ) > $_GET['maxYear'] ) $check = false;
											if ( isset( $_GET['minCost'] ) && $product->get_price() < $_GET['minCost'] ) $check = false;
											if ( isset( $_GET['maxcost'] ) && $product->get_price() > $_GET['maxcost'] ) $check = false;
											if ( isset( $_GET['brand-name'] ) && $_GET['brand-name'] !== 'all' && $parent !== (int)$_GET['brand-name'] ) $check = false;
											if ( isset( $_GET['model-name'] ) && $_GET['model-name'] !== 'all' && $cat_id !== (int)$_GET['model-name'] ) $check = false;

											if ( $check ) {
												$attachment_ids = $product->get_gallery_attachment_ids();
												?>
                                                <section class="searching-result-block__item">
                                                    <a class="searching-result-block__link"
                                                       href="<?= get_permalink() ?>"></a>
                                                    <figure>
                                                        <ul class="searching-result-block__images js--fancybox">
															<?
															if ( $attachment_ids ) {
																foreach ( $attachment_ids as $key => $attachment_id ) {
																	$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
																	?>
                                                                    <li class="searching-result-block__image <? if ( $key !== 0 ) {
																		echo 'visually-hidden';
																	} ?>">
                                                                        <a href="<?= esc_url( $full_size_image[0] ) ?>"
                                                                           data-fancybox="gallery1">
                                                                            <img src="<?= esc_url( $full_size_image[0] ) ?>"
                                                                                 alt="photo1" width="310"
                                                                                 height="230">
                                                                        </a>
                                                                    </li>
																	<?
																}
															}
															?>
                                                        </ul>
                                                        <ul class="searching-result-block__indicators">
															<? foreach ( $attachment_ids as $key => $attachment_id ) { ?>
                                                                <li class="searching-result-block__indicator <? if ( $key !== 0 ) {
																	echo 'searching-result-block__indicator--active';
																} ?>"></li>
															<? } ?>
                                                        </ul>
                                                    </figure>
                                                    <section class="desc-block">
                                                        <h1 class="desc-block__title">
                                                            <span class="desc-block__title-text"><? the_title() ?></span>
															<? if ( get_field( 'av_credit', $product->get_id() ) ) { ?>
                                                                <span class="desc-block__title--credit">Доступна в кредит</span>
															<? } ?>
                                                        </h1>
                                                        <p class="desc-block__item">
                                                            <b>Двигатель:</b>
															<?= $product->get_attribute( 'eng_litr' ) ?> л
                                                            / <?= $product->get_attribute( 'eng_ls' ) ?>
                                                            л.с. / <?= $product->get_attribute( 'fuel' ) ?>
                                                        </p>
                                                        <p class="desc-block__item">
                                                            <b>Цвет:</b>
															<?= $product->get_attribute( 'color' ) ?>
                                                        </p>
                                                        <p class="desc-block__item">
                                                            <b>Коробка:</b>
															<?= $product->get_attribute( 'transmission' ) ?>
                                                        </p>
                                                        <span class="btn dark-btn">Подробнее</span>
                                                    </section>
                                                    <section class="cost-block">
                                                        <ul class="cost-block__feature">
                                                            <li class="cost-block__feature-item">
																<?= $product->get_attribute( 'yaer' ) ?>
                                                            </li>
                                                            <li class="cost-block__feature-item">
																<?= number_format($product->get_attribute( 'km' ), 0, '.', ' '); ?> км
                                                            </li>
                                                        </ul>
                                                        <b class="cost-block__cost">
                                                            <?= number_format($product->get_price(), 0, '.', ' '); ?> руб.
                                                        </b>
                                                        <button class="form-pickup__button request-book"
                                                                data-equipment="<? the_title() ?>"
                                                                data-year="<?= $product->get_attribute( 'yaer' ) ?>"
                                                                data-mileage="<?= $product->get_attribute( 'km' ) ?>"
                                                                data-cost="<?= $product->get_price(); ?>"
                                                                type="button">
                                                            Забронировать
                                                        </button>
                                                    </section>
                                                </section>
												<?
											}
											endwhile;
										} ?>
                                </div>
                                <section class="pagination">
									<? pagination(); ?>
                                </section>
                            </div>
                        </section>
                        <section class="content">
                            <div class="inner">
                                <h2 class="content-title">
                                    <svg class="pickup-svg">
                                        <path fill-rule="evenodd" stroke="rgb(229, 229, 229)" stroke-width="2px"
                                              stroke-dasharray="8, 4" stroke-linecap="butt" stroke-linejoin="miter"
                                              fill="none"
                                              d="M742.000,1469.000 C592.000,1520.000 453.000,1377.000 381.000,1355.000 C309.000,1333.000 243.000,1324.000 167.000,1410.000 C91.000,1496.000 -12.000,1428.000 3.000,1340.000 C18.000,1252.000 141.000,1226.000 143.000,1144.000 C145.000,1061.999 127.000,1034.000 116.000,967.000 C105.000,900.000 228.201,867.760 249.000,832.000 C272.028,792.405 225.000,692.999 216.000,631.000 C207.000,569.000 177.000,392.000 271.000,307.000 C365.000,222.000 543.000,283.000 564.000,281.000 C585.000,279.000 642.655,261.443 637.000,200.000 C631.178,136.735 602.241,115.647 623.000,82.000 C644.853,46.578 724.437,69.301 744.000,38.000 C756.500,18.000 754.000,2.000 754.000,2.000 "/>
                                    </svg>
                                    <?= get_field('used_car_title', 'options') ?>
                                </h2>
	                            <?= get_field('used_car_text', 'options') ?>
                            </div>
                        </section>
                        <section class="car-types-block">
		                    <? get_template_part( 'inc/car-types', 'index' ); ?>
                        </section>
                    </section>
                    <section class="form-tabs__body-item" id="tab2">
                        <form method="post" enctype="multipart/form-data" class="form-pickup form-sell">
                            <div class="form-pickup__row">
                                <label class="field-block">
                                    <span class="field-block__title">Марка*</span>
                                    <input class="js--validate" type="text" name="brand-name"
                                           placeholder="Марка автомобиля" required>
                                </label>
                                <label class="field-block">
                                    <span class="field-block__title">Модель*</span>
                                    <input class="js--validate" type="text" name="model-name"
                                           placeholder="Модель автомобиля" required>
                                </label>
                                <label class="field-block field-block--half">
                                    <span class="field-block__title">Пробег*</span>
                                    <input class="js--validate" type="number" name="mileage" min="0" value="100000"
                                           required>
                                </label>
                                <label class="field-block field-block--half">
                                    <span class="field-block__title">Год выпуска*</span>
                                    <input class="js--validate" type="number" name="release-year" min="1980"
                                           value="2010" required>
                                </label>
                            </div>
                            <div class="form-pickup__row">
                                <label class="field-block">
                                    <span class="field-block__title">Ф.И.О.*</span>
                                    <input class="js--validate" type="text" name="name-user"
                                           placeholder="Фамилия Имя Отчество" required>
                                </label>
                                <label class="field-block">
                                    <span class="field-block__title">Телефон*</span>
                                    <input class="js--validate" type="tel" name="tel-user"
                                           placeholder="+7 (999) 999-99-99" required>
                                </label>
                                <label class="field-block">
                                    <span class="field-block__title">E-mail*</span>
                                    <input class="js--validate" type="email" name="email-user"
                                           placeholder="info@sitename.com" required>
                                </label>
                            </div>
                            <div class="form-pickup__row form-pickup__row--line-b form-pickup__row--half">
                                <input class="privacy-policy__checkbox" id="agree-privacy-policy" type="checkbox">
                                <label class="custom-checkbox field-text" for="agree-privacy-policy">Отправляя
                                    заявку через форму обратной связи Вы соглашаетесь с нашей <a href="#!">Политикой
                                        конфиденциальности</a></label>
                            </div>
                            <button class="form-pickup__button form-pickup__button--theme-blue" type="submit"
                                    id="submit-sell" disabled>Отправить заявку
                            </button>
							<?php wp_nonce_field( 'sellcar', 'noncesellcar' ); ?>
                        </form>
                        <section class="content">
                            <div class="inner">
                                <h2 class="content-title">
                                    <svg class="pickup-svg">
                                        <path fill-rule="evenodd" stroke="rgb(229, 229, 229)" stroke-width="2px"
                                              stroke-dasharray="8, 4" stroke-linecap="butt" stroke-linejoin="miter"
                                              fill="none"
                                              d="M742.000,1469.000 C592.000,1520.000 453.000,1377.000 381.000,1355.000 C309.000,1333.000 243.000,1324.000 167.000,1410.000 C91.000,1496.000 -12.000,1428.000 3.000,1340.000 C18.000,1252.000 141.000,1226.000 143.000,1144.000 C145.000,1061.999 127.000,1034.000 116.000,967.000 C105.000,900.000 228.201,867.760 249.000,832.000 C272.028,792.405 225.000,692.999 216.000,631.000 C207.000,569.000 177.000,392.000 271.000,307.000 C365.000,222.000 543.000,283.000 564.000,281.000 C585.000,279.000 642.655,261.443 637.000,200.000 C631.178,136.735 602.241,115.647 623.000,82.000 C644.853,46.578 724.437,69.301 744.000,38.000 C756.500,18.000 754.000,2.000 754.000,2.000 "/>
                                    </svg>
	                                <?= get_field('sell_car_title', 'options') ?>
                                </h2>
	                            <?= get_field('sell_car_text', 'options') ?>
                            </div>
                        </section>
                        <section class="car-types-block">
							<? get_template_part( 'inc/car-types', 'index' ); ?>
                        </section>
                    </section>
                </section>
            </section>
        </div>
    </section>
</main>
<?php

get_footer();