<?php
/**
 * Created by PhpStorm.
 * User: Flag_2
 * Date: 27.12.2017
 * Time: 16:30
 */
function ladaMail(){
    include_once('class.phpmailer.php');
    $__smtp = array(
        "host" => get_field('smtp_server', 'options'), //smtp сервер
        "debug" => 0,                   //отображение информации дебаггера (0 - нет вообще)
        "auth" => true,                 //сервер требует авторизации
        "port" => get_field('smtp_port', 'options'),                    //порт (по-умолчанию - 25)
        "username" => get_field('smtp_login', 'options'),//имя пользователя на сервере
        "password" => get_field('smtp_pass', 'options'),//пароль
        "addreply" => get_field('smtp_login', 'options'),//ваш е-mail
        "replyto" => get_field('smtp_login', 'options')      //e-mail ответа
    );
    $email = new PHPMailer();
    $email->IsSMTP();
    if (gethostbyaddr($__smtp['host'])){
        $email->Host = gethostbyaddr($__smtp['host']);
    }
    else {
        $email->Host = gethostbyname($__smtp['host']);
    }
    $email->SMTPDebug  = $__smtp['debug'];
    $email->SMTPAuth   = $__smtp['auth'];
    $email->Port       = $__smtp['port'];
    $email->Username   = $__smtp['username'];
    $email->Password   = $__smtp['password'];
    $email->AddReplyTo($__smtp['addreply'], $__smtp['username']);
    $email->SetFrom($__smtp['addreply'], 'Автовек Лада'); //от кого (желательно указывать свой реальный e-mail на используемом SMTP сервере
    $email->AddReplyTo($__smtp['addreply'], $__smtp['username']);
    $email->IsHTML();
    $email->CharSet = 'UTF-8';


    return $email;
}