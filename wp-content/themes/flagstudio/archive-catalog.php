<?php
/**
 * Template Name: Шаблон каталога (page-cars.php)
 */
get_header();
$post_fields['commerce'] = get_field('commerce','options');
$categoryList = get_categories('taxonomy=category&orderby=id');
?>
    <main>
        <section class="cars-catalog">
            <div class="inner">
                <section class="breadcrumbs">
                    <ul>
                        <li><a href="/">Главная страница</a></li>
                        <li><a href="/catalog">Каталог автомобилей</a></li>
                    </ul>
                </section>

                <h1>Каталог автомобилей</h1>

                <section class="car-catalog-items">
					<? while ($categoryList) :
						$category=array_shift($categoryList);
						$args = array(
							'post_type' => 'catalog',
							'post_status' => 'publish',
							'orderby' => 'date',
							'order' => 'ASC',
							'posts_per_page' => '-1',
							'category' => $category->slug
						);
						$posts = new WP_Query( $args );
						?>

                        <div class="car-catalog-item">
                            <input class="fake-input" type="checkbox" id="car-<?=$category->term_id?>">
                            <label class="content-header" for="car-<?=$category->term_id?>">
                                <h2><a href="/catalog/<?=strtolower($category->slug)?>"><?=$category->name?></a></h2>
                            </label>
                            <div class="wrapper-for-js">
                                <div class="content">
									<?php
									while ( $posts->have_posts() ) :
										$posts->the_post();
										if ( ! get_field( 'is_cat', $post->ID ) ) {
										?>
                                        <a class="content-item" href="<? the_permalink() ?>" title="<? the_title() ?>">
                                        <span class="img-wrapper">
                                            <?= the_post_thumbnail() ?>
                                        </span>
                                            <span class="link-title"><? the_title() ?></span>
                                        </a>
										<? } ?>
									<? endwhile; ?>
                                </div>
                            </div>
                        </div>

					<?endwhile;?>
					<? if($post_fields["commerce"]): ?>
                        <div class="car-catalog-item">
                            <input class="fake-input" type="checkbox" id="commerce">
                            <label class="content-header" for="commerce">
                                <h2>Коммерческий транспорт</h2>
                            </label>
                            <div class="wrapper-for-js">
								<?$coms=$post_fields['commerce'];
								foreach ($coms as $com):?>
                                    <h3><?=$com["com_header"] ?></h3>
                                    <div class="content">
										<?$links=$com['com_cars'];
										foreach ($links as $link):?>
                                            <a class="content-item" href="<?=$link["com_cars_file"] ?>" title="<?=$link["com_cars_file_title"] ?>" target="_blank">
                                    <span class="img-wrapper">
                                        <img src="<?=$link["com_cars_img"] ?>">
                                    </span>
                                                <span class="link-title"><?=$link["com_cars_header"] ?></span>
                                            </a>
										<? endforeach; ?>
                                    </div>
								<? endforeach; ?>
                            </div>
                        </div>
					<? endif; ?>
                </section>
            </div>
        </section>

        <section class="main-about cars-about">
            <section class="information-advantages inner">
                <svg class="cars-line-svg"
                     xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     width="1283px" height="1593px">
                    <path fill-rule="evenodd"  stroke="rgb(219, 228, 235)" stroke-width="2px" stroke-dasharray="8, 4" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                          d="M795.000,1591.000 C795.000,1515.000 828.930,1519.483 868.000,1458.000 C905.023,1399.738 979.000,1401.000 1110.000,1375.000 C1241.000,1349.000 1329.132,1173.697 1246.000,1052.000 C1155.747,919.879 1019.101,1012.874 936.000,1006.000 C861.019,999.797 857.000,950.000 776.000,936.000 C692.800,921.619 609.000,974.000 389.000,1010.000 C162.670,1047.035 166.000,793.000 166.000,793.000 C166.000,793.000 176.000,751.000 131.000,660.000 C86.000,569.000 -75.000,373.000 45.000,275.000 C165.000,177.000 323.000,275.000 341.000,276.000 C359.000,277.000 389.000,272.500 403.000,250.000 C417.000,227.500 424.500,191.500 405.000,144.000 C385.500,96.500 399.500,56.500 463.000,55.000 C526.500,53.500 535.500,21.000 536.000,1.000 "/>
                </svg>
                <figure class="about-image" style="background-image: url('<?=get_stylesheet_directory_uri()?>/assets/img/main-about-image.jpg'); background-size: cover;">
                </figure>
                <div class="information-block">
                    <h2 class="dot-title"><?=get_field('about_salon_title','options')?></h2>
                    <p><?=get_field('about_salon_text','options')?></p>
                    <div class="advantages-block">
						<? $elements = get_field('about_salon_elements','options');
						foreach ($elements as $element){?>
                            <div class="advantage">
                                <img src="<?=$element['pic']?>" width="<?=$element['width']?>" height="<?=$element['height']?>" alt="">
                                <span><?=$element['text']?></span>
                            </div>
						<? } ?>
                    </div>
                </div>
            </section>
        </section>
        <section class="car-types-block car-types-margin no-border">
			<?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>