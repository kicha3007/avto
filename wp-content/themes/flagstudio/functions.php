<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 * @subpackage your-clean-template
 */

/**
 * Подключение скриптов и стилей
 */

function my_theme_enqueue_scripts_and_styles() {
   $files_css = array(
      'jquery-fancybox'   => '/assets/css/vendor/jquery.fancybox.min.css',
      'justifiedGallery'  => '/assets/css/vendor/justifiedGallery.min.css',
      'owl-carousel'      => '/assets/css/vendor/owl.carousel.min.css',
      'owl-theme-default' => '/assets/css/vendor/owl.theme.default.min.css',
      'nice-select' => '/assets/css/vendor/nice-select.css',
      'jquery-ui' => '/assets/css/vendor/jquery-ui.min.css',
       'swiper'   => '/assets/css/vendor/swiper.min.css',
      'main-style'        => '/assets/css/style.min.css'
   );

   $files_js = array(
      'jquery-fancybox'   => '/assets/js/vendor/jquery.fancybox.min.js',
      'justifiedGallery'  => '/assets/js/vendor/jquery.justifiedGallery.min.js',
      'owl-carousel'      => '/assets/js/vendor/owl.carousel.min.js',
      'modernizr'         => '/assets/js/vendor/modernizr-custom.js',
      'nice-select'         => '/assets/js/vendor/jquery.nice-select.min.js',
      'jquery-ui'         => '/assets/js/vendor/jquery-ui.min.js',
      'number-polyfill'         => '/assets/js/vendor/number-polyfill.min.js',
       'swiper'   => '/assets/js/vendor/swiper.min.js',
       'touch-punch' => '/assets/js/vendor/jquery.ui.touch-punch.min.js',
       'main'              => '/assets/js/main.js',
   );

   foreach ( $files_css as $key => $value ) {
      $version = filemtime( get_stylesheet_directory() . $value );

      if ( $version !== false ) {
         wp_enqueue_style( $key, get_stylesheet_directory_uri() . $value, array(), $version, 'all' );
      }
   }

   foreach ( $files_js as $key => $value ) {
      $version = filemtime( get_stylesheet_directory() . $value );

      if ( $version !== false ) {
         wp_enqueue_script( $key, get_template_directory_uri() . $value, array('jquery'), $version, true );
      }
   }
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts_and_styles' );



function typical_title() { // функция вывода тайтла
	global $page, $paged; // переменные пагинации должны быть глобыльными
	wp_title('|', true, 'right'); // вывод стандартного заголовка с разделителем "|"
	bloginfo('name'); // вывод названия сайта
	$site_description = get_bloginfo('description', 'display'); // получаем описание сайта
	if ($site_description && (is_home() || is_front_page())) //если описание сайта есть и мы на главной
		echo " | $site_description"; // выводим описание сайта с "|" разделителем
	if ($paged >= 2 || $page >= 2) // если пагинация была использована
		echo ' | '.sprintf(__( 'Страница %s'), max($paged, $page)); // покажем номер страницы с "|" разделителем
}
// Включаем поддержку меню
add_theme_support('menus');

register_nav_menus(array( // Регистрируем 2 меню
	'main-header-menu' => 'Главное меню в шапке',
	'footer-menu' => 'Меню в подвале',
   'slider-menu' => 'Меню на главной под слайдером'
));

add_theme_support('post-thumbnails'); // включаем поддержку миниатюр
set_post_thumbnail_size(250, 150); // задаем размер миниатюрам 250x150
add_image_size('big-thumb', 400, 400, true); // добавляем еще один размер картинкам 400x400 с обрезкой

register_sidebar(array( // регистрируем левую колонку, этот кусок можно повторять для добавления новых областей для виджитов
	'name' => 'Колонка слева', // Название в админке
	'id' => "left-sidebar", // идентификатор для вызова в шаблонах
	'description' => 'Обычная колонка в сайдбаре', // Описалово в админке
	'before_widget' => '<div id="%1$s" class="widget %2$s">', // разметка до вывода каждого виджета
	'after_widget' => "</div>\n", // разметка после вывода каждого виджета
	'before_title' => '<span class="widgettitle">', //  разметка до вывода заголовка виджета
	'after_title' => "</span>\n", //  разметка после вывода заголовка виджета
));

class clean_comments_constructor extends Walker_Comment { // класс, который собирает всю структуру комментов
	public function start_lvl( &$output, $depth = 0, $args = array()) { // что выводим перед дочерними комментариями
		$output .= '<ul class="children">' . "\n";
	}
	public function end_lvl( &$output, $depth = 0, $args = array()) { // что выводим после дочерних комментариев
		$output .= "</ul><!-- .children -->\n";
	}
    protected function comment( $comment, $depth, $args ) { // разметка каждого комментария, без закрывающего </li>!
    	$classes = implode(' ', get_comment_class()).($comment->comment_author_email == get_the_author_meta('email') ? ' author-comment' : ''); // берем стандартные классы комментария и если коммент пренадлежит автору поста добавляем класс author-comment
        echo '<li id="li-comment-'.get_comment_ID().'" class="'.$classes.'">'."\n"; // родительский тэг комментария с классами выше и уникальным id
    	echo '<div id="comment-'.get_comment_ID().'">'."\n"; // элемент с таким id нужен для якорных ссылок на коммент
    	echo get_avatar($comment, 64)."\n"; // покажем аватар с размером 64х64
    	echo '<p class="meta">Автор: '.get_comment_author()."\n"; // имя автора коммента
    	echo ' '.get_comment_author_email(); // email автора коммента
    	echo ' '.get_comment_author_url(); // url автора коммента
    	echo ' Добавлено '.get_the_time('l, F jS, Y').' в '.get_the_time().'</p>'."\n"; // дата и время комментирования
    	if ( '0' == $comment->comment_approved ) echo '<em class="comment-awaiting-moderation">Ваш комментарий будет опубликован после проверки модератором.</em>'."\n"; // если комментарий должен пройти проверку
        comment_text()."\n"; // текст коммента
        $reply_link_args = array( // опции ссылки "ответить"
        	'depth' => $depth, // текущая вложенность
        	'reply_text' => 'Ответить', // текст
			'login_text' => 'Вы должны быть залогинены' // текст если юзер должен залогинеться
        );
        echo get_comment_reply_link(array_merge($args, $reply_link_args)); // выводим ссылку ответить
        echo '</div>'."\n"; // закрываем див
    }
    public function end_el( &$output, $comment, $depth = 0, $args = array() ) { // конец каждого коммента
		$output .= "</li><!-- #comment-## -->\n";
	}
}

// Рекурсивная функция вывода хлебных крошек
function get_breadcrumb($post_id = null){
  $breadcrumb = '';
    if (wp_get_post_parent_id($post_id)){ // Если есть parent_page, вызываем для нее эту же функцию
        $breadcrumb = get_breadcrumb(wp_get_post_parent_id($post_id));
    }
    return $breadcrumb . '<li><a href="'.get_permalink($post_id).'">' . get_the_title($post_id) . '</a></li>';
}

// Функция вывода хлебных крошек с html
function the_breadcrumb(){
    if (!is_home()){
        echo '<ul>
                <li><a href="'.get_home_url().'">'.get_the_title( get_option('page_on_front', true) ).'</a></li>
                ' . get_breadcrumb() . '
                </ul>';
    }
}


function pagination() { // функция вывода пагинации
	global $wp_query; // текущая выборка должна быть глобальной
	$big = 999999999; // число для замены
	echo paginate_links(array( // вывод пагинации с опциями ниже
		'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))), // что заменяем в формате ниже
		'format' => '?paged=%#%', // формат, %#% будет заменено
		'current' => max(1, get_query_var('paged')), // текущая страница, 1, если $_GET['page'] не определено
		'type' => 'list', // ссылки в ul
		'prev_text'    => 'Назад', // текст назад
    	'next_text'    => 'Вперед', // текст вперед
		'total' => $wp_query->max_num_pages, // общие кол-во страниц в пагинации
		'show_all'     => false, // не показывать ссылки на все страницы, иначе end_size и mid_size будут проигнорированны
		'end_size'     => 15, //  сколько страниц показать в начале и конце списка (12 ... 4 ... 89)
		'mid_size'     => 15, // сколько страниц показать вокруг текущей страницы (... 123 5 678 ...).
		'add_args'     => false, // массив GET параметров для добавления в ссылку страницы
		'add_fragment' => '',	// строка для добавления в конец ссылки на страницу
		'before_page_number' => '', // строка перед цифрой
		'after_page_number' => '' // строка после цифры
	));
}
remove_action('wp_head', 'wp_generator');
function selectel_remove_version() {
return '';
}
add_filter('the_generator', 'selectel_remove_version');


function maxsite_the_russian_time($tdate = '') {
	if ( substr_count($tdate , '---') > 0 ) return str_replace('---', '', $tdate);

	$treplace = array (
	"Январь" => "января",
	"Февраль" => "февраля",
	"Март" => "марта",
	"Апрель" => "апреля",
	"Май" => "мая",
	"Июнь" => "июня",
	"Июль" => "июля",
	"Август" => "августа",
	"Сентябрь" => "сентября",
	"Октябрь" => "октября",
	"Ноябрь" => "ноября",
	"Декабрь" => "декабря",

	"January" => "января",
	"February" => "февраля",
	"March" => "марта",
	"April" => "апреля",
	"May" => "мая",
	"June" => "июня",
	"July" => "июля",
	"August" => "августа",
	"September" => "сентября",
	"October" => "октября",
	"November" => "ноября",
	"December" => "декабря",

	"Sunday" => "воскресенье",
	"Monday" => "понедельник",
	"Tuesday" => "вторник",
	"Wednesday" => "среда",
	"Thursday" => "четверг",
	"Friday" => "пятница",
	"Saturday" => "суббота",

	"Sun" => "воскресенье",
	"Mon" => "понедельник",
	"Tue" => "вторник",
	"Wed" => "среда",
	"Thu" => "четверг",
	"Fri" => "пятница",
	"Sat" => "суббота",

	"th" => "",
	"st" => "",
	"nd" => "",
	"rd" => ""

	);
   	return strtr($tdate, $treplace);
}

add_filter('the_time', 'maxsite_the_russian_time');
add_filter('get_the_time', 'maxsite_the_russian_time');
add_filter('the_date', 'maxsite_the_russian_time');
add_filter('get_the_date', 'maxsite_the_russian_time');
add_filter('the_modified_time', 'maxsite_the_russian_time');
add_filter('get_the_modified_date', 'maxsite_the_russian_time');
add_filter('get_post_time', 'maxsite_the_russian_time');
add_filter('get_comment_date', 'maxsite_the_russian_time');


add_action( 'admin_bar_menu', 'wp_admin_bar_my_custom_account_menu', 11 );
function wp_admin_bar_my_custom_account_menu( $wp_admin_bar ) {
    $user_id = get_current_user_id();
    $current_user = wp_get_current_user();
    $profile_url = get_edit_profile_url( $user_id );
    exec('cd ' . ABSPATH . ' && git status 2>&1', $strings);
    $text = str_replace('# On branch ', '', $strings[0]);

    if ( 0 != $user_id ) {
        /* Add the "My Account" menu */
        $avatar = get_avatar( $user_id, 28 );
        $howdy = sprintf( __('Привет, %1$s'), $current_user->display_name );
        $class = empty( $avatar ) ? '' : 'with-avatar';

        $wp_admin_bar->add_menu( array(
            'id' => 'my-account',
            'parent' => 'top-secondary',
            'title' => $howdy.', ты на ветке: '. $text . $avatar,
            'href' => $profile_url,
            'meta' => array(
                'class' => $class,
            ),
        ) );

    }
}

function getPostViews($postID){
   $count_key = 'post_views_count';
   $count = get_post_meta($postID, $count_key, true);
   if($count==''){
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
      return "0";
   }
   return $count;
}

function setPostViews($postID) {
   $count_key = 'post_views_count';
   $count = get_post_meta($postID, $count_key, true);
   if($count==''){
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
   }else{
      $count++;
      update_post_meta($postID, $count_key, $count);
   }
}


// add_filter('pre_site_transient_update_core',create_function('$a', "return null;"));
// wp_clear_scheduled_hook('wp_version_check');

add_action('init', 'register_post_types');
function register_post_types()
{
   register_post_type('catalog', array(
      'label'  => null,
      'labels' => array(
         'name'               => 'Автомобили', // основное название для типа записи
         'singular_name'      => 'Автомобиль', // название для одной записи этого типа
         'add_new'            => 'Добавить автомобиль', // для добавления новой записи
         'add_new_item'       => 'Добавление автомобиля', // заголовка у вновь создаваемой записи в админ-панели.
         'edit_item'          => 'Редактирование автомобиля', // для редактирования типа записи
         'new_item'           => 'Новый автомобиль', // текст новой записи
         'view_item'          => 'Смотреть автомобиль', // для просмотра записи этого типа.
         'search_items'       => 'Искать автомобиль', // для поиска по этим типам записи
         'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
         'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
         'parent_item_colon'  => '', // для родителей (у древовидных типов)
         'menu_name'          => 'Автомобили', // название меню
      ),
      'description'         => '',
      'public'              => true,
      'publicly_queryable'  => null,
      'exclude_from_search' => null,
      'show_ui'             => null,
      'show_in_menu'        => true, // показывать ли в меню адмнки
      'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
      'show_in_nav_menus'   => true,
      'show_in_rest'        => null, // добавить в REST API. C WP 4.7
      'rest_base'           => null, // $post_type. C WP 4.7
      'menu_position'       => null,
      'menu_icon'           => null,
      //'capability_type'   => 'post',
      //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
      //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
      'hierarchical'        => true,
      'supports'            => array('title', 'thumbnail', 'page-attributes'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
      'taxonomies'          => array(),
      'has_archive'         => true,
      'rewrite'             => true,
      'query_var'           => true,
   ));

   register_post_type('sales', array(
      'label'  => null,
      'labels' => array(
         'name'               => 'Акции', // основное название для типа записи
         'singular_name'      => 'Акции', // название для одной записи этого типа
         'add_new'            => 'Добавить акцию', // для добавления новой записи
         'add_new_item'       => 'Добавление акции', // заголовка у вновь создаваемой записи в админ-панели.
         'edit_item'          => 'Редактирование акции', // для редактирования типа записи
         'new_item'           => 'Новая акция', // текст новой записи
         'view_item'          => 'Смотреть акцию', // для просмотра записи этого типа.
         'search_items'       => 'Искать акцию', // для поиска по этим типам записи
         'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
         'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
         'parent_item_colon'  => '', // для родителей (у древовидных типов)
         'menu_name'          => 'Акции', // название меню
      ),
      'description'         => '',
      'public'              => true,
      'publicly_queryable'  => null,
      'exclude_from_search' => null,
      'show_ui'             => null,
      'show_in_menu'        => true, // показывать ли в меню адмнки
      'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
      'show_in_nav_menus'   => true,
      'show_in_rest'        => null, // добавить в REST API. C WP 4.7
      'rest_base'           => null, // $post_type. C WP 4.7
      'menu_position'       => null,
      'menu_icon'           => null,
      //'capability_type'   => 'post',
      //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
      //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
      'hierarchical'        => false,
      'supports'            => array('title', 'thumbnail', 'editor'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
      'taxonomies'          => array(),
      'has_archive'         => false,
      'rewrite'             => true,
      'query_var'           => true,
   ));

    register_post_type('news', array(
        'label'  => null,
        'labels' => array(
            'name'               => 'Новости', // основное название для типа записи
            'singular_name'      => 'Новость', // название для одной записи этого типа
            'add_new'            => 'Добавить новость', // для добавления новой записи
            'add_new_item'       => 'Добавление новости', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование новости', // для редактирования типа записи
            'new_item'           => 'Новая новость', // текст новой записи
            'view_item'          => 'Смотреть новость', // для просмотра записи этого типа.
            'search_items'       => 'Искать новость', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Новости', // название меню
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => null,
        'exclude_from_search' => null,
        'show_ui'             => null,
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
        'show_in_nav_menus'   => true,
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'hierarchical'        => false,
        'supports'            => array('title', 'thumbnail', 'editor'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'taxonomies'          => array(),
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ));

    register_post_type('articles', array(
        'label'  => null,
        'labels' => array(
            'name'               => 'Статьи', // основное название для типа записи
            'singular_name'      => 'Статья', // название для одной записи этого типа
            'add_new'            => 'Добавить статью', // для добавления новой записи
            'add_new_item'       => 'Добавление статьи', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование статьи', // для редактирования типа записи
            'new_item'           => 'Новая статья', // текст новой записи
            'view_item'          => 'Смотреть статью', // для просмотра записи этого типа.
            'search_items'       => 'Искать статью', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Статьи', // название меню
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => null,
        'exclude_from_search' => null,
        'show_ui'             => null,
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
        'show_in_nav_menus'   => true,
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'hierarchical'        => false,
        'supports'            => array('title', 'thumbnail', 'editor'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'taxonomies'          => array(),
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ));
}


add_action('init', 'create_taxonomy');
function create_taxonomy(){
   // заголовки
   $labels = array(
      'name'              => 'Цвета автомобилей',
      'singular_name'     => 'Цвета автомобилей',
      'search_items'      => 'Искать цвет автомобиля',
      'all_items'         => 'Все типы цветов автомобилей',
      'parent_item'       => 'Тип цвета автомобиля',
      'parent_item_colon' => 'Тип цвета автомобиля',
      'edit_item'         => 'Изменить тип цвета автомобиля',
      'update_item'       => 'Изменить тип цвета автомобиля',
      'add_new_item'      => 'Добавить новый тип цвета автомобиля',
      'new_item_name'     => 'Новый тип цвета автомобиля',
      'menu_name'         => 'Цвета автомобилей',
   );
   // параметры
   $args = array(
      'label'                 => '', // определяется параметром $labels->name
      'labels'                => $labels,
      'description'           => '', // описание таксономии
      'public'                => true,
      'publicly_queryable'    => null, // равен аргументу public
      'show_in_nav_menus'     => false, // равен аргументу public
      'show_ui'               => true, // равен аргументу public
      'show_tagcloud'         => true, // равен аргументу show_ui
      'hierarchical'          => false,
      'update_count_callback' => '',
      'rewrite'               => array('slug' => 'colors'),
      'capabilities'          => array(),
      'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
      'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
      '_builtin'              => false,
      'show_in_quick_edit'    => null, // по умолчанию значение show_ui
   );
   register_taxonomy('colors', array('catalog'), $args );
}

add_action('init', 'create_taxonomy_category');
function create_taxonomy_category(){
   // заголовки
   $labels = array(
      'name'              => 'Категории',
      'singular_name'     => 'Категории',
      'search_items'      => 'Искать категорию',
      'all_items'         => 'Все типы категорий',
      'parent_item'       => 'Тип категории',
      'parent_item_colon' => 'Тип категории',
      'edit_item'         => 'Изменить тип категории',
      'update_item'       => 'Изменить тип категории',
      'add_new_item'      => 'Добавить новый тип категории',
      'new_item_name'     => 'Новый тип категорий',
      'menu_name'         => 'Категории',
   );
   // параметры
   $args = array(
      'label'                 => '', // определяется параметром $labels->name
      'labels'                => $labels,
      'description'           => '', // описание таксономии
      'public'                => true,
      'publicly_queryable'    => null, // равен аргументу public
      'show_in_nav_menus'     => true, // равен аргументу public
      'show_ui'               => true, // равен аргументу public
      'show_tagcloud'         => true, // равен аргументу show_ui
      'hierarchical'          => false,
      'update_count_callback' => '',
      'rewrite'               => array('slug' => 'category'),
      'capabilities'          => array(),
      'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
      'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
      '_builtin'              => false,
      'show_in_quick_edit'    => null, // по умолчанию значение show_ui
   );
   register_taxonomy('category', array('catalog'), $args );
}


if(function_exists('acf_add_options_page')) {
    $option_page = acf_add_options_page(array(
        'page_title' => 'Настройки Сайта',
        'menu_title' => 'Настройки Сайта',
        'menu_slug' => 'options',
        'capability' => 'manage_options',
        'redirect' => false
    ));
}
if(function_exists('acf_add_options_page')) {
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Коммерческий транспорт',
        'menu_title'    => 'Коммерческий транспорт',
        'menu_slug'     => 'commercial',
        'capability'    => 'manage_options',
        'redirect'  => false
    ));
}
if(function_exists('acf_add_options_page')) {
	$option_page = acf_add_options_page(array(
		'page_title'    => 'Авто с пробегом',
		'menu_title'    => 'Авто с пробегом',
		'menu_slug'     => 'used_cars',
		'capability'    => 'manage_options',
		'redirect'  => false
	));
}



/**
 * Ставим ссылку на себя в футере в админке
 */

function my_change_admin_footer () {
  $footer_text = array(
    'Спасибо вам за творчество с <a href="http://wordpress.org">WordPress</a>',
    'Разработка сайта <a href="https://flagstudio.ru" target="_blank" rel="noopener">«Студия Флаг»</a>'
  );
  return implode( ' &bull; ', $footer_text);
}

add_filter('admin_footer_text', 'my_change_admin_footer');



/**
 * Скрываем версию WordPress
 */

function my_remove_wp_version_wp_head_feed() {
  return '';
}

add_filter('the_generator', 'my_remove_wp_version_wp_head_feed');




/**
 * Убираем весь мусор из хэдера
 */

add_filter('xmlrpc_enabled', '__return_false');
remove_action('wp_head','feed_links_extra', 3); // убирает ссылки на RSS категорий
remove_action('wp_head','feed_links', 2); // минус ссылки на основной RSS и комментарии
remove_action('wp_head','rsd_link');  // удаляет RSD ссылку для удаленной публикации
remove_action('wp_head','wlwmanifest_link'); // удаляет ссылку Windows для Live Writer
remove_action('wp_head','wp_generator');  // удаляет версию WordPress
remove_action('wp_head','start_post_rel_link',10,0);
remove_action('wp_head','index_rel_link');
remove_action('wp_head','adjacent_posts_rel_link_wp_head', 10, 0 ); // удаляет ссылки на предыдущую и следующую статьи
remove_action('wp_head','wp_shortlink_wp_head', 10, 0 ); // удаляет короткую ссылку

// Отключаем type="application/json+oembed"
remove_action( 'wp_head', 'rest_output_link_wp_head');
remove_action( 'wp_head', 'wp_oembed_add_discovery_links');
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );


/**
 * Избавляемся от REST API
 */

// Отключаем сам REST API

 add_filter('rest_enabled', '__return_false');

// Отключаем события REST API

 remove_action( 'init', 'rest_api_init' );
 remove_action( 'rest_api_init', 'rest_api_default_filters', 10, 1 );
 remove_action( 'parse_request', 'rest_api_loaded' );

// Отключаем Embeds связанные с REST API

 remove_action( 'rest_api_init', 'wp_oembed_register_route' );
 remove_filter( 'rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4 );

// Отключаем фильтры REST API

 remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
 remove_action( 'wp_head', 'rest_output_link_wp_head', 10, 0 );
 remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
 remove_action( 'auth_cookie_malformed', 'rest_cookie_collect_status' );
 remove_action( 'auth_cookie_expired', 'rest_cookie_collect_status' );
 remove_action( 'auth_cookie_bad_username', 'rest_cookie_collect_status' );
 remove_action( 'auth_cookie_bad_hash', 'rest_cookie_collect_status' );
 remove_action( 'auth_cookie_valid', 'rest_cookie_collect_status' );
 remove_filter( 'rest_authentication_errors', 'rest_cookie_check_errors', 100 );


 /**
  * Защита от вредоносных URL-запросов
  */

 if (strpos($_SERVER['REQUEST_URI'], "eval(") || strpos($_SERVER['REQUEST_URI'], "CONCAT") || strpos($_SERVER['REQUEST_URI'], "UNION+SELECT") || strpos($_SERVER['REQUEST_URI'], "base64")) {
   @header("HTTP/1.1 400 Bad Request");
   @header("Status: 400 Bad Request");
   @header("Connection: Close");
   @exit;
 }

if(is_admin()) {
	add_filter('filesystem_method', create_function('$a', 'return "direct";' ));
	define( 'FS_CHMOD_DIR', 0751 );
}


//Сортировка
add_filter( 'woocommerce_get_catalog_ordering_args', 'custom_woocommerce_get_catalog_ordering_args' );
function custom_woocommerce_get_catalog_ordering_args( $args ) {
    if ($_SERVER['REQUEST_URI'] == '/pick-up/'){
        $args['orderby']  = 'meta_value_num';
        $args['order']    = 'asc';
        $args['meta_key'] = '_price';
        $_GET['sortByPrice'] = 'asc';
    }

	if ( isset( $_GET['sortByYear'] ) ) {
		switch ( $_GET['sortByYear'] ) :
			case 'asc' :
				$args['meta_key']  = 'pa_yaer';
				$args['order']    = 'ASC';
				$args['orderby'] = 'meta_value';
				break;
			case 'desc' :
				$args['meta_key']  = 'pa_yaer';
				$args['order']    = 'DESC';
				$args['orderby'] = 'meta_value';
				break;
		endswitch;
	}
	if ( isset( $_GET['sortByPrice'] ) ) {
		switch ( $_GET['sortByPrice'] ) :
			case 'asc' :
				$args['orderby']  = 'meta_value_num';
				$args['order']    = 'asc';
				$args['meta_key'] = '_price';
				break;
			case 'desc' :
				$args['orderby']  = 'meta_value_num';
				$args['order']    = 'desc';
				$args['meta_key'] = '_price';
				break;
		endswitch;
	}

	return $args;
}

/**
 * Save product attributes to post metadata when a product is saved.
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 *
 * Refrence: https://codex.wordpress.org/Plugin_API/Action_Reference/save_post
 */
function wh_save_product_custom_meta($post_id, $post, $update) {
	$post_type = get_post_type($post_id);
	// If this isn't a 'product' post, don't update it.
	if ($post_type != 'product')
		return;

	if (!empty($_POST['attribute_names']) && !empty($_POST['attribute_values'])) {
		$attribute_names = $_POST['attribute_names'];
		$attribute_values = $_POST['attribute_values'];
		foreach ($attribute_names as $key => $attribute_name) {
			switch ($attribute_name) {
				//for color (string)
				case 'pa_yaer':
					//it may have multiple color (eg. black, brown, maroon, white) but we'll take only the first color.
					if (!empty($attribute_values[$key])) {
						update_post_meta($post_id, 'pa_yaer', $attribute_values[$key]);
					}
					break;
				//for lenght (int)
				default:
					break;
			}
		}
	}
}

add_action( 'save_post', 'wh_save_product_custom_meta', 10, 3);