<?php
/**
 * Template Name: Шаблон страницы c произвольным контентом (page-content.php)
 */
get_header();
$post_fields = get_fields();
$options = get_fields(options);

?>
    <main>
        <section class="breadcrumbs">
            <div class="inner">
               <? the_breadcrumb(); ?>
            </div>
        </section>


        <section class="content-page">
                <div class="inner">

                <? if($post_fields["content_block"]) :?>
                   <? $contentRows=$post_fields['content_block'];
                   foreach ($contentRows as $row):?>
                        <div class="row">
                            <? if($row["content_select"] == 'text'): ?>
                            <div class="text_2 <?=$row["content_right"] ? 'block-reverse' : '' ?>">
                               <?=$row["content_text_2"] ?>
                            </div>
                            <? endif;?>

                           <? if($row["content_select"] == 'image'): ?>
                               <div class="content-image <?=$row["content_right"] ? 'block-reverse' : '' ?>" style="background: url('<?=$row["content_image"] ?>') no-repeat center; background-size: cover;">
                               </div>
                           <? endif;?>

                           <? if($row["content_select"] == 'minigallery'): ?>
                               <div class="mini-gallery <?=$row["content_right"] ? 'block-reverse' : '' ?>">
                                   <? $images=$row['content_minigallery']; ?>
                                   <div class="main-image" style="background: url('<?=$images[0]["url"]?>') no-repeat center; background-size: cover;">
                                   </div>
                                   <div class="carousel owl-carousel">
                                   <? foreach ($images as $image):?>
                                       <div class="thumbnail" style="background: url('<?=$image["sizes"]["thumbnail"]?>') no-repeat center; background-size: cover;" alt="" data-src="<?=$image["url"]?>"></div>
                                   <?endforeach; ?>
                                   </div>
                               </div>
                           <? endif;?>


                            <div class="text">
                               <?=$row["content_text_1"] ?>
                            </div>
                        </div>
                      <?endforeach; ?>

                <? endif;?>
                   <? if($post_fields["check_repair"]) :?>
                       <section class="buttons-block">
                           <button type="button" class="show-repair-popup" title="Записаться на ремонт/ТО">Записаться на ремонт/ТО</button>
                       </section>
                   <? endif;?>
                </div>
            </section>


        <section class="car-types-block no-border">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>