<?php
/**
 * Шаблон страницы автомобиля (single-cars.php)
 */
get_header();
$post_fields = get_fields();
$options     = get_fields( options );
$cat         = wp_get_post_categories( $post->ID );
$cat         = get_the_category_by_ID( $cat[0] );
?>
    <main>
        <section class="breadcrumbs">
            <div class="inner">
                <ul>
                    <li><a href="/">Главная страница</a></li>
                    <li><a href="/catalog">Каталог автомобилей</a></li>
                    <li><a href="/catalog/<?= strtolower( $cat ) ?>"><?= $cat ?></a></li>
                    <li><a href="<?= get_permalink( $post->ID ) ?>"><?= $post->post_title ?></a></li>
                </ul>
            </div>
        </section>
		<? if ( $post_fields["car_colors"] ): ?>
            <section class="color-tabs-block">
                <div class="inner">
                    <div class="color-tabs-wrapper">
						<?
						$colors = $post_fields['car_colors'];
						$i      = 1;
						foreach ( $colors as $color ):?>
                            <input type="radio" name="color-tab-fake-input"<?= ( $i == 1 ) ? 'checked="checked"' : '' ?>
                                   id="color-<?= $i ?>"/>
                            <div class="car-image"
                                 style="background: url('<?= $color["cars_color_image"] ?>') no-repeat center; background-size: contain;"></div>
							<? $i ++;
						endforeach; ?>

                        <h1><? the_title() ?></h1>
                        <b class="price">от <span><?= $post_fields["car_price"] ?></span> руб</b>
                        <ul class="right-buttons">
                            <li class="wheel">
                                <button class="color-tabs-button show-test-drive-popup" type="button">пройти
                                    тест-драйв
                                </button>
                            </li>
                            <li class="credit">
                                <a class="color-tabs-button" href="/avtokredit" title="Заявка на кредит">Заявка на
                                    кредит</a>
                            </li>
                            <li class="download">
                                <a class="color-tabs-button" href="<?= $post_fields["pricelist_file"] ?>"
                                   title="Скачать прайслист" target="_blank">скачать прайслист</a>
                            </li>
                        </ul>
                    </div>
                    <div class="color-pagination">
						<? $colors = $post_fields['car_colors'];
						$i         = 1;
						foreach ( $colors as $color ):
							$colorList = get_term( $color['color-tax'], 'colors' ); ?>
                            <label title='<?= $colorList->name ?>' for="color-<?= $i ?>"><span
                                        style="background:<?= get_field( 'color_code', 'colors_' . $color['color-tax'] ) ?>"><?= $colorList->name ?></span></label>
							<? $i ++;
						endforeach; ?>

                    </div>
                </div>
            </section>
		<? endif; ?>


        <section id="tabs-start" class="catalog-item-tabs">
			<?
			$checked = false;
			if ( $post_fields["content_block"] || $post_fields["video_link"] ) :?>
                <input type="radio" name="catalog-item-tab-fake-input"
                       id="description" <?= ! $checked ? 'checked' : '' ?> />
				<? $checked = true;
			endif; ?>
			<? if ( $post_fields["characteristics"] ) : ?>
                <input type="radio" name="catalog-item-tab-fake-input"
                       id="characteristics" <?= ! $checked ? 'checked' : '' ?> />
				<? $checked = true;
			endif; ?>
			<? if ( $post_fields["gallery_exterior"] || $post_fields["gallery_interior"] ) : ?>
                <input type="radio" name="catalog-item-tab-fake-input"
                       id="gallery" <?= ! $checked ? 'checked' : '' ?> />
				<? $checked = true;
			endif; ?>
			<? if ( $post_fields["equipment_block"] ) : ?>
                <input type="radio" name="catalog-item-tab-fake-input"
                       id="equipment" <?= ! $checked ? 'checked' : '' ?> />
				<? $checked = true;
			endif; ?>
            <div class="tabs-menu-wrapper">
                <div id="tabs-menu" class="tab-controls">
                    <div class="inner">
						<?
						if ( $post_fields["content_block"] || $post_fields["video_link"] ) :?>
                            <label for="description" tabindex="0" class="active">Описание</label>
						<? endif; ?>
						<? if ( $post_fields["characteristics"] ) : ?>
                            <label for="characteristics" tabindex="0">Технические характеристики</label>
						<? endif; ?>
						<? if ( $post_fields["gallery_exterior"] || $post_fields["gallery_interior"] ) : ?>
                            <label for="gallery" tabindex="0">Галерея</label>
						<? endif; ?>
						<? if ( $post_fields["equipment_block"] ) : ?>
                            <label for="equipment" tabindex="0">Комплектации и цены</label>
						<? endif; ?>
						<? if ( $post_fields["booklet_file"] ) : ?>
                            <a href="<?= $post_fields["booklet_file"] ?>" title="Скачать брошюру" target="_blank">Скачать
                                брошюру</a>
						<? endif; ?>
                    </div>
                </div>
            </div>

			<? if ( $post_fields["content_block"] || $post_fields["video_link"] ) : ?>
                <section id="description-tab" class="tab">
                    <div class="inner">
						<? if ( $post_fields["video_link"] ) : ?>
                            <div class="video-preview"
                                 style="background: url('<?= $post_fields["video_preview"] ?>') no-repeat center; background-size: cover;">
                                <a data-fancybox href="<?= $post_fields["video_link"] ?>" class="play-video">Смотреть
                                    видео</a>
                            </div>
						<? endif; ?>

						<? if ( $post_fields["content_block"] ) : ?>
                            <div class="description-content">
								<? $contentRows = $post_fields['content_block'];
								foreach ( $contentRows as $row ):?>
                                    <div class="row">
										<? if ( $row["content_select"] == 'image' ): ?>
                                            <div class="content-image <?= $row["content_right"] ? 'block-reverse' : '' ?>"
                                                 style="background: url('<?= $row["content_image"] ?>') no-repeat center; background-size: cover;">
                                            </div>
										<? endif; ?>

										<? if ( $row["content_select"] == 'minigallery' ): ?>
                                            <div class="mini-gallery <?= $row["content_right"] ? 'block-reverse' : '' ?>">
												<? $images = $row['content_minigallery']; ?>
                                                <div class="main-image"
                                                     style="background: url('<?= $images[0]["url"] ?>') no-repeat center; background-size: cover;">
                                                </div>
                                                <div class="carousel owl-carousel">
													<? foreach ( $images as $image ): ?>
                                                        <div class="thumbnail"
                                                             style="background: url('<?= $image["sizes"]["thumbnail"] ?>') no-repeat center; background-size: cover;"
                                                             alt="" data-src="<?= $image["url"] ?>"></div>
													<? endforeach; ?>
                                                </div>
                                            </div>
										<? endif; ?>


                                        <div class="text">
                                            <h2><?= $row["content_header"] ?></h2>
											<?= $row["content_text"] ?>
                                        </div>
                                    </div>
								<? endforeach; ?>

                            </div>
						<? endif; ?>
                    </div>
                </section>
			<? endif; ?>
			<? if ( $post_fields["characteristics"] ) : ?>
                <section id="characteristics-tab" class="tab">
                    <div class="inner">
                        <h2>Технические характеристики</h2>
						<?= $post_fields["characteristics"] ?>
                    </div>
                </section>
			<? endif; ?>

			<? if ( $post_fields["gallery_exterior"] || $post_fields["gallery_interior"] ) : ?>
                <section id="gallery-tab" class="tab gallery-tab">
                    <div class="inner">
                        <h2>Галерея</h2>
						<? $checked = false;
						if ( $post_fields["gallery_exterior"] ) :?>
                            <input type="radio" name="gallery-tab-fake-input"
                                   id="exterior" <?= ! $checked ? 'checked' : '' ?> />

							<? $checked = true;
						endif; ?>

						<? if ( $post_fields["gallery_interior"] ) : ?>
                            <input type="radio" name="gallery-tab-fake-input"
                                   id="interior" <?= ! $checked ? 'checked' : '' ?> />

							<? $checked = true;
						endif; ?>
                        <div class="tab-controls">
							<? if ( $post_fields["gallery_exterior"] ) : ?>
                                <label for="exterior" tabindex="0">Эстерьер</label>
							<? endif; ?>
							<? if ( $post_fields["gallery_interior"] ) : ?>
                                <label for="interior" tabindex="0">Интерьер</label>
							<? endif; ?>
                        </div>
						<? if ( $post_fields["gallery_exterior"] ) : ?>
                            <div id="exterior-tab" class="tab-inside-gallery">
                                <div class="grid">

									<? $images = $post_fields['gallery_exterior']; ?>
									<? foreach ( $images as $image ): ?>
                                        <a class="grid-item" data-fancybox="gallery1" href="<?= $image["url"] ?>"><img
                                                    src="<?= $image["url"] ?>"></a>
									<? endforeach; ?>

                                    <div class="preloader">
                                        <div class="spinner">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--                        <button type="button" class="show-more">Смотреть еще</button>-->
                            </div>
						<? endif; ?>
						<? if ( $post_fields["gallery_interior"] ) : ?>
                            <div id="interior-tab" class="tab-inside-gallery">
                                <div class="grid">
									<? $images = $post_fields['gallery_interior']; ?>
									<? foreach ( $images as $image ): ?>
                                        <a class="grid-item" data-fancybox="gallery2" href="<?= $image["url"] ?>"><img
                                                    src="<?= $image["url"] ?>"></a>
									<? endforeach; ?>
                                    <div class="preloader">
                                        <div class="spinner">
                                            <div class="bounce1"></div>
                                            <div class="bounce2"></div>
                                            <div class="bounce3"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--                        <button type="button" class="show-more">Смотреть еще</button>-->
                            </div>
						<? endif; ?>
                    </div>
                </section>
			<? endif; ?>
			<? if ( $post_fields["equipment_block"] ) : ?>
                <section id="equipment-tab" class="tab equipment-tab">
                    <div class="inner">

						<? $equipments = $post_fields['equipment_block'];
						foreach ( $equipments as $equipment ):?>
                            <div class="equipment-block">
                                <h3><?= $equipment["equipment-header"] ?></h3>
								<? if ( $equipment["equipment_item"] ) : ?>
									<? $items = $equipment["equipment_item"];
									foreach ( $items as $item ):?>
                                        <div class="equipment-item">
                                            <b class="name"><?= $item["equipment-title"] ?></b>
                                            <span class="feature"><?= $item["equipment-feature"] ? $item["equipment-feature"] : '' ?></span>
                                            <div class="price"><span><?= $item["equipment-price"] ?></span>&nbsp; руб.
                                            </div>
                                            <button class="request-btn" type="button"
                                                    data-equipment="<? the_title() ?> <?= $equipment["equipment-header"] ?> <?= $item["equipment-title"] ?> <?= $item["equipment-feature"] ?>">
                                                Оставить заявку
                                            </button>
                                        </div>
									<? endforeach; ?>
								<? endif; ?>
                            </div>
						<? endforeach; ?>

                    </div>
                </section>
			<? endif; ?>
        </section>


        <section class="steps-block">
            <div class="bg1"></div>
            <div class="bg2"></div>
            <div class="inner">
                <div class="first-step">
                    <h2><?= $options['first_buttons_header']; ?></h2>
                    <ul class="buttons-row">
                        <li class="wheel">
                            <button class="step-button show-test-drive-popup" type="button">пройти тест-драйв</button>
                        </li>
                        <li class="credit">
                            <a class="step-button" href="/avtokredit" class="step-button" type="button">Заявка на
                                кредит</a>
                        </li>
                    </ul>
                </div>

                <div class="second-step">
                    <h2><?= $options['second_buttons_header']; ?></h2>
                    <ul class="buttons-row">
                        <li class="download">
                            <a class="step-button" href="<?= $post_fields["pricelist_file"] ?>"
                               title="Скачать прайслист" target="_blank">скачать прайслист</a>
                        </li>
                        <li class="download">
                            <a class="step-button" href="<?= $post_fields["booklet_file"] ?>" title="Скачать брошюру"
                               target="_blank">скачать брошюру</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="car-types-block no-border">
			<? get_template_part( 'inc/car-types', 'index' ); ?>
        </section>
    </main>
<?php get_footer(); ?>