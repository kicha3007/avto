<?php
/**
 * Страница 404 ошибки (404.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); ?>

<section class="not-found">
	<div class="inner">
        <div class="not-found__logo-wrapper">
            <a class="not-found__logo" href="/" title="На главную"><img src="<?=get_stylesheet_directory_uri()?>/assets/img/logo-lada-404.png" width="157" height="31" alt="logo lada"></a>
            <a class="not-found__logo" href="/" title="На главную"><img src="<?=get_stylesheet_directory_uri()?>/assets/img/logo-avtovek-404.png" width="144" height="39" alt="logo avtovek"></a>
        </div>
        <div class="not-found__phone">
            8 (343) 253-00-533
        </div>
        <div class="not-found__number">404</div>
        <h1 class="not-found__title">Вы ввели несуществующий адрес, проверьте адрес<br>
            или перейдите на </h1>
        <a class="not-found__home-btn" href="/" title="Перейти на главную страницу">Главную страницу</a>
    </div>
</section>

<?php get_footer(); ?>
