<div class="inner">
    <div class="car-types-items">
    <?php
    $categoryList = get_categories('taxonomy=category&orderby=id');
    while ($categoryList) : $category=array_shift($categoryList);

        if($category->name === 'Коммерческие и специальные автомобили'){
            continue;
        }
        $args = array(
            'post_type' => 'catalog',
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'ASC',
            'posts_per_page' => '-1',
            'category' => $category->name
        );
        $posts = new WP_Query( $args );

        ?>
        <div class="car-types-item">
            <div class="car-types-img" style="background: url('<?=get_stylesheet_directory_uri() . '/assets/img/'.strtolower($category->name).'.png'?>') no-repeat left bottom;"></div>
            <h3><a class="car-types-link" href="/catalog/<?=strtolower($category->name)?>"><?=$category->name?></a></h3>
            <ul>
                <?php
                while ( $posts->have_posts() ) : $posts->the_post();
                    if ( ! get_field( 'is_cat', $post->ID ) ) { ?>
                        <li><a href="<? the_permalink() ?>" title="<? the_title() ?>"><? the_title() ?></a></li>
                <? } endwhile; ?>
            </ul>
        </div>

    <?endwhile;?>

      </div>
   </div>