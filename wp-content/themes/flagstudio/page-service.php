<?php
/**
 * Template Name: Шаблон страницы техническое обслуживание (page-service.php)
 */
if ( $_REQUEST['subFastService'] ) {
	include( 'lada-mailer.php' );
//Отправка письма пользователю
	$message        = '
                Поступила новая заявка на техническое обслуживание со страницы ' . get_site_url() . $_SERVER['REQUEST_URI'] . '.<br>
                Содержание заявки:<br>
                Модель автомобиля: ' . $_REQUEST['model'] . '<br>
                ФИО: ' . $_REQUEST['service-username'] . '<br>
                E-mail: ' . $_REQUEST['service-useremail'] . '<br>
                Телефон: ' . $_REQUEST['service-userphone'] . '<br>';
	$email          = ladaMail();
	$email->Subject = 'Заявка на техническое обслуживание';
	$email->MsgHTML( $message );
	$mails = get_field( 'fos_services', 'options' );
	foreach ( $mails as $mail ) {
		$email->AddAddress( $mail['mail'] );
	}
	$send = $email->Send();
	header( 'Location: /thank-you' );
}
get_header();
?>
    <main>
        <section class="breadcrumbs">
            <div class="inner">
                <? the_breadcrumb(); ?>
            </div>
        </section>

        <section class="service">
            <div class="inner">

                <h1 class="service__header">
                    Техническое обслуживание
                </h1>
                <button class="service__button js--service-fast">Оставить быструю заявку на то</button>
                <div class="service-calculator">
                    <form method="post" enctype="multipart/form-data" class="js--nice-select">
                        <div class="service-calculator__select calculator-select js--data-select">

                            <div class="service-calculator__select-item">
                                <h2 class="service-calculator__select-title">Выберите марку</h2>
                                <select name="brand">
	                                <?
	                                $categoryList = get_categories( 'taxonomy=category&orderby=id' );
	                                foreach ( $categoryList as $list ) {
		                                $args  = array(
			                                'post_type'      => 'catalog',
			                                'post_status'    => 'publish',
			                                'orderby'        => 'date',
			                                'order'          => 'ASC',
			                                'posts_per_page' => '-1',
			                                'category'       => $list->name
		                                );
		                                $posts = new WP_Query( $args );
		                                foreach ( $posts->posts as $post ) {
			                                if (  get_field( 'is_cat', $post->ID ) ) {
				                                ?>
                                                <option value="<?= $post->ID ?>"><?= $post->post_title ?></option>
			                                <? }
		                                }
	                                } ?>
                                </select>
                            </div>
                            <div class="service-calculator__select-item">
                                <h2 class="service-calculator__select-title">Выберите кузов</h2>
                                <select name="carcase">
                                    <option value="LADA">LADA Vesta Седан</option>
                                    <option value="BMW">BMW</option>
                                    <option value="AUDI">AUDI</option>
                                </select>
                            </div>
                            <div class="service-calculator__select-item">
                                <h2 class="service-calculator__select-title">Комплектация</h2>
                                <select name="equipment">
                                    <option value="LADA">Vesta - 1.8 л (122 л.с.)</option>
                                    <option value="BMW">BMW</option>
                                    <option value="AUDI">AUDI</option>
                                </select>
                            </div>

                            <div class="service-calculator__image">
                                <img src="<?= get_template_directory_uri(); ?>/assets/img/service-auto.png" alt="auto">
                            </div>
                        </div>
                        <div class="service-calculator__panel">
                            <h2 class="service-calculator__panel-title">
                                Калькулятор ТО
                            </h2>
                            <p class="service-calculator__panel-desc">
                                Настоящий раздел содержит информационные сведения,
                                позволяющие ознакомиться с ориентировочной стоимостью
                                технического обслуживания Вашего автомобиля.
                            </p>
                            <section class="service-mileage">
                                <div class="service-mileage__inner">

                                    <h2>Пробег (км.)</h2>
                                    <input type="number" value="14830" class="js--input-service-mileage">
                                    <input class="js--data-service-mileage" type="text" style="display: none;" data-from-mileage="0" data-to-mileage="90000" data-step-mileage="1" data-set-default-mileage="17430">

                                </div>
                                <div class="range js--service-mileage"></div>
                                <div class="service-mileage__numbers js--generate-mileage"></div>
                                <div class="line-of-range js--line-of-range"></div>
                            </section>
                            <section class="service-calculator__panel-show">
                                <button class="service-calculator__button">Посчитать</button>
                                <div class="service-calculator__data">
                                    <h3>ТО - 1</h3>
                                    <p>14500-15500 км. или через 1 год эксплуатации
                                        (за 1мес. до окончания года и не позднее
                                        1 мес. с окончания года эксплуатации).</p>
                                    <div class="service-calculator__data-cost">
                                        <h2>Стоимость работы</h2>
                                        <span class="cost">7500</span>
                                        <span class="type">ТО - 1</span>
                                    </div>
                                    <button class="service-calculator__button service-calculator__button--white js--service-request">Записаться на то</button>
                                </div>
                            </section>
                            <a data-material-id="5" class="service-calculator__panel-materials js--open-materials" href="#">Перечень материалов, подлежащих замене</a>
                        </div>
                    </form>
                </div>

                <div class="service-notation">
                    <p>
                       cтоимость технического обслуживания указана с учетом применения моторного масла «Роснефть». Для получения подробной информации об обязательных к исполнению операций по техническому обслуживанию, стоимости запасных частей и материалов Вашего автомобиля обращайтесь к официальным дилерам ПАО «АВТОВАЗ».
                    </p>
                    <p>
                        итоговая стоимость технического обслуживания (ТО), указанная в настоящем калькуляторе, рекомендуется ПАО «АВТОВАЗ» как максимальная розничная цена и может отличаться от действительных цен официальных дилеров LADA. Дилеры LADA могут устанавливать стоимость предложения меньше, чем рекомендованная максимальная розничная цена. Обозначенные цены не охватывают случаи использования на ТО материалов, отличных от указанных ПАО «АВТОВАЗ», а равно выполнения работ дополнительно к регламентным.
                    </p>
                    <p>
                        информация о максимальных рекомендованных розничных ценах ТО в настоящем разделе носит справочный характер и ни при каких обстоятельствах не является публичной офертой, определяемой положениями статьи 437 Гражданского кодекса Российской Федерации. ПАО «АВТОВАЗ» оставляет за собой право вносить изменения в регламент технического обслуживания, а также изменять состав запасных частей и материалов без предварительного уведомления.
                    </p>
                    <p>
                        сведения настоящего раздела предназначены исключительно для некоммерческих целей (личных, ознакомительных, исследовательских и иных не связанных с предпринимательской деятельностью). Посетители сайта www.lada.ru не вправе использовать информацию в коммерческих целях, в том числе не имеют права воспроизводить, изменять, рассылать или публиковать информацию в части или полностью без получения предварительного письменного согласия ПАО «АВТОВАЗ».
                    </p>
                </div>

            </div>
        </section>



        <section class="car-types-block">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>