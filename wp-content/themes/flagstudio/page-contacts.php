<?php
/**
 * Template Name: Шаблон страницы контакты (page-contacts.php)
 */
get_header();

?>

    <main>

        <section class="breadcrumbs">
            <div class="inner">
                <? the_breadcrumb(); ?>
            </div>
        </section>

        <section class="main-about page-contacts">
            <section class="information-advantages inner">
                <h1 class="dot-title title"><?=get_field('cont_title','options')?></h1>
                <svg class="contact-line-svg"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="747px" height="445px">
                    <path fill-rule="evenodd"  stroke="rgb(229, 229, 229)" stroke-width="2px" stroke-dasharray="8, 4" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                          d="M132.000,2.000 C124.008,16.056 126.737,36.073 144.000,97.000 C173.787,202.132 19.829,210.841 5.000,299.000 C-9.808,387.032 92.223,454.307 169.000,369.000 C232.000,299.000 337.000,310.000 409.000,332.000 C481.000,354.000 594.000,479.000 744.000,428.000 "/>
                </svg>
            </section>

            <section id="question" class="contacts-form">
                <div class="inner">
                    <div class="contacts-block">
                        <h2 class="dot-title">Наш адрес</h2>
                        <address>
                            <?=get_field('cont_addr','options')?>
                        </address>
                        <div class="phones">
                            <? $phones = get_field('cont_phones','options');
                            foreach ($phones as $phone){
                                echo $phone['cont_phone']."<br>";
                            }
                            ?>
                        </div>
                        <h2 class="dot-title">Задать вопрос</h2>
                        <form id="main-form" class="main-form" method="post" name="askQuestion">
                            <label for="name">
                                <input name="username" id="name" type="text" required>
                                <span>ФИО</span>
                            </label>
                            <label class="half" for="email">
                                <input name="useremail" id="email" type="text" required>
                                <span>Email</span>
                            </label>

                            <label class="half" for="phone">
                                <input name="userphone" id="phone" type="text" required>
                                <span>Телефон</span>
                            </label>

                            <label for="text">
                                <span>Сообщение</span>
                                <textarea class="js-area" name="usertext" id="text" rows="1"> </textarea>
                            </label>

                            <!--                                <input name="ask" type="submit" value="Отправить">-->
                            <?php wp_nonce_field('askQuestion','nonceaskQuestion'); ?>
                            <button  class="g-recaptcha" data-sitekey="6LcbtEAUAAAAALehVcWQ5dlApBvwOSMRienN4Wfq" data-callback='onSubmit'>Отправить</button>
                            <input style="display: none" name="ask" type="submit" value="Отправить">
                        </form>
                    </div>
                    <div id="map-block" class="map-block">
                        <svg class="map-line-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             viewBox="0 0 386 176" width="386px" height="176px" style="enable-background:new 0 0 386 176;" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:none;stroke:#42525A;stroke-width:2;stroke-dasharray:8,4;}
                                </style>
                            <path class="st0" d="M2.1,161.3c10.9-3.5,20.9-6.5,31.9-9.5c60-21,119-44,182-49c34-2,67,1,100,12c25,8,48,21,69.2,36.2"/>
                            </svg>
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A7fe974d16d8a3b3f84596666d71f6543aae7a22263e5657263838f959780ce24&amp;source=constructor" width="815" height="720" frameborder="0"></iframe>
                        <div id="substrate" class="substrate">
                        </div>
                    </div>
                </div>
            </section>

            <section class="feedback-block">

                <div class="inner">
                    <h1 class="dot-title title">Отзывы</h1>
                    <section class="feedback-block__item">
                        <h3>Игорь Каневский</h3>
                        <p>
                            Очень доброжелательная атмосфера, уже не первую машину берем в этом автосалоне. Проехав множество автосалонов и в этот раз,я снова вернулась в этот салон. Нашлась машина в наличии и в нужной комплектации,ни каких навязанных доп.оборудований(во всех других дилерских центрах допы...
                        </p>
                        <a href="#!" class="feedback-block__btn">Читать все</a>
                    </section>
                    <section class="feedback-block__item">
                        <h3>Игорь Каневский</h3>
                        <p>
                            Очень доброжелательная атмосфера, уже не первую машину берем в этом автосалоне. Проехав множество автосалонов и в этот раз,я снова вернулась в этот салон. Нашлась машина в наличии и в нужной комплектации,ни каких навязанных доп.оборудований(во всех других дилерских центрах допы...
                        </p>
                        <a href="#!" class="feedback-block__btn">Читать все</a>
                    </section>

                    <div class="inner-btn">
                        <a href="#!" class="feedback-block__btn">Больше отзывов</a>
                    </div>
                </div>
            </section>


        </section>

        <section class="car-types-block">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>