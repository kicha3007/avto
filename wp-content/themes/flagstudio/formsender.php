<?php
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
//Обработчик ФОС Обратный звонок
if (isset($_POST['callMe']) && wp_verify_nonce($_POST['callMeEnc'], 'callMe')) {
	global $wp;
	include('lada-mailer.php');
	$err = false;
	$name = $_POST['username'];
	if (empty($name) || $name == ' ') $err = true;
	$phone = $_POST['userphone'];
	if (empty($phone) || $phone == ' ') $err = true;
	if (!$err) {
		$message = 'Содержание заявки:<br>
                ФИО: ' . $name . '<br>
                Телефон: ' . $phone . '<br>';
		$email = ladaMail();
		$email->Subject = 'Заявка на обратный звонок';
		$email->MsgHTML($message);
		$mails = get_field('fos_callme', 'options');
		foreach ($mails as $m) {
			$email->AddAddress($m['mail']);
		}
		$send = $email->Send();
		header('Location: /thank-you');
	}
}
//Обработчик ФОС Специальное предложение
if (isset($_POST['specOffer']) && wp_verify_nonce($_POST['specOfferEnc'], 'specOffer')) {
	global $wp;
	include('lada-mailer.php');
	$err = false;
	$name = $_POST['username'];
	if (empty($name) || $name == ' ') $err = true;
	$phone = $_POST['userphone'];
	if (empty($phone) || $phone == ' ') $err = true;
	if (!$err) {
		$message = 'Содержание заявки:<br>
                ФИО: ' . $name . '<br>
                Телефон: ' . $phone . '<br>';
		$email = ladaMail();
		$email->Subject = 'Заявка на Специальное предложение';
		$email->MsgHTML($message);
		$mails = get_field('fos_specoffer', 'options');
		foreach ($mails as $m) {
			$email->AddAddress($m['mail']);
		}
		$send = $email->Send();
		header('Location: /thank-you');
	}
}
//Обработчик ФОС Подождите уходить
if (isset($_POST['dontLeave']) && wp_verify_nonce($_POST['dontLeaveEnc'], 'dontLeave')) {
	global $wp;
	include('lada-mailer.php');
	$err = false;
	$name = $_POST['username'];
	if (empty($name) || $name == ' ') $err = true;
	$phone = $_POST['userphone'];
	if (empty($phone) || $phone == ' ') $err = true;
	if (!$err) {
		$message = 'Содержание заявки:<br>
                ФИО: ' . $name . '<br>
                Телефон: ' . $phone . '<br>
				Марка автомобиля: ' . $_POST['auto'] . '<br>';
		$email = ladaMail();
		$email->Subject = 'Заявка с формы "Подождите уходить"';
		$email->MsgHTML($message);
		$mails = get_field('fos_dontleave', 'options');
		foreach ($mails as $m) {
			$email->AddAddress($m['mail']);
		}
		$send = $email->Send();
		header('Location: /thank-you');
	}
}