<?php
/**
 * Template Name: Шаблон страницы кредита (page-credit.php)
 */
//Обработчик ФОС Кредит
if (isset($_POST['credit']) && wp_verify_nonce($_POST['noncecredit'],'credit')){
    global $wp;
    include('lada-mailer.php');
    $err = false;
    $text = $_POST['text'];
    $name = $_POST['username']; if (empty($name) || $name == ' ') $err = true;
    $phone = $_POST['phone']; if (empty($phone) || $phone == ' ') $err = true;
    $mail = $_POST['useremail']; if (empty($mail) || $mail == ' ') $err = true;
    $car = $_POST['car']; if (empty($car) || $car == ' ') $err = true;
    $date = $_POST['date']; if (empty($date) || $date == ' ') $err = true;
    $srok = $_POST['srok']; if (empty($srok) || $srok == ' ') $err = true;
    $vznos = $_POST['vznos']; if (empty($vznos) || $vznos == ' ') $err = true;
    $stage = $_POST['stage']; if (empty($stage) || $stage == ' ') $err = true;
    $documents = '<br>';
    if ($_POST['pasport'] == 'on') $documents .= 'Паспорт<br>';
    if ($_POST['vodit'] == 'on') $documents .= 'Водительское удостоверение<br>';
    if ($_POST['strah'] == 'on') $documents .= 'Страховое свидетельство пенсионного фонда<br>';
    if ($_POST['2ndfl'] == 'on') $documents .= 'Справка 2-НДФЛ<br>';
    if ($_POST['trud'] == 'on') $documents .= 'Копия трудовой книжки, заверенная работодателем<br>';
    if ($_POST['voen'] == 'on') $documents .= 'Военный билет<br>';
    if ($_POST['zagran'] == 'on') $documents .= 'Заграничный паспорт<br>';
    if (!$err) {
//Отправка письма пользователю
        $message = 'Добрый день!<br><br>
                Вы оставили заявку на сайте ' . get_site_url() . '.<br> Спасибо за проявленный интерес, наш менеджер свяжется с Вами в ближайшее время.<br>
                Содержание заявки:<br>
                Заявка на Кредит<br>
                ФИО: ' . $name . '<br>
                Дата рождения: ' . $date . '<br>
                Марка/Модель: ' . $car . '<br>
                Первоначальный взнос: ' . $vznos . '<br>
                Срок кредита: ' . $srok . '<br>
                Стаж: ' . $stage . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>
                Сообщение: ' . $text . '<br><br>
                Документы: ' . $documents;
        $email = ladaMail();
        $email->Subject = 'Заявка на сайте Лада Автовек';
        $email->MsgHTML($message);
        $email->AddAddress($mail);
        $send = $email->Send();
//Письмо администраторам
        $message = 'Поступила заявка на кредит со страницы «' . home_url($wp->request) . '»<br><br>
                Содержание заявки:<br>
                ФИО: ' . $name . '<br>
                Дата рождения: ' . $date . '<br>
                Марка/Модель: ' . $car . '<br>
                Первоначальный взнос: ' . $vznos . '<br>
                Срок кредита: ' . $srok . '<br>
                Стаж: ' . $stage . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>
                Сообщение: ' . $text . '<br><br>
                Документы: ' . $documents;
        $email2 = ladaMail();
        $email2->Subject = 'Новая заявка на кредит с сайта Лада Автовек';
        $email2->MsgHTML($message);
        $mails = get_field('fos_credit', 'options');
        foreach ($mails as $m) {
            $email2->AddAddress($m['mail']);
        }
        $send = $email2->Send();
        header('Location: /thank-you');
    }
}
get_header();
$post_fields = get_fields();
$options = get_fields(options);
the_post();

?>
    <main>
        <section class="breadcrumbs">
            <div class="inner">
               <? the_breadcrumb(); ?>
            </div>
        </section>


        <section class="content-page">
            <div class="inner">
                <h1 class="title credit-title-margin"><? the_title(); ?></h1>
               <? the_content(); ?>
            </div>
        </section>

        <section class="credit-form-block">
            <div class="inner">
                <h2 class="form-header">Заполните форму для заявки на кредит</h2>
                <form class="main-form credit-form" method="post" name="credit" onSubmit="yaCounter42970034.reachGoal('kredit');">
                    <div class="fieldset first">
                        <label for="name-5">
                            <input name="username" id="name-5" type="text" required>
                            <span>ФИО</span>
                        </label>
                        <label for="date-5">
                            <input name="date" id="date-5" type="text" required>
                            <span>Дата рождения</span>
                        </label>
                        <label for="model-5">
                            <input name="car" id="model-5" type="text" required>
                            <span>Марка автомобиля  (модель и комплектация)</span>
                        </label>
                        <label onabort="pay" for="payment-5">
                            <input name="vznos" id="payment-5" type="text" required>
                            <span>Первоначальный взнос</span>
                        </label>
                        <label for="time-5">
                            <input name="srok" id="time-5" type="text" required>
                            <span>Срок кредита</span>
                        </label>
                    </div>

                    <div class="fieldset second">
                        <b class="radio-header">Какие документы Вы готовы предоставить:</b>

                        <input id="radio-1" class="hidden-radio" type="checkbox" name="pasport">
                        <label for="radio-1" class="custom-radio">
                            Паспорт
                        </label>

                        <input id="radio-2" class="hidden-radio" type="checkbox" name="vodit">
                        <label for="radio-2" class="custom-radio">
                            Водительское удостоверение
                        </label>

                        <input id="radio-3" class="hidden-radio" type="checkbox" name="strah">
                        <label for="radio-3" class="custom-radio">
                            Страховое свидетельство пенсионного фонда
                        </label>

                        <input id="radio-4" class="hidden-radio" type="checkbox" name="2ndfl">
                        <label for="radio-4" class="custom-radio">
                            Справка 2-НДФЛ
                        </label>

                        <input id="radio-5" class="hidden-radio" type="checkbox" name="trud">
                        <label for="radio-5" class="custom-radio">
                            Копия трудовой книжки, заверенная работодателем
                        </label>

                        <input id="radio-6" class="hidden-radio" type="checkbox" name="voen">
                        <label for="radio-6" class="custom-radio">
                            Военный билет
                        </label>

                        <input id="radio-7" class="hidden-radio" type="checkbox" name="zagran">
                        <label for="radio-7" class="custom-radio">
                            Заграничный паспорт
                        </label>

                    </div>

                    <div class="fieldset">
                        <label for="experience-5">
                            <span>Стаж на последнем месте работы</span>
                            <input name="stage" class="js-area" id="experience-5" type="text">
                        </label>

                        <label class="half" for="email-5">
                            <input name="useremail" id="email-5" type="text" required>
                            <span>Email</span>
                        </label>
                        <label class="half" for="phone-5">
                            <input  name="phone" id="phone-5" type="text" required>
                            <span>Телефон</span>
                        </label>

                        <label for="text-5">
                            <span>Сообщение</span>
                            <textarea class="js-area" name="text" id="text-5" rows="1"> </textarea>
                        </label>
                        <input class="personal-data" id="personal-data-4" type="checkbox">
                        <label class="custom-checkbox" for="personal-data-4">Согласен на обработку персональных данных</label>
                        <input id="submit-credit" class="credit-submit" type="submit" value="Отправить" name="credit" disabled>
                    </div>
                    <?php wp_nonce_field('credit','noncecredit'); ?>
                </form>
            </div>
        </section>

        <section class="car-types-block">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>