<?php
/**
 * Шаблон страницы акции (single-sales.php)
 */
//Обработчик ФОС Задать вопрос
if (isset($_POST['sale']) && wp_verify_nonce($_POST['nonceform'], 'form')) {
    include('lada-mailer.php');
    $err = false;
    $name = $_POST['username'];
    if (empty($name) || $name == ' ') $err = true;
    $phone = $_POST['userphone'];
    if (empty($phone) || $phone == ' ') $err = true;
    $mail = $_POST['useremail'];
    if (empty($mail) || $mail == ' ') $err = true;
    if (!$err) {
//Отправка письма пользователю
        $message = "Добрый день!<br><br>
                Вы оставили заявку на сайте " . get_site_url() . ".<br> Спасибо за проявленный интерес, наш менеджер свяжется с Вами в ближайшее время.<br>
                Содержание заявки:<br>
                Акция: «" . home_url($wp->request) . "»<br>
                ФИО: $name<br>
                E-mail: $mail<br>
                Телефон: $phone<br>
                <br>
                <br>
                Удачного дня!";
        $email = ladaMail();
        $email->Subject = 'Заявка на сайте Лада Автовек';
        $email->MsgHTML($message);
        $email->AddAddress($mail);
        $send = $email->Send();
//Письмо администраторам
        $message = 'Поступила заявка со страницы «' . home_url($wp->request) . '»<br><br>
                Содержание заявки:<br>
                ФИО: ' . $name . '<br>
                E-mail: ' . $mail . '<br>
                Телефон: ' . $phone . '<br>';
        $email2 = ladaMail();
        $email2->Subject = 'Новая заявка на участие в акции с сайта Лада Автовек';
        $email2->MsgHTML($message);
        $mails = get_field('fos_action', 'options');
        foreach ($mails as $m) {
            $email2->AddAddress($m['mail']);
        }
        $send = $email2->Send();
        header('Location: /thank-you');
    }
}
get_header();
$post_fields = get_fields();
$options = get_fields(options);
the_post();
global $wp;
?>
    <main>
        <section class="breadcrumbs">
            <div class="inner">
                <? the_breadcrumb(); ?>
            </div>
        </section>
        <script type="application/javascript">
            jQuery(document).ready(function ($) {
                $('.breadcrumbs').find('ul li:eq(0)').after('<li><a href="/sales/">Акции</a></li>');
            });
        </script>
        <section class="sales-block">
            <div class="inner">

                <article class="content">
                    <figure class="post-img">
                        <? the_post_thumbnail('full'); ?>
                    </figure>
                    <h1 class="title"><? the_title() ?></h1>
                    <? the_content(); ?>

                </article>
            </div>
        </section>
        <section class="sales-request">
            <div class="inner">
                <h2>Оставить заявку</h2>
                <svg class="sales-line" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     width="591px" height="276px">
                    <path fill-rule="evenodd" stroke="rgb(228, 228, 228)" stroke-width="2px" stroke-dasharray="8, 4"
                          stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                          d="M440.000,2.000 C439.000,93.000 492.000,99.000 557.000,144.000 C622.000,189.000 585.000,313.000 455.000,261.000 C325.000,209.000 256.270,87.983 116.000,126.000 C9.000,155.000 8.000,185.000 3.000,193.000 "/>
                </svg>
            </div>
            <div class="sale-form-bg">
                <div class="inner">
                    <form class="sale-form" name="form" method="post">
                        <input name="username" type="text" placeholder="ФИО" required>
                        <input name="userphone" type="text" placeholder="Номер телефона" required>
                        <input name="useremail" type="text" placeholder="Email" required>
                        <input type="submit" value="Отправить заявку" name="sale">
                        <?php wp_nonce_field('form', 'nonceform'); ?>
                    </form>
                </div>
            </div>


        </section>
        <section class="car-types-block no-border">
            <? get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>