<?php
/**
 * Шаблон страницы новости (single-news.php)
 */

get_header();
the_post();
global $wp;
?>
    <main>
        <section class="breadcrumbs">
            <div class="inner">
                <? the_breadcrumb(); ?>
            </div>
        </section>
        <script type="application/javascript">
            jQuery(document).ready(function ($) {
                $('.breadcrumbs').find('ul li:eq(0)').after('<li><a href="/news/">Новости</a></li>');
            });
        </script>
        <section class="sales-block">
            <div class="inner">

                <article class="content">
                    <figure class="post-img">
                        <? the_post_thumbnail('full'); ?>
                    </figure>
                    <h1 class="title"><? the_title() ?></h1>
                    <? the_content(); ?>

                </article>
            </div>
        </section>
        <section class="car-types-block no-border">
            <? get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>