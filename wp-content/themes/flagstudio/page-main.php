<?php
/**
 * Template Name: Шаблон главной страницы (page-main.php)
 */
//Обработчик ФОС Задать вопрос
if (wp_verify_nonce($_POST['nonceaskQuestion'],'askQuestion')) {

    $result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, stream_context_create(array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query(array(
                'response' => $_POST['g-recaptcha-response'],
                'secret' => '6LcbtEAUAAAAAFf5qy-CC0e8MZTJ9a0X9Ys0VZie'
            )),
        ),
    )));
    $result = json_decode($result);
    if ($result->success) {

        include('lada-mailer.php');
        $err = false;
        $text = $_POST['usertext'];
        if (empty($text) || $text == ' ') $err = true;
        $name = $_POST['username'];
        if (empty($name) || $name == ' ') $err = true;
        $phone = $_POST['userphone'];
        if (empty($phone) || $phone == ' ') $err = true;
        $mail = $_POST['useremail'];
        if (empty($mail) || $mail == ' ') $err = true;
        if (!$err) {
//Отправка письма пользователю
            $message = "Добрый день!<br><br>
                Вы оставили заявку на сайте " . get_site_url() . ".<br> Спасибо за проявленный интерес, наш менеджер свяжется с Вами в ближайшее время.<br>
                Содержание заявки:<br>
                ФИО: $name<br>
                E-mail: $mail<br>
                Телефон: $phone<br>
                Вопрос: $text<br>
                <br><br>
                Удачного дня!";
            $email = ladaMail();
            $email->Subject = 'Заявка на сайте Лада Автовек';
            $email->MsgHTML($message);
            $email->AddAddress($mail);
            $send = $email->Send();
//Письмо администраторам
            $message = "Поступила заявка со страницы Главная<br><br>
                Содержание заявки:<br>
                ФИО: $name<br>
                E-mail: $mail<br>
                Телефон: $phone<br>
                Вопрос: $text<br>";
            $email2 = ladaMail();
            $email2->Subject = 'Новая заявка с сайта Лада Автовек';
            $email2->MsgHTML($message);
            $mails = get_field('fos_home', 'options');
            foreach ($mails as $m) {
                $email2->AddAddress($m['mail']);
            }
            $send = $email2->Send();
            header('Location: /thank-you');

            //Запись в CRM
//            $data = '{ "brandId": 125, "modelId": 2, "salonId": 1, "sourceId": 9, "comment": "'.strip_tags($message).'", "firstName": "'.$name.'", "lastName": "", "middleName": "", "phoneNumber": "'.$phone.'", "email": "'.$mail.'", "tagIds": [ 0 ] }';
//
//            $ch = curl_init('http://avtovek.autocrm.ru/yii/api/lead');
//            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_USERPWD, "t.babintseva:avth85");
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                    'Content-Type: application/json',
//                    'Content-Length: ' . strlen($data))
//            );
//
//            $result = curl_exec($ch);
        }
    }
}

get_header();

//$data = '{"salonId": 1, "type":2 }';
//
//            $ch = curl_init('http://avtovek.autocrm.ru/yii/api/tag');
//            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_USERPWD, "s.zapis:cw7x5j");
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                    'Content-Type: application/json',
//                    'Content-Length: ' . strlen($data))
//            );
//
//            $result = curl_exec($ch);
//            ob_start();
//            echo "<pre>";
//            var_dump(json_decode($result));
//            echo "</pre>";
//            $output = ob_get_clean();
//            file_put_contents('wizlog.html', $output);

$post_fields = get_fields();
$options = get_fields(options);
?>

<?php

?>


    <main>
        <script>
            function onSubmit(){
                jQuery('input[name=ask]').click();
            }
        </script>
    <? if($post_fields["slider"]): ?>
        <section id="main-slider" class="main-page-slider owl-carousel">
            <?$slides=$post_fields['slider'];
            foreach ($slides as $slide):?>
            <div class="slide" style="background-image: url('<?=$slide["slider_img"]?>')">
                <div class="inner">
                <div class="slider-text">
                    <h2><?=$slide["slider_header"]?></h2>
                    <p><?=$slide["slider_text"]?></p>

                    <?if($slide["slider_link"]):?>
                    <a class="btn" href="<?=$slide["slider_link"]?>" title="<?=$slide["slider_link_title"]?>">Подробнее</a>
                    <?endif;?>
                </div>
                </div>
            </div>
            <? endforeach; ?>
        </section>
   <? endif; ?>
    <?if ( has_nav_menu( 'slider-menu' ) ):?>
        <section class="models-menu">
            <div class="inner">
               <?
               $args = array(
                  'theme_location'  => 'slider-menu',
                  'menu'            => '',              // (string) Название выводимого меню (указывается в админке при создании меню, приоритетнее чем указанное местоположение theme_location - если указано, то параметр theme_location игнорируется)
                  'container'       => '',           // (string) Контейнер меню. Обворачиватель ul. Указывается тег контейнера (по умолчанию в тег div)
                  'container_class' => '',              // (string) class контейнера (div тега)
                  'menu_class'      => '',          // (string) class самого меню (ul тега)
                  'echo'            => true,            // (boolean) Выводить на экран или возвращать для обработки
                  'fallback_cb'     => 'wp_page_menu',  // (string) Используемая (резервная) функция, если меню не существует (не удалось получить)
               );
               wp_nav_menu($args);
               ?>
            </div>
        </section>
    <? endif; ?>

    <? if($post_fields["sale"]): ?>
        <section class="main-carousel">
            <div id="main-carousel" class="inner carousel-inner owl-carousel">
           <?$sales=$post_fields['sale'];
           foreach ($sales as $sale):?>
                <a href="<?=$sale["sale_link"]?>" title="<?=$sale["sale_title"]?>"><img src="<?=$sale["sale_image"]?>" width="320" height="160" alt=""></a>
           <? endforeach; ?>
            </div>
            <div class="inner">
                <a class="btn dark-btn " href="/sales" title="Смотреть все акции">Смотреть все акции</a>
            </div>
        </section>
    <? endif; ?>

        <section class="videos">
            <div class="inner">
            <? $videos = json_decode( file_get_contents( 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UCtimlnsTN17gQieP2x35IsQ&maxResults=4&order=date&type=video&key=AIzaSyBlDOboaTRr8LETFLWgIc4Gjcfsa3RXk6U' ) ) ?>
                <h2 class="videos__title">Видео</h2>
                <div class="videos__main">
                    <img src="<?= $videos->items[0]->snippet->thumbnails->high->url ?>" alt="preview">
                    <ul class="buttons-list buttons-list--full">
                        <li class="buttons-list__button buttons-list__button--play ">
                            <a href="https://www.youtube.com/watch?v=<?= $videos->items[0]->id->videoId ?>" data-fancybox></a>
                        </li>
                    </ul>
                </div>

                <h2 class="videos__carousel-title">Последние видео</h2>
                <div class="videos__carousel swiper-container js--swiper">
                    <div class="swiper-wrapper videos__carousel-inner">
                        <? foreach ( $videos->items as $key => $video ) {
                            if ($key == 0) continue;?>
                        <div class="swiper-slide videos__carousel-item ">
                            <img src="<?= $video->snippet->thumbnails->high->url ?>" alt="preview">
                            <ul class="buttons-list buttons-list--full">
                                <li class="buttons-list__button buttons-list__button--play ">
                                    <a href="https://www.youtube.com/watch?v=<?= $video->id->videoId ?>" data-fancybox></a>
                                </li>
                            </ul>
                        </div>
                        <? } ?>
                    </div>
                </div>
            </div>

        </section>

        <section class="main-about">
                <section class="information-advantages inner">
                    <svg class="main-line-svg"
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         width="759px" height="1482px">
                        <path fill-rule="evenodd"  stroke="rgb(228, 228, 228)" stroke-width="2px" stroke-dasharray="8, 4" stroke-linecap="butt" stroke-linejoin="miter" fill="none"
                              d="M742.000,1469.000 C592.000,1520.000 453.000,1377.000 381.000,1355.000 C309.000,1333.000 243.000,1324.000 167.000,1410.000 C91.000,1496.000 -12.000,1428.000 3.000,1340.000 C18.000,1252.000 141.000,1226.000 143.000,1144.000 C145.000,1062.000 127.000,1034.000 116.000,967.000 C105.000,900.000 228.201,867.761 249.000,832.000 C272.028,792.406 225.000,693.000 216.000,631.000 C207.000,569.000 177.000,392.000 271.000,307.000 C365.000,222.000 543.000,283.000 564.000,281.000 C585.000,279.000 642.655,261.443 637.000,200.000 C631.178,136.736 602.241,115.647 623.000,82.000 C644.853,46.579 724.437,69.301 744.000,38.000 C756.500,18.000 754.000,2.000 754.000,2.000 "/>
                    </svg>
                    <figure class="about-image" style="background-image: url(<?=get_field('about_salon_image','options')?>); background-size: cover;">
                    </figure>
                    <div class="information-block">
                        <h1 class="dot-title"><?=get_field('about_salon_title','options')?></h1>
                        <p><?=get_field('about_salon_text','options')?></p>
                        <div class="advantages-block">
                            <? $elements = get_field('about_salon_elements','options');
                            foreach ($elements as $element){?>
                                <div class="advantage">
                                    <img src="<?=$element['pic']?>" width="<?=$element['width']?>" height="<?=$element['height']?>" alt="">
                                    <span><?=$element['text']?></span>
                                </div>
                                <? } ?>
                        </div>
                    </div>
                </section>

                <section id="question" class="contacts-form">
                    <div class="inner">
                        <div class="contacts-block">
                            <h2 class="dot-title"><?=get_field('cont_title','options')?></h2>
                            <address>
                                <?=get_field('cont_addr','options')?>
                            </address>
                            <div class="phones">
                                <? $phones = get_field('cont_phones','options');
                                foreach ($phones as $phone){
                                    echo $phone['cont_phone']."<br>";
                                }
                                ?>
                            </div>
                            <h2 class="dot-title">Задать вопрос</h2>
                            <form id="main-form" class="main-form" method="post" name="askQuestion" onSubmit="yaCounter42970034.reachGoal('vopros');">
                                <label for="name">
                                    <input name="username" id="name" type="text" required>
                                    <span>ФИО</span>
                                </label>
                                <label class="half" for="email">
                                    <input name="useremail" id="email" type="text" required>
                                    <span>Email</span>
                                </label>

                                <label class="half" for="phone">
                                    <input name="userphone" id="phone" type="text" required>
                                    <span>Телефон</span>
                                </label>

                                <label for="text">
                                    <span>Сообщение</span>
                                    <textarea class="js-area" name="usertext" id="text" rows="1"> </textarea>
                                </label>

<!--                                <input name="ask" type="submit" value="Отправить">-->
                                <?php wp_nonce_field('askQuestion','nonceaskQuestion'); ?>
                                <button  class="g-recaptcha" data-sitekey="6LcbtEAUAAAAALehVcWQ5dlApBvwOSMRienN4Wfq" data-callback='onSubmit'>Отправить</button>
                                <input style="display: none" name="ask" type="submit" value="Отправить">
                            </form>
                        </div>
                        <div id="map-block" class="map-block">
                            <svg class="map-line-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 viewBox="0 0 386 176" width="386px" height="176px" style="enable-background:new 0 0 386 176;" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:none;stroke:#42525A;stroke-width:2;stroke-dasharray:8,4;}
                                </style>
                                <path class="st0" d="M2.1,161.3c10.9-3.5,20.9-6.5,31.9-9.5c60-21,119-44,182-49c34-2,67,1,100,12c25,8,48,21,69.2,36.2"/>
                            </svg>
                            <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A7fe974d16d8a3b3f84596666d71f6543aae7a22263e5657263838f959780ce24&amp;source=constructor" width="815" height="720" frameborder="0"></iframe>
                            <div id="substrate" class="substrate">
                            </div>
                        </div>
                    </div>
                </section>
        </section>
        <section class="car-types-block">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>