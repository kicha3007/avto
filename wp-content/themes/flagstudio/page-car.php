<?php
/**
 * Template Name: Шаблон модели автомобиля (page-car.php)
 */
get_header();
$post_fields = get_fields();
$categoryList = get_categories('taxonomy=category&orderby=id');
?>
    <main class="page-car">
        <section class="cars-catalog">
            <div class="inner">
                <section class="breadcrumbs">
                   <? the_breadcrumb(); ?>
                </section>

                <h1><? the_title() ?></h1>

                <section class="car-catalog-items">
                    <div class="car-catalog-item">

                        <h2>Granta</h2>
                        <div class="wrapper-for-js">
                            <div class="content">

                                <a class="content-item" href="/cars/granta-sedan/" title="Lada Granta седан">
                                    <span class="img-wrapper">
                                        <img width="150" height="70" src="/wp-content/uploads/2017/12/model_6557223-1.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="">                                    </span>
                                    <span class="link-title">Lada Granta седан</span>
                                </a>
                                <a class="content-item" href="/cars/lada-granta-liftbek/" title="Granta лифтбек">
                                    <span class="img-wrapper">
                                        <img width="150" height="70" src="/wp-content/uploads/2017/12/model_7214482-1.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="">                                    </span>
                                    <span class="link-title">Granta лифтбек</span>
                                </a>
                                <a class="content-item" href="/cars/granta-sport-versii/" title="Granta Sport версии">
                                    <span class="img-wrapper">
                                        <img width="150" height="70" src="/wp-content/uploads/2017/12/model_6660162-1.png" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="">                                    </span>
                                    <span class="link-title">Granta Sport версии</span>
                                </a>

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>

        <section class="content-page">

            <div class="inner">
                <h2 class="title">Заголовок блока</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquid beatae delectus ex facilis
                    magnam magni pariatur quaerat quisquam vero? Aperiam autem dolorem expedita id in nemo neque nulla
                    voluptas!
                </p>
                <p>A consectetur dolores pariatur sequi voluptatem. Accusamus architecto atque blanditiis, dolorem
                    doloribus eos facilis quisquam voluptates! Amet at cum deserunt, dolor dolorum excepturi in
                    inventore labore magni nihil, quas veritatis.
                </p>
            </div>

        </section>

        <section class="car-types-block car-types-margin no-border">
            <?get_template_part('inc/car-types', 'index'); ?>
        </section>
    </main>
<?php get_footer(); ?>