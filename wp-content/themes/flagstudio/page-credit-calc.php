<?php
/**
 * Template Name: Шаблон страницы кредитного калькулятора(page-credit-calc.php)
 */

if ($_REQUEST['subCreditCalc']) {
    include('lada-mailer.php');

    $model = get_post($_REQUEST['model']);
    $modelName = $model->post_title;

    $message = '
        Поступила новая заявка на Кредитование со страницы: ' . get_site_url() . $_SERVER['REQUEST_URI'] . '.<br>'
        .'Содержание заявки:<br>'
        .'ФИО: ' . $_REQUEST['name-user'] . '<br>'
        .'Телефон: ' . $_REQUEST['phone'] . '<br>'
        .'Модель: '. $modelName . '<br>'
        .'Комплектация: '. $_REQUEST['equipment'] . '<br>'
        .'Цена: '. $_REQUEST['cost'] . '<br>'
        .'Первоначальный взнос: '. $_REQUEST['first-payment'] . '<br>'
        .'Срок кредитования: '. $_REQUEST['date-payment'] . ' мес.<br>'
        .'Ежемесячный платеж: '. $_REQUEST['per-month'] . ' р/мес.<br>'
        .'Ежедневный платеж: '. $_REQUEST['per-day'] . 'р/день.<br>';

    $email          = ladaMail();
    $email->Subject = 'Заявка не кредитование';
    $email->MsgHTML( $message );
    $mails = get_field( 'fos_credit_calc', 'options' );
    foreach ( $mails as $mail ) {
        $email->AddAddress( $mail['mail'] );
    }
    $send = $email->Send();
    header( 'Location: /thank-you' );

}

if ( $_REQUEST['subCreditService'] ) {
	include( 'lada-mailer.php' );
//Отправка письма пользователю
	$message        = '
                Поступила новая заявка на Кредит со страницы: ' . get_site_url() . $_SERVER['REQUEST_URI'] . '.<br>
                Содержание заявки:<br>
                Модель автомобиля: ' . $_REQUEST['model'] . '<br>
                ФИО: ' . $_REQUEST['credit-username'] . '<br>
                E-mail: ' . $_REQUEST['credit-useremail'] . '<br>
                Телефон: ' . $_REQUEST['credit-userphone'] . '<br>';
	$email          = ladaMail();
	$email->Subject = 'Быстрая заявка на кредитование';
	$email->MsgHTML( $message );
	$mails = get_field( 'fos_credit_calc', 'options' );
	foreach ( $mails as $mail ) {
		$email->AddAddress( $mail['mail'] );
	}
	$send = $email->Send();
	header( 'Location: /thank-you' );
}

get_header();
$post_id = $post->ID;
?>
    <main>
        <section class="breadcrumbs">
            <div class="inner">
				<? the_breadcrumb(); ?>
            </div>
        </section>

        <section class="credit-block">
            <div class="inner">

                <h1 class="credit__header">
					<? the_title(); ?>
                </h1>

                <section class="credit-calculator">
                    <form method="post" enctype="multipart/form-data" class="js--nice-select" onSubmit="yaCounter42970034.reachGoal('calc');">
                        <div class="credit-calculator__select calculator-select js--data-select">
                            <div class="credit-calculator__select-inner">

                                <h1>Предварительная заявка на кредит</h1>
                                <div class="credit-calculator__select-item">
                                    <h2 class="credit-calculator__select-title">Выберите модель</h2>
                                    <select name="model" class="js--credit-model">
                                        <option value="selected">Выберите модель</option>
										<?
										$categoryList = get_categories( 'taxonomy=category&orderby=id' );
										foreach ( $categoryList as $list ) {
											$args  = array(
												'post_type'      => 'catalog',
												'post_status'    => 'publish',
												'orderby'        => 'date',
												'order'          => 'ASC',
												'posts_per_page' => '-1',
												'category'       => $list->name
											);
											$posts = new WP_Query( $args );
											foreach ( $posts->posts as $post ) {
												if ( ! get_field( 'is_cat', $post->ID ) ) {
													?>
                                                    <option value="<?= $post->ID ?>"><?= $post->post_title ?></option>
												<? }
											}
										} ?>
                                    </select>
                                </div>
                                <div class="credit-calculator__select-item">
                                    <h2 class="credit-calculator__select-title">Комплектация</h2>
                                    <select name="equipment" class="js--credit-equip">
                                        <option value="selected">Выберите комплектацию</option>
                                    </select>
                                </div>
                                <div class="credit-calculator__cost">
                                <span class="cost js--auto-cost js--cost-in">
                                    0
                                </span>
                                    <input name="cost" type="text" class="hidden js--cost-form" style="display: none;" value="0">
                                    <input name="per-day" type="text" class="hidden js--per-day-form" style="display: none;" value="">
                                    <input name="per-month" type="text" class="hidden js--per-month-form" style="display: none;" value="">
                                    <p>
                                        Цена зависит от модели и
                                        комплекатции автомобиля
                                    </p>
                                </div>
                                <section class="credit-first-payment">
                                    <div class="credit-first-payment__inner">

                                        <h2>Первоначальный взнос</h2>
                                        <input name="first-payment" type="number" value="0" class="js--input-credit-payment">
                                        <input class="js--data-credit-payment" type="text" style="display: none;"
                                               data-from-payment="0" data-to-payment="100" data-step-payment="1"
                                               data-set-default-payment="0">

                                    </div>
                                    <div class="range js--credit-payment"></div>
                                    <div class="credit-payment__numbers js--generate-payment"></div>
                                    <div class="line-of-range js--line-of-range"></div>
                                </section>
                                <section class="credit-date">
                                    <div class="credit-date__inner">

                                        <h2>Срок кредитования</h2>
                                        <input name="date-payment" type="number" value="48" class="js--input-credit-date">
                                        <input class="js--data-credit-date" type="text" style="display: none;"
                                               data-from-date="6" data-to-date="60" data-step-date="1"
                                               data-set-default-date="24">

                                    </div>
                                    <div class="range js--credit-date"></div>
                                    <div class="credit-date__numbers js--generate-date"></div>
                                    <div class="line-of-range js--line-of-range"></div>
                                </section>
                                <div class="credit-calculator__payment">
                                    <h2>Платеж в день/месяц</h2>
                                    <span class="per-day js--per-day-in">
                                    320
                                </span>
                                    <span class="per-month js--per-month-in">
                                    10 200
                                </span>

                                </div>
                            </div>
                            <section class="credit-calculator__request">
                                <h2 class="credit-calculator__request-title">
                                    Заполните заявку
                                </h2>
                                <label class="credit-calculator__request-field">
                                    <span>Ф.И.О.</span>
                                    <input name="name-user" type="text" required>
                                </label>
                                <label class="credit-calculator__request-field">
                                    <span>Телефон</span>
                                    <input name="phone" type="text" required>
                                </label>

                                <label class="credit-calculator__request-agree">
                                    <input name="agree-policy" type="checkbox" id="credit-calculator-personal-data">
                                    <span class="checkmark"></span>
                                    Отправляя заявку через форму обратной связи Вы соглашаетесь
                                    с нашей <a class="privacy-policy
" target="_blank" href="<?= get_field('policy', 'options') ?>">Политикой конфиденциальности</a>.
                                </label>
                                <button class="credit-calculator__button js--send" type="submit" id="submit-creditCalc" disabled name="subCreditCalc" value="1">
                                    Отправить заявку
                                </button>
                            </section>
                        </div>
                        <div class="credit-calculator__panel">
                            <button type="button" class="service__button credit__button js--credit-fast">Оставить быструю заявку на
                                кредит
                            </button>
                            <div class="credit-calculator__panel-show">
                                <div class="img">
                                    <img src="<?= get_template_directory_uri(); ?>/assets/img/lada.png" alt="lada" class="js--credit-image">
                                    <div class="preloader-small js--load-image">
                                        <div class="circle circle-1"></div>
                                        <div class="circle circle-2"></div>
                                        <div class="circle circle-3"></div>
                                    </div>
                                </div>
                                <div class="cost">
                                    <span class="cost__normal js--cost-out">500 000</span>
                                    <span class="cost__per-day js--per-day-out">320</span>
                                    <span class="cost__per-month js--per-month-out">10 200</span>
                                </div>
                            </div>
                            <section class="credit-stock">
                                <?php
                                $actions = get_field( 'actions', $post_id );
                                if ($actions !== false){ ?>
                                    <h2>
                                        Акция по автокредиту
                                    </h2>
                                <? } ?>

                                <div class="credit-stock__slider">
                                    <section class="main-carousel">
                                        <div id="credit-carousel" class="inner carousel-inner owl-carousel">
											<?
											foreach ( $actions as $action ) { ?>
                                                <a target="_blank" href="<?= get_permalink( $action["action"]->ID ) ?>"
                                                   title="text"><img src="<?= $action["img"] ?>" alt=""></a>
											<? } ?>
                                        </div>
                                    </section>
                                </div>
                                <div class="credit-stock__note">
                                    <p>
                                        Обращаем Ваше внимание на то, что данный интернет-сайт носит исключительно
                                        информационный характер и ни при каких условиях не является публичной офертой,
                                        определяемой положениями ч. 2 ст. 437 Гражданского кодекса Российской Федерации.
                                        Для получения подробной информации о стоимости автомобилей, пожалуйста,
                                        обратитесь к менеджерам автосалона. Осуществляя навигацию по сайту, Вы даете нам
                                        право запоминать и иметь доступ к куки-файлам на Вашем устройстве доступа к
                                        Интернету согласно
                                        <a class="privacy-policy" target="_blank"
                                           href="<?= get_field( 'policy', 'options' ) ?>">политике
                                            конфиденциальности</a>.
                                    </p>
                                </div>
                            </section>
                        </div>
                    </form>
                </section>

                <section class="our-partners">
                    <h2>
						<?= get_field( 'partners_title', $post_id ) ?>
                    </h2>
                    <p><?= get_field( 'information', $post_id ) ?></p>
                    <br>
                    <ul>
						<? $partners = get_field( 'partners', $post_id );
						foreach ( $partners as $partner ) { ?>
                            <li><img src="<?= $partner['partner'] ?>" width="<?= $partner['width'] ?>"
                                     height="<?= $partner['height'] ?>" alt="logo"></li>
						<? } ?>
                    </ul>
                </section>

                <section class="credit-conditionals">
                    <h2>
                        Условия кредитования
                    </h2>
					<?= get_field( 'conditions', $post_id ) ?>
                </section>
            </div>
        </section>


        <section class="car-types-block">
			<? get_template_part( 'inc/car-types', 'index' ); ?>
        </section>
    </main>
<?php get_footer(); ?>