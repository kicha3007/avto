<?php
/**
 * Template Name: Шаблон страницы Thanks (page-thanks.php)
 */

get_header();
$post_fields = get_fields();
?>

<section class="thanks-block">
  <div class="inner">
    <div class="check-icon">
      <img src="<?=get_stylesheet_directory_uri()?>/assets/img/check.png" width="120" height="120" alt="done">
    </div>
    <div class="thank-you-text">
      Спасибо за вашу заявку!
      <span>Наш менеджер свяжется с Вами в ближайшее время</span>
    </div>
    <div class="thanks-social-icons">
        <a class="vk" href="https://vk.com/avtovekekbru" title="ссылка на страницу VK" target="_blank" rel="noopener">ссылка VK</a>
        <a class="fb" href="https://www.facebook.com/AVTOVEKLADA/ " title="ссылка на страницу Facebook" target="_blank" rel="noopener">ссылка Facebook</a>
        <a class="you" href="https://www.youtube.com/user/ttrulia" title="ссылка на страницу Youtube" target="_blank" rel="noopener">ссылка Youtube</a>
        <a class="inst" href="https://www.instagram.com/avtovek_ekb_lada/" title="ссылка на профиль Instagram" target="_blank" rel="noopener">ссылка Instagram</a>
        <a class="ok" href="https://ok.ru/avtovekekb" title="ссылка на страницу Одноклассники" target="_blank" rel="noopener">ссылка Одноклассники</a>
        <a class="ps" href="https://www.periscope.tv/AVTOVEK" title="ссылка на профиль Periscope" target="_blank" rel="noopener">ссылка Periscope</a>
    </div>
  </div>
</section>
</div>

<? get_footer(); ?>
